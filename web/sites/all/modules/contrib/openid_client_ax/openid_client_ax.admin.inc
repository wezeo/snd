<?php

/**
 * Settings function for the module
 */
function openid_client_ax_admin_settings($form, &$form_state) {
  $form = array();
  $form['openid_client_ax'] = array(
    '#type' => 'fieldset',
    '#title' => t('OpenID Client Attribute Exchange Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['openid_client_ax']['openid_client_ax_alias'] = array(
    '#type' => 'textfield',
    '#title' => t('Namespace to utilize for Attribute Exchange'),
    '#default_value' => variable_get('openid_client_ax_alias', 'ax'),
    '#description' => t('Per OpenID Specification you can specify your own namespace for attribute exchange that way you are guaranteed to always get your information back without conflicting with another'),
  );
  $form['openid_client_ax']['openid_client_ax_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain for AX definitions'),
    '#default_value' => variable_get('openid_client_ax_domain', 'openid.net'),
    '#description' => t('Prefix domain to use for ax definitions since some providers use different domains than the standard ones'),
  );
  if (module_exists('content_profile')) {
    $form['openid_client_ax']['openid_client_ax_supress_notifications'] = array(
      '#type' => 'checkbox',
      '#title' => t('Supress Content Profile Notifications'),
      '#default_value' => variable_get('openid_client_ax_supress_notifications', FALSE),
      '#description' => t('If checked we will supress notifications when a user logs in via Open ID and Client Attribute exchange if the content profile is updated'),
    );
  }
  return system_settings_form($form);
}


OpenID Attribute Exchange API
==============


DESCRIPTION
-----------

This module holds shared functionality of OpenID Client AX and OpenID Provider AX modules.

[1] http://drupal.org/project/openid_client_ax
[2] http://drupal.org/project/openid_provider_ax

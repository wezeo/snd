(function($) {

Drupal.admin = Drupal.admin || {};
Drupal.admin.behaviors = Drupal.admin.behaviors || {};

/**
 * @ingroup admin_behaviors
 * @{
 */

/**
 * Apply active trail highlighting based on current path.
 *
 * @todo Not limited to toolbar; move into core?
 */
Drupal.admin.behaviors.toolbarActiveTrail = function (context, settings, $adminMenu) {
  if (settings.admin_menu.toolbar && settings.admin_menu.toolbar.activeTrail) {
    $adminMenu.find('> div > ul > li > a[href="' + settings.admin_menu.toolbar.activeTrail + '"]').addClass('active-trail');
  }
};

/**
 * Toggles the shortcuts bar.
 */
Drupal.admin.behaviors.shortcutToggle = function (context, settings, $adminMenu) {
  var $shortcuts = $adminMenu.find('.shortcut-toolbar');
  if (!$shortcuts.length) {
    return;
  }
  var storage = window.localStorage || false;
  var storageKey = 'Drupal.admin_menu.shortcut';
  var $body = $(context).find('body');
  var $toggle = $adminMenu.find('.shortcut-toggle');
  $toggle.click(function () {
    var enable = !$shortcuts.hasClass('active');
    $shortcuts.toggleClass('active', enable);
    $toggle.toggleClass('active', enable);
    if (settings.admin_menu.margin_top) {
      $body.toggleClass('admin-menu-with-shortcuts', enable);
    }
    // Persist toggle state across requests.
    storage && enable ? storage.setItem(storageKey, 1) : storage.removeItem(storageKey);
    this.blur();
    return false;
  });

  if (!storage || storage.getItem(storageKey)) {
    $toggle.trigger('click');
  }
};

/**
 * @} End of "ingroup admin_behaviors".
 */
 
 var onceStarted = false;
 
 function onWindowResize()
 {
	var $ = jQuery;
	var menu = $('#admin-menu');
	if (menu.length)
	{
		// this block runs only once
		if (!onceStarted)
		{
			// hiding text "Hello " username
			menu.find('.admin-menu-account a').wrapInner('<span class="text"></span>');
			menu.find('.admin-menu-account a strong').appendTo('.admin-menu-account a');
			menu.find('.admin-menu-account a .text').css('display', 'none');
			// hiding "Tasks" item
			menu.find('li a[href$="admin/tasks"]').parent().hide();
			// hiding "Help" item
			menu.find('li a[href$="admin/help"]').parent().hide();
			
			onceStarted = true;
		}
		// normal size is 0.9em
		menu.css('font-size', '0.9em');
		var size = 8;
		// this will size down from 0.8 to 0.3em if menu is breaked to two lines
		while (menu.outerHeight()>36 && size>=3)
			menu.css('font-size', (size--/10).toFixed(1)+'em');
	}
	else
		setTimeout( arguments.callee, 50 );
 };
 
 jQuery(window).bind('resize', onWindowResize);
 onWindowResize();
 
 
})(jQuery);

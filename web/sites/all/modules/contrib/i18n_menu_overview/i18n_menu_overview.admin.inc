<?php
/**
 * @file
 * Admin page callbacks for the menu language module.
 */

 /**
  * Builds a form with all the menus to be enabled/disabled.
  */
function i18n_menu_overview_settings() {
  $menus = menu_get_menus();

  foreach ($menus as $menu => $localized_name) {
    $var_name_mnu = 'i18n_menu_overview_' . str_replace(' ', '_', $menu);
    $form[$var_name_mnu] = array(
      '#type' => 'checkbox',
      '#title' => t('Do you want to manage') . ' ' . $menu,
      '#default_value' => variable_get($var_name_mnu, FALSE),
    );
  }
  $form['#submit'][] = 'i18n_menu_overview_settings_form_submit';

  return system_settings_form($form);
}

 /**
  * Submit function that states flush caches.
  *
  * form @param $form the form.
  * form @param $form_state the form_state.
  */
function i18n_menu_overview_settings_form_submit($form, &$form_state) {
  drupal_set_message(t('Please flush caches when you have changed a setting :') . ' ' . l(t('Flush all caches'), 'admin/config/development/performance'));
  drupal_set_message(t('Please make this menu translatable at :') . ' ' . l(t('Menu overview'), 'admin/structure/menu'));
}

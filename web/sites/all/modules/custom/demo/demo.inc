<?php

/**
 * Implements hook_search_info().
 */
function snd_entity_search_info() {
  return array(
    'title' => 'Person',
    'path' => 'person',
  );
}

/**
 * Implements hook_search_access().
 */
function snd_entity_search_access() {
  return user_access('access content');
}

/**
 * Implements hook_search_reset().
 */
function snd_entity_search_reset() {
  db_update('search_dataset')
    ->fields(array('reindex' => REQUEST_TIME))
    ->condition('type', 'person')
    ->execute();
}

/**
 * Implements hook_search_status().
 */
function snd_entity_search_status() {
  $total = db_query('SELECT COUNT(*) FROM {custom_snd_person}')->fetchField();
  $remaining = db_query("SELECT COUNT(*) FROM {custom_snd_person} p LEFT JOIN {search_dataset} d ON d.type = 'snd_entity' AND d.sid = p.id WHERE d.sid IS NULL OR d.reindex <> 0")->fetchField();
  return array('remaining' => $remaining, 'total' => $total);
}

/**
 * Implements hook_search_execute().
 */
function snd_entity_search_execute($keys = NULL, $conditions = NULL) {
/*
  // Build matching conditions
  $query = db_select('search_index', 'i', array('target' => 'slave'))->extend('SearchQuery')->extend('PagerDefault');
  $query->join('custom_snd_person', 'p', 'p.id = i.sid');
  $query
    ->condition('p.deleted', 0)
//    ->addTag('node_access')
    ->searchExpression($keys, 'snd_entity');

  // Insert special keywords.
  $query->setOption('type', 'n.type');
  $query->setOption('language', 'n.language');
  if ($query->setOption('term', 'ti.tid')) {
    $query->join('taxonomy_index', 'ti', 'n.nid = ti.nid');
  }
  // Only continue if the first pass query matches.
  if (!$query->executeFirstPass()) {
    return array();
  }

  // Add the ranking expressions.
  _node_rankings($query);

  // Load results.
  $find = $query
    ->limit(10)
    ->execute();
*/
  $results = array();
/*
  foreach ($find as $item) {
    // Render the node.
    $node = node_load($item->sid);
    $build = node_view($node, 'search_result');
    unset($build['#theme']);
    $node->rendered = drupal_render($build);

    // Fetch comments for snippet.
    $node->rendered .= ' ' . module_invoke('comment', 'node_update_index', $node);

    $extra = module_invoke_all('node_search_result', $node);

    $uri = entity_uri('node', $node);
    $results[] = array(
      'link' => url($uri['path'], array_merge($uri['options'], array('absolute' => TRUE))),
      'type' => check_plain(node_type_get_name($node)),
      'title' => $node->title,
      'user' => theme('username', array('account' => $node)),
      'date' => $node->changed,
      'node' => $node,
      'extra' => $extra,
      'score' => $item->calculated_score,
      'snippet' => search_excerpt($keys, $node->rendered),
      'language' => entity_language('node', $node),
    );
  }
*/
  return $results;
}
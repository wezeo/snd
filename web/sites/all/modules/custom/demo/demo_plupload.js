(function ($, Drupal, window, document, undefined) {
	
	$(function(){
		var uploader = $('#edit-my-plupload-multiimage-field').pluploadQueue();

		  // add remove all files at once button
		  var removeSel = 'a.plupload_remove';
		  $('div.plupload_buttons').append('<a href="#" class="plupload_button plupload_remove">'+Drupal.t('Remove all files')+'</a>');
		  $(removeSel).addClass('display-none-important');

		  
		// show delete icon for single files in the queue after files have been added (override default plupload behaviour)
		uploader.bind('FilesAdded', function(up, files) {			
		  $(removeSel).removeClass('display-none-important');	  
		  
          $.each(files, function(i, file) {
            
			var interval = 100;
			setTimeout(function () {
				var fileEl = $('#'+file.id);
				$('.plupload_file_action *').addClass('display-inline-block-important');
				setTimeout( arguments.callee, interval );
			}, interval);
			  

			// remove all files at once handler
			$('a.plupload_remove').first().click(function(e) {
              e.preventDefault();
              up.splice();
			  $(removeSel).addClass('display-none-important');
            });
            
          });
        });
		
		// show delete icon for single files in the queue after one has been removed (override default plupload behaviour)
	   uploader.bind('FilesRemoved', function(up, files) {
		   var interval = 100;
			setTimeout(function () {
				$('.plupload_file_action *').addClass('display-inline-block-important');
				setTimeout( arguments.callee, interval );
			}, interval);
		   });
		
	});
	
})(jQuery, Drupal, this, this.document);
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

	$(function () {
        multilangInit();
        widgetPoziciaInit();
		initWidgetNamePreview();
    });
	
	
	// ====================================================================================================================================
	
	var NO_OPTION_TEXT = 'please select option',
        ADD_POSITION_TEXT = 'Add position',
        NO_POSITION_TEXT = 'No positions added yet!',
        BASE_PATH;

	
	// MULTILANG PREVIEW WIDGET =======================================================================================================================
	
	// Staci dat pred multilang element <div class="widget-multilang-preview"><span class="empty">default empty message</span></div>
	
	function initWidgetNamePreview() {
		$('.widget-multilang-preview').each(function(){
			var $wnp = $(this),
				defaultHTML = $wnp.html();
			if ($wnp.length) {
				var partsIds = [], //['edit-pre-nominal-titles-', 'edit-first-name-', 'edit-last-name-', 'edit-post-nominal-titles-'],
					languages = ['sk', 'en', 'de'];

				$wnp.next().find('input, textarea').each(function(){
					if (this.id.indexOf('-'+languages[0])>0)
						partsIds.push(this.id.replace(languages[0],''));
				});
				console.log(partsIds);

				function renderNames() {
					var names = {},
						hasValue = false;
					for (var i=0; i<languages.length; i++) {
						var name = [];
						for (var j=0; j<partsIds.length; j++) {
							name.push( getMultilangValue(partsIds[j], languages[i]) );
						}
						names[languages[i]] = name.join(' ').trim();

						if (names[languages[i]]!='')
							hasValue = true;
					}

					if (hasValue) {
						printValues( names );
					}
					else {
						$wnp.html(defaultHTML);
					}					
				}

				// return given language or other by priority sk, en, de
				function getMultilangValue(idPrefix, lang) {
					var values = [];
					for (var i=0; i<languages.length; i++) {
						values[languages[i]] = $('#'+idPrefix+languages[i]).val();
					}
					var retVal = values[lang];
					for (var i=0; i<languages.length; i++) {
						retVal = retVal || values[languages[i]];
					}
					return retVal;
				}


				function printValues( values ) {
					var alreadyPrinted = {},
						rows = [];
					for (var i=0; i<languages.length; i++) {
						var lang = languages[i],
							langs = [];
						if (!alreadyPrinted[lang]) {
							var value = values[lang];
							langs.push(lang);
							for (var j=i+1; j<languages.length; j++) {
								if (value==values[languages[j]]) {
									langs.push(languages[j]);
									alreadyPrinted[languages[j]] = true;
								}
							}
							rows.push('<div><span class="lang">'+langs.join('/')+':</span> <span class="value">'+value+'</span></div>');
						}
					}

					$wnp.html( rows.join('') ).removeClass('count-1 count-2 count-3').addClass('count-'+rows.length);
				}

				$wnp.next().bind('change keydown keypress keyup copy cut', renderNames);
				renderNames();
				setTimeout(renderNames, 1000);
			}
		});
	}
	
	
    // MULTILANG ====================================================================================================================================

    function multilangInit() {
	    $('.multilang-text').each(function (i, fieldset) {
	        var $fieldset = $(fieldset),
	            $fieldsetTitle = $fieldset.find('.fieldset-title'),
	            $inputs = $fieldset.find('input, textarea'),
                textareas = !!$inputs.not('input').length,
                isRichText = $fieldset.hasClass('rich-textarea'),
	            $inputWraps,
	            $description = $fieldset.find('.fieldset-description'),
                $customWraper = $('<div class="custom-wrappper multilang-text">'+
                                    '<div class="form-item">'+
                                        '<div class="multi-wrapper">' +
                                            '<span class="lang-tabs">'+
                                                '<span class="lang-label lang-label-sk selected">sk</span>'+
                                                '<span class="lang-label lang-label-en">en</span>'+
                                                '<span class="lang-label lang-label-de">de</span>'+
                                                '<span class="lang-label lang-label-all">all</span>'+
                                            '</span>'+
                                            '<label>' + $fieldsetTitle.html() + '</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'),
                $tabs = $customWraper.find('.lang-label'),
                $formItem = $customWraper.find('.form-item');

            $inputs
                .each(function (i, input) {
                    var $input = $(input);

                    if(!textareas)
                        $input.wrap('<div></div>');

                    if($input.hasClass('error'))
                        $tabs.eq(i).addClass('error');
                    if($input.val()){
                        $input.attr('data-prev-value', true);
                        $tabs.eq(i).addClass('has-value');
                    }
                });

            // reverse, because jQuery parents method returns in reversed order
            $inputWraps = isRichText ? [].reverse.call($inputs.parents('.text-format-wrapper')) : $inputs.parent();

            if(isRichText)
                $inputWraps
                    .find('.form-type-textarea > label')
                    .hide();

            $formItem
                .addClass(textareas ? 'form-type-textfield' : 'form-type-textarea')
                .children('.multi-wrapper')
                    .append($inputWraps);

            $inputWraps
                .wrapAll('<div class="inputs-wrap"></div>')
                .addClass('input-wrap')
                .hide()
                .eq(0)
                    .show();

            multilangInitShowHideLogic($tabs, $inputWraps);

            if($description.length)
                $description
                    .removeClass('fieldset-description')
                    .addClass('description')
                    .appendTo($customWraper.children('.form-item'));

            $fieldset.replaceWith($customWraper);

            // console.log('$inputs', $inputs);
            $inputs.bind('propertychange input', multilangInputChange);
            $customWraper.click(multilangClickLangTabs);
        });

        $('input.error').first().focus();

        CKEDITOR.on( 'instanceReady', function( evt ) {
            var editor = evt.editor,
                $inputWraps = $(editor.element.$).parents('.inputs-wrap').children(),
                $tabs = $inputWraps.parents('.form-item').find('.lang-tabs').children();

            $(editor.document.$)
                .contents()
                .find("body")
                    .bind('blur focus cut paste', callHandler);

            editor.on('key', callHandler); // because CKEditor somehow blocks keydown event when deleting all content at once

            // initial setup
            multilangCKEditorEventHandler(editor);
            setTimeout(multilangInitShowHideLogic, 0, $tabs, $inputWraps);

            function callHandler() {
                setTimeout(multilangCKEditorEventHandler, 0, editor);
            }
        } );
    }
    
    function multilangCKEditorEventHandler(editor) {
        var data = editor.getData(),
            $inputWrap = $(editor.element.$).parents('.input-wrap'),
            $tabs = $inputWrap.parents('.form-item').find('.lang-tabs').children();

        if(data && data.match(/\w/) && data != '<p>&nbsp;</p>')
            // has value
            $tabs.eq($inputWrap.index()).addClass('has-value');
        else
            $tabs.eq($inputWrap.index()).removeClass('has-value');

    }
    
    function multilangClickLangTabs(e) {
        var $tab = $(e.target);
        if(!$tab.hasClass('lang-label'))
            return;

        var $parent = $tab.parent(),
            $inputWraps = $parent.siblings().find('.input-wrap');

        if($tab.hasClass('lang-label-all'))
            $inputWraps
                .show()
                .eq(0)
                .children()
                    .focus();
        else
            $inputWraps
                .hide()
                .eq($tab.index())
                    .show()
                        .children()
                        .focus();

        $parent
            .children()
                .removeClass('selected');
        $tab.addClass('selected');
    }

    function multilangInitShowHideLogic($tabs, $inputWraps) {
        if($tabs.filter('.has-value').length)
            $inputWraps.hide().eq(
                $tabs
                    .removeClass('selected')
                    .filter('.has-value')
                    .first()
                    .addClass('selected')
                    .index()
            ).show();

        if($tabs.filter('.error').length)
            $inputWraps.hide().eq(
                $tabs
                    .removeClass('selected')
                    .filter('.error')
                    .first()
                    .addClass('selected')
                    .index()
            ).show();
    }

    function multilangInputChange(e) { // because change method fires on blur
        if(!e.target.id)
            return;

        var valueChanged = false;

        if (e.type=='propertychange') // http://stackoverflow.com/a/17384341
            valueChanged = e.originalEvent.propertyName=='value';
        else
            valueChanged = true;
            
        if (valueChanged) {
            var $input = $(e.target),
                val = $input.val(),
                prev = $input.attr('data-prev-value'),
                $tabs = $input.parents('.form-item').find('.lang-tabs').children();

            // on change handle has-value class on tab
            if(val && !prev){
                $input.attr('data-prev-value', true);
                $tabs.eq($input.parent().index()).addClass('has-value');
            }else if(!val && prev){
                $input.attr('data-prev-value', '');
                $tabs.eq($input.parent().index()).removeClass('has-value');
            }

            // handle "all" tab has-value class
            if($tabs.filter('.has-value').not('.lang-label-all').length > 2)
                $tabs.last().addClass('has-value');
            else
                $tabs.last().removeClass('has-value');

            // clear error class on change
            if($input.hasClass('error')){
                $input.removeClass('error');
                $tabs.eq($input.parent().index()).removeClass('error');
            }
        }

    }


    // WIDGET-POZICIA ====================================================================================================================================

    function widgetPoziciaInit(){
        BASE_PATH = Drupal.settings.basePath + Drupal.settings.pathPrefix;

        $('.widget-pozicie').each(function(i, widget){
            var $widget = $(widget),
                $input = $widget.find('input'),
                $list = $('<ul class="list"><li class="list-item list-item-no-position">' + NO_POSITION_TEXT + '</li></ul>'),
                $noPosition = $list.find('.list-item-no-position'),
                $addSection = $('<div class="add-ui">' +
                    '<div class="add-ui-selects">' +
                        '<select class="form-select add-ui-dummy"><option value="">' + NO_OPTION_TEXT + '</option></select>' +
                    '</div>' +
                    '<button type="button" class="add-ui-button button" disabled>' + ADD_POSITION_TEXT + '</button>' +
                    '</div>');

            if($input.val()){
                $.getJSON(BASE_PATH + 'api/person-categories/paths?tids=' + $input.val().replace(/;/g,','), function(positions){
                    widgetPoziciaFillPositions($list, positions);
                    widgetPoziciaUpdateInput($widget);
                });
                $noPosition.hide();
            }
            
            $widget
                .delegate( '.list-item-delete', 'click', function (e) {
                    e.preventDefault();

                    $(this).parent().remove();

                    if($list.children().length == 1) // NO_POSITION_TEXT only
                        $noPosition.show();

                    widgetPoziciaUpdateInput($widget);
                } )
                .find('label')
                    .after($list)
                .end();
            
            $input
                .hide()
                .before($addSection);

            $addSection.find('.add-ui-button')
                .click(function (e) {
                    e.preventDefault();

                    var listItemText = '',
                        positionID = 'id?';

                    $noPosition.hide();
                    $addSection.find('.form-select').each(function (i, select) {
                        positionID = select.value;
                        listItemText += (listItemText ? ' / ' : '') + $(select).children(':selected').text();
                    });

                    widgetPoziciaAddItemToList($list, positionID, listItemText);
                    widgetPoziciaUpdateInput($widget);

                    $widget.find('.form-select')
                        .first()
                            .val('')
                            .nextAll()
                                .remove();

                    $widget.find('.add-ui-button').attr('disabled', true);
                });

        });
        $.getJSON(BASE_PATH + 'api/person-categories', widgetPoziciaListReady);
    }
    
    function widgetPoziciaAddItemToList($list, positionID, listItemText) {
        $('<li class="list-item button"><a href="#" class="list-item-delete">remove</a></li>')
            .prepend(listItemText)
            .attr('data-position-id', positionID)
            .appendTo($list);
    }

    function widgetPoziciaUpdateInput($widget) {
        var result = '';
        $widget.find('[data-position-id]').each(function (i, item) {
            result += (result ? ';' : '') + $(item).attr('data-position-id');
        });
        $widget.find('input').val(result);
    }

    function widgetPoziciaFillPositions($list, positions) {
        $.each(positions, function (i, position) {
            if(position.name === null)
                return;
            
            var listItemText = '';

            if(position.path)
                $.each(position.path, function (ii, pathItem) {
                    listItemText += (listItemText ? ' / ' : '') + pathItem;
                });
            listItemText += (listItemText ? ' / ' : '') + position.name;

            widgetPoziciaAddItemToList($list, position.tid, listItemText);
        });
    }

    function widgetPoziciaListReady(positions) {
        $('.widget-pozicie').each(function(i, widget){
            var $widget = $(widget);

            $widget
                .find('.add-ui-dummy')
                    .remove();

            widgetPoziciaAddSelect($widget, positions);
        });
    }

    function widgetPoziciaAddSelect($widget, data) {
        var $select = $('<select class="form-select"><option value="">' + NO_OPTION_TEXT + '</option></select>'),
            $addButton = $widget.find('.add-ui-button');

        $.each(data, function(i, item) {
            // console.log('option', item);
            $select.append($("<option></option>")
                .attr("value",item.tid)
                .attr("data-has-child", !!item.hasChild)
                .text(item.name));
        });
        // console.log('-------------------------------------------------------------------');

        if(data.length == 1){
            $select
                .children()
                    .first() // NO_OPTION_TEXT
                        .remove();
            $addButton.removeAttr('disabled');
        }

        $select
            .appendTo($widget.find('.add-ui-selects'))
            .change(function () {
                var $currentSelect = $(this);

                $currentSelect
                    .nextAll()
                        .remove();

                if(!this.value) // NO_OPTION case
                    return;

                if($currentSelect.children(':selected').attr('data-has-child') === 'true'){
                    $.getJSON(BASE_PATH + 'api/person-categories?parent=' + $currentSelect.val(), function (positions) {
                        widgetPoziciaAddSelect($widget, positions);
                    });
                    $addButton.attr('disabled', true);
                }else{
                    $addButton.removeAttr('disabled');
                }

            });
    }

})(jQuery, Drupal, this, this.document);
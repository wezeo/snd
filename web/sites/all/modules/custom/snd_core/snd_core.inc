<?php


// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------------------------------------
// --- Force not to use @import css on DEV environment ------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------

function snd_core_css_alter(&$css) {
  // use it only on DEV server (dev.wezeo.com)
  if (strpos($_SERVER['SERVER_NAME'],'wezeo')>=0) {
	  $preprocess_css = variable_get('preprocess_css', TRUE);
	  if (!$preprocess_css) {
		// For each item, don't allow preprocessing to disable @import.
		foreach ($css as &$item) {
		  if (file_exists($item['data'])) {
			$item['preprocess'] = FALSE;
		  }
		}
	  }
  }
}


// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------


function snd_core_page_program( $season=null, $month=null, $artisticBody ) {

	$season = snd_core_parse_season_year( $season );

	// ak nie je nastaveny mesiac
	if ($month==null) {
		// ak aktualna sezona, tak nastavi na prvy mesiac
		if ($season==date("Y"))
			$month = date("n");
		// inak vzdy na prvy mesiac sezony (september)
		else
			$month = 9;
	}
//print_r($artisticBody);exit;
	$html = '
		<div class="program-block">
			'.snd_core_fragment_program_filter($artisticBody).'
			'.snd_core_fragment_program_season($season).'
			'.snd_core_fragment_program_month($month).'

			'.snd_core_fragment_program_calendar($season, $month, $artisticBody).'
		</div>
	';
	return $html;
}

function snd_core_fragment_program_filter($artisticBody) {

	$artisticBodyArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_ARTISTIC_BODY_VID));
	foreach($taxonomy as $term) {
		$artisticBodyArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$placeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, 0, 1));
	foreach($taxonomy as $term) {
		$placeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$performanceTypeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PERFORMANCE_TYPE_VID));
	foreach($taxonomy as $term) {
		$performanceTypeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$targetGroupArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_TARGET_GROUP_VID));
	foreach($taxonomy as $term) {
		$targetGroupArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$stateArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_STATE_VID));
	foreach($taxonomy as $term) {
		$stateArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$html = '
			<form class="program-search-by-artist" onsubmit="return false">
				<label for="search-artist">'.t('Search by artist').'</label>
				<input type="text" placeholder="'.t('name and/or surname of artist').'" name="search-artist" id="search-artist" />
				<input type="submit" value="Vyhľadať" />
			</form>

			<div class="program-artistic-body">
				'.snd_core_fragment_program_filter_criteria( array(
					'name' => 				t('Artistic body'),
					'no-title-checkbox' => 	true,
					'class-name' => 		'umelecky-subor',
					'checked-value' => 		$artisticBody,
					'options' => 			$artisticBodyArr
				) ).'
			</div>

			<div class="program-filter">
				<button>'.t('Program filter').'</button>

			</div>
			<div class="program-criterias">

					'.snd_core_fragment_program_filter_criteria( array(
						'name' => 		t('Performance location'),//Miesto predstavenia',
						'class-name' => 'miesto-predstavenia',
						'options' => 	$placeArr
					) ).'

					'.snd_core_fragment_program_filter_criteria( array(
						'name' => 		t('Performance type'),//Typ predstavenia',
						'class-name' => 'typ-predstavenia',
						'options' => 	$performanceTypeArr
					) ).'

					'.snd_core_fragment_program_filter_criteria( array(
						'name' => 		t('Target group'),//Cieľová skupina',
						'class-name' => 'cielova-skupina',
						'options' => 	$targetGroupArr
					) ).'

					'.snd_core_fragment_program_filter_criteria( array(
						'name' => 		t('Performance status'),//Stav predstavenia',
						'class-name' => 'stav',
						'options' => 	$stateArr
					) ).'

			</div>
	';
	return $html;
}

function snd_core_fragment_program_filter_criteria( $config ) {
		$html = '
					<div class="program-criterium program-criterium-'.$config['class-name'].'">
						<h3>'.(!$config['no-title-checkbox'] ? '<input type="checkbox" checked="checked" name="program-criterium-toggle" id="program-criterium-'.$config['class-name'].'-toggle" /><span class="space"> </span>' : '').'<label for="program-criterium-'.$config['class-name'].'-toggle">'.$config['name'].'</label></h3>
						<div class="program-criterium-options">
		';

		foreach ($config['options'] as $option) {
			$html .= '
							<div class="program-criterium-option"><input type="checkbox"'.(!isset($config['checked-value']) || !$config['checked-value'] || 'tid-'.$config['checked-value']==$option['class-name'] ? ' checked="checked"' : '').' name="program-criterium-'.$option['class-name'].'" id="program-criterium-'.$option['class-name'].'" value="criterium-'.$option['class-name'].'" /><span class="space"> </span><label for="program-criterium-'.$option['class-name'].'">'.$option['name'].'</label></div>';
		}

		$html .= '
						</div>
					</div>
		';
		return $html;
}

// $type - specifies the page on which the select is used, default is program page ($type = null)
function snd_core_fragment_program_season($season, $type = null) {

	$addClass = '';
	if($type=='repertoir')
		$addClass = '-repertoir';
	
	//	create filter select element
	$seasons = array();
	$current_season_year = snd_core_parse_season_year();

	//for($i=SND_FIRST_YEAR;$i<=$current_season_year;$i++)
	for($i=$current_season_year; $i>=SND_FIRST_YEAR; $i--)
		$seasons[] = $i.'/'.($i+1);

	$html = '
			<div class="season">
				<label for="program-season'.$addClass.'">'.t("Season").'</label>
				<select name="program-season'.$addClass.'" id="program-season'.$addClass.'">';

	foreach ($seasons as $option) {

		$optionUrl = intval($option);

		$html .= '
					<option value="'.$option.'"'.($season==$optionUrl ? ' selected' : '').'>'.$option.'</option>';
	}

	$html .= '
				</select>
			</div>
	';
	return $html;
}

function snd_core_fragment_program_month( $month ) {

	$months = array(t("September"), t("October"), t("November"), t("December"), t("January"), t("February"), t("March"), t("April"), t("May"), t("June"));
	$starts = 9;
	$html = '
			<div class="month">
				<label for="program-month-09">'.t("Month").'</label>
				<span class="months">';

	$index = 0;
	foreach ($months as $option) {
		$monthNumber = (($starts + $index - 1)%12)+1;
		$monthZerofill = sprintf('%02d', $monthNumber);
		$html .=  '
					<span class="program-month"><input type="radio"'.($monthNumber==$month ? ' checked' : '').' name="program-month" id="program-month-'.$monthZerofill.'" value="'.$monthZerofill.'" /><span class="space"> </span><label for="program-month-'.$monthZerofill.'" title="'.$option.'">'.mb_substr($option,0,3).'<span class="long">'.mb_substr($option,3).'</span></label></span>';
		$index++;
	}

	$html .= '
				</span>
			</div>';

	return $html;
}

function snd_core_sort_function($a, $b) {
	$a = $a['date'];
	$b = $b['date'];
    return ($a == $b) ? 0 : ($a < $b) ? -1 : 1;
}

// $month 1..12
function snd_core_fragment_program_calendar( $year, $month, $artisticBody, $hall='', $targetGroup='' ) {

	// convert $month to number (we dont want leading zeros)
	$month *= 1;

	// oznacenie sezony
	$seasonYear = $year;

	// ak je mesiac JAN az JUN, tak skutocny rok je o jeden vyssi ako je sezona
	if($month <= 6) {
		$year += 1;
	}

	$sizePatterns = array(
						array(''),	// 0 items in day
						array('triple'),	// 1 item in day
						array('single', 'double'),	// 2 items in day
						array('single', 'single', 'single'),	// 3 items in day
						array('single', 'double', 'single', 'double'),	// 4 items in day
						array('single', 'single', 'single', 'single', 'double'),	// 5 items in day
						array('single', 'single', 'single', 'single', 'single', 'single'),	// 6 items in day
						array('single', 'double', 'single', 'single', 'single', 'double', 'single'),	// 7 items in day
					);

	$html = '
			<div class="program-calendar">';

	$weekdays = array(t('Sunday'), t('Monday'), t('Tuesday'), t('Wednesday'), t('Thursday'), t('Friday'), t('Saturday'));
	$daysInGivenMonth = date('t', mktime(0, 0, 0, $month, 1, $year));

	// nacitam predstavenia
	$predstavenia = snd_core_get_predstavenia_for_calendar( $seasonYear, $month, $artisticBody, $hall, $targetGroup);
	$predstaveniaCount = count($predstavenia);
	$predstaveniaIndex = 0;

	// create date objects
	for ($i=0; $i<$predstaveniaCount; $i++)
		$predstavenia[$i]['date'] = new DateTime( $predstavenia[$i]['date_from'] );

	// sort by date
	//usort($predstavenia, 'snd_core_sort_function');

	$today = date("Y-m-d");

	for ($day=1; $day<=$daysInGivenMonth; $day++) {

		$weekdayIndex = date('w', mktime(0, 0, 0, $month, $day, $year));
		$actualDate = date("Y-m-d", mktime(0, 0, 0, $month, $day, $year));

		$weekday = $weekdays[$weekdayIndex];
		$isWeekend = ($weekdayIndex==0 || $weekdayIndex==6); // Sunday or Saturday
		$date = $day . '.' . $month . '.' . $year;

		$performancesInOneDay = array();

		foreach($predstavenia as $predstavenie) {

			$d = explode(' ', $predstavenie['date_from']);
			if($d[0] == $actualDate) {
				$performancesInOneDay[] = $predstavenie;
			}
		}

		$count = count($performancesInOneDay);
		$rowCount = ceil($count/3);
		// ak vychadza viac ako na 2 riadky pre jeden den, tak urobi classname
		$rowCountClass = 'row-count-'.$rowCount;

		$isToday    = ($year==date("Y") && $month==date("n") && $day==date("j"));
		$currentDay = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
		$isFuture   = $currentDay>$today;
		$isPast     = $currentDay<$today;

		$html .= '
				<div class="calendar-day-row'.($isPast ? ' past' : '').($isToday ? ' today' : '').($isFuture ? ' future' : '').($count==0 ? ' no-events' : '').'">
					<div class="calendar-day-group'.($isWeekend ? ' calendar-day-weekend' : '').' '.$rowCountClass.'">
						<div class="calendar-day" title="'.$date.'">
							<div class="day-number">'.$day.'.</div>
							<div class="day-name">'.$weekday.'</div>
						</div>
					</div>
					<div class="calendar-events">';

		if ($count==0)
			$html .= '
						<p class="no-events">'.t('On this day there are no performances.').'</p>';

		// analyze and print performances in one day
		$sizePattern = $sizePatterns[$count];
		for ($i=0; $i<$count; $i++)
			$html .= snd_core_fragment_program_performance( $performancesInOneDay[$i], $sizePattern[$i], $weekday );

		$html .= '
					</div>
				</div>';
	}

	$html .= '
			</div>';

	return $html;
}

function snd_core_get_predstavenia_for_calendar( $year, $month, $artisticBody, $hall='', $targetGroup='' ) {

	// performance table
	$pTab = snd_entity__performance_config_get_table_name();
	// performance table alias
	$pTabAlias = snd_entity__performance_config_get_table_alias();

	// inscenation table
	$iTab = snd_entity__inscenation_config_get_table_name();
	// inscenation table alias
	$iTabAlias = snd_entity__inscenation_config_get_table_alias();

	$query = db_select( $pTab, $pTabAlias )
				->fields($pTabAlias);

	$query->leftJoin( $iTab, $iTabAlias, $pTabAlias.'.inscenation_id = '.$iTabAlias.'.id');
	$fields = theme_snd_entity_common_populate_by_lang('title_');
	$fields = array_merge($fields, theme_snd_entity_common_populate_by_lang('teaser_'));
	$fields[] = 'artistic_body';
	$fields[] = 'hosted_in_snd';
	$fields[] = 'target_group';
	$fields[] = 'duration_in_minutes';
	$fields[] = 'image';
	$fields[] = 'temp_image_url';
	$fields[] = 'season_year';
	$query->fields($iTabAlias, $fields);

	if($artisticBody) {
		$query->condition($iTabAlias.'.artistic_body', $artisticBody);
	}
	if($hall) {
		$query->condition($iTabAlias.'.hall', $hall);
	}
	if($targetGroup) {
		$query->condition($iTabAlias.'.target_group', $targetGroup);
	}
	$query->condition($iTabAlias.'.active', 1);
	$query->condition($pTabAlias.'.active', 1);
	$query->condition($iTabAlias.'.deleted', 0);
	$query->condition($pTabAlias.'.deleted', 0);
	$query->condition($pTabAlias.'.hide_from_program', 0);
	$query->condition($iTabAlias.'.season_year', $year);
	$query->where('MONTH('.$pTabAlias.'.date_from) = :month', array(':month' => $month));
	$query->orderBy('date_from', 'ASC');

    $result = $query->execute();

	if($result->rowCount() == 0) {
		return false;
	}

	$items = array();
	$imageFids = array();
	foreach($result as $record) {

		$record = (array) $record;
		$items[] = snd_core__inscenation_and_performance_add_taxonomy_terms($record);
		
		// z obrazkov pozbieram idecka (neskor nacitam naraz)
		if($record['image'] != 0) {
			$imageFids[] = $record['image'];
		}
	}

	// nacitam obrazky naraz pre vsetky osoby
	$files = count($imageFids)>0 ? file_load_multiple($imageFids) : array();

	// doplnim obrazky do predstaveni
	foreach($items as &$item) {

		// image
		$fid = $item['image'];
		$item['#image'] = ($fid && isset($files[$fid])) ? $files[$fid] : null;
	}
	
	return $items;
}

function snd_core_fragment_program_performance( $performance, $classNames, $weekdayName ) {

	global $language;
	$lang = $language->language;

	//die_r($performance);
	// k datumu a casu od sopocitam datum a cas do
	$dateFrom = $performance['date_from'];
	$dateTo = date("Y-m-d H:i:s", strtotime($dateFrom." + ".$performance['duration_in_minutes']." minute"));

	$printDateFrom = date("d.m.Y", strtotime($dateFrom));
	$printTimeFrom = date("H:i", strtotime($dateFrom));
	$printTimeTo = date("H:i", strtotime($dateTo));

	$placeArr = array();
	$placeClassArr = array();
	if(isset($performance['place']['name'])) {
		$placeArr[] = $performance['place']['name'];
		$placeClassArr[] = $performance['place']['class-name'];
	}
	if(isset($performance['hall']['name'])) {
		$placeArr[] = $performance['hall']['name'];
		$placeClassArr[] = $performance['hall']['class-name'];
	}
	$place = implode(', ', $placeArr);
	$placeClass = implode(' ', $placeClassArr);

	// image
	if (!$performance['#image'])
		$imageHTML = '<div class="image no-image"></div>';
	else
		$imageHTML = '<div class="image">'.theme('image_style', array('path' => $performance['#image']->uri, 'style_name' => 'program_image')).'</div>';

	$performanceClasses = array();
	$performanceClasses[] = 'performance';
	$performanceClasses[] = $classNames;
	if(isset($performance['artistic_body']['tid']) && $performance['artistic_body']['tid']) {
		$performanceClasses[] = $performance['artistic_body']['class-name'];
		$performanceClasses[] = 'criterium-tid-'.$performance['artistic_body']['tid'];
	}
	if(isset($performance['place']['tid']) && $performance['place']['tid']) {
		$performanceClasses[] = 'criterium-tid-'.$performance['place']['tid'];
	}
	if(isset($performance['hall']['tid']) && $performance['hall']['tid']) {
		$performanceClasses[] = 'criterium-tid-'.$performance['hall']['tid'];
	}
	if($performance['target_group']){
		$groups = explode(';', $performance['target_group']);
		foreach($groups as $group) 
			$performanceClasses[] = 'criterium-tid-'.$group;
	}
	if($performance['type']) {
		$type_conversion = array(
			// Verejná generálka => Predpremiéra / Generálka
			308 => 58,
			// Predpremiéra => Predpremiéra / Generálka
			309 => 58,
			
			// Premiéra => Premiéra / Obnovená premiéra
			311 => 59,
			// Obnovená premiéra => Premiéra / Obnovená premiéra
			313 => 59,
			
			// Repríza => Repríza / Matiné 
			312 => 60,
			// Matiné => Repríza / Matiné 
			310 => 60,
			
			// Derniéra => Derniéra
			314 => 61,
		);
		$performanceClasses[] = 'criterium-tid-'.$type_conversion[ $performance['type'] ];
	}
	// hostovanie v SND
	if($performance['hosted_in_snd']==1) {
		$performanceClasses[] = 'criterium-tid-61';
	}
	// zdarma
	if($performance['for_free']==1) {
		$performanceClasses[] = 'criterium-tid-80';
	}
	if(isset($performance['state']['tid']) && $performance['state']['tid']) {
		$performanceClasses[] = 'criterium-tid-'.$performance['state']['tid'];
	}

	$title = snd_entity_common_get_multilang_string($performance, 'title_', $lang);
	$teaser = snd_entity_common_get_multilang_string($performance, 'teaser_', $lang);
	$urlAlias = snd_entity_common_get_multilang_string($performance, 'url_alias_', $lang);

	$url = snd_entity__performance_config_get_public_url_names()[$lang] . '/' . $performance['id'] . '/' . $urlAlias;
	$link = url($url);

	$html = '
						<div id="performance-'.$performance['id'].'" class="'.implode(' ', $performanceClasses).'">
							'.$imageHTML.'
							<div class="info">
								<div class="title" title="'.$title.'"><label>Názov:</label><span class="space"> </span><span class="value">'.$title.'</span></div>
								<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span>'.((isset($performance['artistic_body']['name']))? '<span class="value">'.$performance['artistic_body']['name'].'</span>' : '').'</div>
								<div class="author"><label>Autor:</label><span class="space"> </span>'.((isset($performance['author']))? '<span class="value">'.$performance['author'].'</span>' : '').'</div>
								<div class="type"><label>Typ predstavenia:</label><span class="space"> </span>'.((isset($performance['#type']['name']))? '<span class="value">'.$performance['#type']['name'].'</span>' : '').'</div>
								<div class="state"><label>Stav:</label><span class="space"> </span>'.((isset($performance['state']['name']))? '<span class="value">'.$performance['state']['name'].'</span>' : '').'</div>
								<div class="date"><label>Dátum konania:</label><span class="space"> </span><span class="value"><span class="weekday">'.$weekdayName.'</span> <span class="on-date">'.$printDateFrom.'</span> <span class="time-from">'.$printTimeFrom.'</span> <span class="time-to">'.$printTimeTo.'</span></span></div>
								<div class="place '.$placeClass.'" title="'.$place.'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$place.'</span></div>
							</div>
							<div class="description"><span>'.$teaser.'</span></div>
							<div class="detail-link"><a href="'.$link.'">&nbsp;</a></div>
						</div>';
	return $html;
}


function snd_core_parse_season_year( $season = 0) {
	// konvertujeme sezonu z tvaru '2016-2017' iba na cislo 2016
	// ak nie je cislo, vysledok bude 0
	$season = intval($season);

	// ak nie je nastavena sezona (alebo nie je cislo)
	// tak vrati aktualny rok 
	$currentYear = date("Y");
	$currentMonth = date("n");
	
	// iba ak nie je nastavena sezona
	if ($season==0 /*|| $season<SND_FIRST_YEAR || $season>$currentYear*/) {
		// nastavi aktualny rok
		$season = date("Y");
		
		// ale POZOR, ak je mesiac jul az december, vrati aktualny rok, ak januar az jun, tak rok spat
		if ($currentMonth<=6)
			$season--;
	}	

	return $season;
}

function snd_core_get_artistic_body_from_URL() {
	// ak v URL /program?opera
	// sk:  cinohra     |  opera  |  balet    |  ine
	// en:  drama       |  opera  |  ballet   |  other
	// de:  schauspiel  |  oper   |  ballett  |  anderes
	$artisticBody = '';
	if ($_SERVER['QUERY_STRING']) {
		// hodnota hned za otaznikom musi bit umelecky subor
		$arguments = explode('&', $_SERVER['QUERY_STRING']);
		$umeleckySubor = $arguments[0];
		// CINOHRA
		if ($umeleckySubor=='cinohra' || $umeleckySubor=='drama' || $umeleckySubor=='schauspiel')
			$artisticBody = 70;
		// OPERA
		if ($umeleckySubor=='opera' || $umeleckySubor=='oper')
			$artisticBody = 71;
		// BALET
		if ($umeleckySubor=='balet' || $umeleckySubor=='ballet' || $umeleckySubor=='ballett')
			$artisticBody = 72;
		// INE
		if ($umeleckySubor=='ine' || $umeleckySubor=='other' || $umeleckySubor=='anderes')
			$artisticBody = 73;
	}

	return $artisticBody;
}

// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------



function snd_core_page_repertoar( $season=null, $artisticBody ) {

	global $language;
	$lang = $language->language;

	$season = snd_core_parse_season_year( $season );

	$html = '
		<div class="repertoar-block">
			'.snd_core_fragment_program_season($season, 'repertoir').'

			<div class="repertoar-inscenations">';

	$inscenacie = snd_entity__api_inscenation_getBySeasonYear_for_repertoar( $season, $artisticBody);
	$inscenacie = snd_core_inscenacie_add_taxonomy_terms( $inscenacie );
	
	//die_r($inscenacie);
	
	foreach ($inscenacie as $inscenation) {
		
		$place = $inscenation['#place'] . (isset($inscenation['#hall'])? ', '.$inscenation['#hall'] : '');
		$placeClass = 'tid-'.$inscenation['place']['tid'] . (isset($inscenation['hall']['tid'])? ' tid-'.$inscenation['hall']['tid'] : '');
		
		// image
		if (!$inscenation['image'])
			$imageHTML = '<div class="image no-image"></div>';
		else
			$imageHTML = '<div class="image">'.theme('image_style', array('path' => $inscenation['image']->uri, 'style_name' => 'inscenation_main_detail')).'</div>';
			
		$inscenation['description'] = snd_entity_common_get_multilang_string($inscenation, 'description_', $lang);
		 
		$detailLink = url($inscenation['#link']);
		
		$authors = '';
		if (array_key_exists('authors', $inscenation) && !empty($inscenation['authors'])) {
			$authorsArr = array();
			$inscenationAuthorsArr = $inscenation['authors'];
			
			foreach($inscenationAuthorsArr as $author)
				$authorsArr[] =  $author['name'];				
			$authors = join(', ',$authorsArr);
		}
		

		$html .= '
				<div class="inscenacia tid-'.$inscenation['artistic_body']['tid'].'">
					'.$imageHTML.'
					<div class="info">
						<div class="author"><label>Autor:</label><span class="space"> </span><span class="value">'.$authors.'</span></div>
						<div class="title" title="'.$inscenation['#title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$inscenation['#title'].'</span></div>
						<div class="artistic-body"><label>'.t('Artistic body:').'</label><span class="space"> </span><span class="value">'.$inscenation['#artistic_body'].'</span></div>
						<div class="place '.$placeClass.'" title="'.$place.'"><label>'.t('Event place:').'</label><span class="space"> </span><span class="value">'.$place.'</span></div>
					</div>
					<div class="description"><span>'.strip_tags($inscenation['description'], '<p>').'</span></div>
					<div class="detail-link"><a href="'.$detailLink.'">&nbsp;</a></div>
				</div>';
	}

	$html .= '
			</div>
		</div>
	';
	return $html;
}

function snd_core_page_umelecky_subor( $artisticBody ) {

	$data = snd_core_get_umelecky_subor($artisticBody);

	$html = '<div class="artistic-body">';

	foreach($data as $category) {

		$html .= '<div class="category category-'.$category['tid'].'"><div class="category-name-wrapper"><h2 class="category-name">'.$category['name'].'</h2></div><div class="list-wrapper">';

		foreach($category['data'] as $list) {

			$html .= '<div class="list list-'.$list['tid'].'"><div class="list-name-wrapper"><h3 class="list-name">'.$list['name'].'</h3></div><div class="item-wrapper">';

			foreach($list['data'] as $item) {

				$info = snd_core_get_profil_by_id($item);

				$html .= '<div id="item-'.$info['id'].'" class="item"><a href="/profil/'.$info['id'].'">
					<div class="image"><img src="/sites/default/files/avatar.svg"></div>
					<div class="name"><span class="value">'.$info['name'].'</span></div>
				</a></div>';
			}
			$html .= '</div></div>';
		}

		$html .= '</div></div>';
	}

	$html .= '</div>';

	return $html;
}

function snd_core_predstavenia_add_taxonomy_terms($predstavenia) {

	// nacitam vsetky taxonomy
	$artisticBodyArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_ARTISTIC_BODY_VID));
	foreach($taxonomy as $term) {
		$artisticBodyArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$placeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, 0, 1));
	foreach($taxonomy as $term) {
		$placeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$hallArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, TERM_NOVA_BUDOVA_TID, 1));
	foreach($taxonomy as $term) {
		$hallArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$performanceTypeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PERFORMANCE_TYPE_VID));
	foreach($taxonomy as $term) {
		$performanceTypeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$stateArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_STATE_VID));
	foreach($taxonomy as $term) {
		$stateArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$taxonomies = array(
		'artisticBody' => $artisticBodyArr,
		'place' => $placeArr,
		'hall' => $hallArr,
		'performanceType' => $performanceTypeArr,
		'targetGroup' => $targetGroupArr,
		'state' => $stateArr,
	);

	// pre jeden prvok (predstavenie) aj pre pole predstaveni
	if(!isset($predstavenia[0]))
		$predstavenia = array($predstavenia);
		
	foreach($predstavenia as &$predstavenie)
			$predstavenie = add_taxonomy_terms_predstavenie($predstavenie, $taxonomies);

	return $predstavenia;
}

function add_taxonomy_terms_predstavenie($predstavenie, $taxonomies) {

	// pridam data k tid-kam
	$predstavenie['#type'] = snd_core_tid_to_taxonomy_term($predstavenie['type'], $taxonomies['performanceType']);
	$predstavenie['state'] = snd_core_tid_to_taxonomy_term($predstavenie['state'], $taxonomies['state']);
	$predstavenie['inscenation']['artistic_body'] = snd_core_tid_to_taxonomy_term($predstavenie['inscenation']['artistic_body'], $taxonomies['artisticBody']);
	$predstavenie['inscenation']['place'] = snd_core_tid_to_taxonomy_term($predstavenie['inscenation']['place'], $taxonomies['place']);
	$predstavenie['inscenation']['hall'] = snd_core_tid_to_taxonomy_term($predstavenie['inscenation']['hall'], $taxonomies['hall']);

	return $predstavenie;
}
		
function snd_core_inscenacie_add_taxonomy_terms($inscenacie) {

	// nacitam vsetky taxonomy
	$artisticBodyArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_ARTISTIC_BODY_VID));
	foreach($taxonomy as $term) {
		$artisticBodyArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$placeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, 0, 1));
	foreach($taxonomy as $term) {
		$placeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$hallArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, TERM_NOVA_BUDOVA_TID, 1));
	foreach($taxonomy as $term) {
		$hallArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$targetGroupArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_TARGET_GROUP_VID));
	foreach($taxonomy as $term) {
		$targetGroupArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
	}

	$taxonomies = array(
		'artisticBody' => $artisticBodyArr,
		'place' => $placeArr,
		'hall' => $hallArr,
		'targetGroup' => $targetGroupArr,
	);

	// pre jeden prvok (inscenacia) aj pre pole inscenacii
	if(!isset($inscenacie[0]))
		$inscenacie = array($inscenacie);
		
	foreach($inscenacie as &$inscenacia)
			$inscenacia = add_taxonomy_terms_inscenacia($inscenacia, $taxonomies);
	
	return $inscenacie;
}


function add_taxonomy_terms_inscenacia($inscenacia, $taxonomies) {

	// pridam data k tid-kam
	$inscenacia['artistic_body'] = snd_core_tid_to_taxonomy_term($inscenacia['artistic_body'], $taxonomies['artisticBody']);
	$inscenacia['place'] = snd_core_tid_to_taxonomy_term($inscenacia['place'], $taxonomies['place']);
	$inscenacia['hall'] = snd_core_tid_to_taxonomy_term($inscenacia['hall'], $taxonomies['hall']);

	return $inscenacia;
}


/*
 * PRIDAVAM CISELNIKOVE HODNOTY K POLIAM Z TAXONOMIE
 */
function snd_core__inscenation_and_performance_add_taxonomy_terms($items) {

	global $inscenationAndPerformanceTaxonomies;

	if(!$inscenationAndPerformanceTaxonomies) {

		// nacitam vsetky taxonomy
		$artisticBodyArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_ARTISTIC_BODY_VID));
		foreach($taxonomy as $term) {
			$artisticBodyArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$placeArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, 0, 1));
		foreach($taxonomy as $term) {
			$placeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$hallArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, TERM_NOVA_BUDOVA_TID, 1));
		foreach($taxonomy as $term) {
			$hallArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$performanceTypeArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PERFORMANCE_TYPE_FULL_VID));
		foreach($taxonomy as $term) {
			$performanceTypeArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$targetGroupArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_TARGET_GROUP_VID));
		foreach($taxonomy as $term) {
			$targetGroupArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$stateArr = array();
		$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_STATE_VID));
		foreach($taxonomy as $term) {
			$stateArr[$term->tid] = array('tid' => $term->tid, 'name' => $term->name, 'class-name' => 'tid-'.$term->tid);
		}

		$inscenationAndPerformanceTaxonomies = array(
			'artistic_body' => $artisticBodyArr,
			'place' => $placeArr,
			'hall' => $hallArr,
			'type' => $performanceTypeArr,
			'target_group' => $targetGroupArr,
			'state' => $stateArr,
		);
	}

	// ak prislo na vstupe pole
	if(isset($items[0])) {

		foreach($items as &$item) {

			$item = snd_core__inscenation_and_performance_add_taxonomy_terms_execute($item, $inscenationAndPerformanceTaxonomies);
		}

	} else {

		$items = snd_core__inscenation_and_performance_add_taxonomy_terms_execute($items, $inscenationAndPerformanceTaxonomies);
	}

	return $items;
}

function snd_core__inscenation_and_performance_add_taxonomy_terms_execute($item, $taxonomies) {

	$item['artistic_body'] = snd_core_tid_to_taxonomy_term($item['artistic_body'], $taxonomies['artistic_body']);
	$item['place'] = snd_core_tid_to_taxonomy_term($item['place'], $taxonomies['place']);
	$item['hall'] = snd_core_tid_to_taxonomy_term($item['hall'], $taxonomies['hall']);
	$item['#type'] = snd_core_tid_to_taxonomy_term($item['type'], $taxonomies['type']);
	$item['state'] = snd_core_tid_to_taxonomy_term($item['state'], $taxonomies['state']);

	return $item;
}

function snd_core_tid_to_taxonomy_term($arr, $taxonomy) {

	if(is_array($arr)) {
		$tid = $arr['tid'];
	} else {
		$tid = $arr;
	}

	if($tid && $taxonomy[$tid]) {
		$arr = $taxonomy[$tid];
	}

	return $arr;
}

// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------


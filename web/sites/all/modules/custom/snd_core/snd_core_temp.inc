<?php


// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------


function snd_core_page_inscenacia( $inscenaciaId ) {

	$html = 'Detail inscenacie';

	$inscenation = snd_core_get_inscenacie_by_id($inscenaciaId);

	// TODO 404
	if (!$inscenation) {
		drupal_not_found();
		exit;
	}

	$inscenation = snd_core_inscenacie_add_taxonomy_terms($inscenation);

	list($dateFrom, $timeFrom) = explode(' ', $performance['date_from']);
	list($dateTo, $timeTo) = explode(' ', $performance['date_to']);
	list($fromHour, $fromMinutes, $fromSeconds) = explode(':', $timeFrom);
	list($toHour, $toMinutes, $toSeconds) = explode(':', $timeTo);

	$place = $inscenation['place']['name'] . (isset($inscenation['hall']['name'])? ', '.$inscenation['hall']['name'] : '');
	$placeClass = 'tid-'.$inscenation['place']['tid'] . (isset($inscenation['hall']['tid'])? ' tid-'.$inscenation['hall']['tid'] : '');

	$imageURL = str_replace('//', '/', base_path().$inscenation['image']);

	$html = '
			<div class="inscenacia">
				<div class="image"><img src="'.$imageURL.'" /></div>
				<div class="info">
					<h3 class="author"><label>Autor:</label><span class="space"> </span><span class="value">'.$inscenation['author'].'</span></h3>
					<h2 class="title" title="'.$inscenation['title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$inscenation['title'].'</span></h2>
					<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span><span class="value">'.$inscenation['artistic_body']['name'].'</span></div>
					<div class="teaser"><span>'.$inscenation['teaser'].'</span></div>
					<div class="clearfix info-row">
						<div class="place '.$placeClass.'" title="'.$place.'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$place.'</span></div>
						<div class="premiere"><label>Premiéra:</label><span class="space"> </span><span class="value">31. marca a 1. apríla</span></div>
					</div>
				</div>
				<div class="description"><span>'.$inscenation['description'].'</span></div>
				<div class="performances">
					<h3>Plánované predstavenia</h3>
					<div class="list">';

	$performances = array(
					array('weekday'=>'Pondelok', 'date'=>'15.4.', 'from'=>'19:00', 'to'=>'22:50', 'state'=>'odohrane'),
					array('weekday'=>'Streda',   'date'=>'22.4.', 'from'=>'20:00', 'to'=>'23:50', 'state'=>'odohrane'),
					array('weekday'=>'Nedeľa',   'date'=>'26.4.', 'from'=>'19:00', 'to'=>'22:50', 'state'=>'zrusene'),
					array('weekday'=>'Štvrtok',  'date'=>'5.5.',  'from'=>'20:00', 'to'=>'23:50', 'state'=>'vypredane'),
					array('weekday'=>'Pondelok', 'date'=>'15.5.', 'from'=>'20:00', 'to'=>'23:50', 'state'=>'v-predaji'),
					array('weekday'=>'Piatok',   'date'=>'19.5.', 'from'=>'19:00', 'to'=>'22:50', 'state'=>'avizovane'),
	);

	$states = array(
		'odohrane' => 'Odohrané',
		'zrusene' => 'Zrušené',
		'v-predaji' => 'V predaji',
		'vypredane' => 'Vypredané',
		'avizovane' => 'Avízované',
		'avizovane' => 'Avízované',
	);

	foreach($performances as $performance) {
		$html .= '
						<div class="performance">
							<div class="date"><label>Dátum konania:</label><span class="space"> </span><span class="value"><span class="weekday">'.$performance['weekday'].'</span> <span class="on-date">'.$performance['date'].'<span class="year">2017</span></span></span></div>
							<div class="time"><span class="time-from">'.$performance['from'].'</span> <span class="time-to">'.$performance['to'].'</span></div>
							<div class="state state-'.$performance['state'].'"><span>'.$states[$performance['state']].'</span></div>
							<div class="action"><a href="/">Zobraziť detail</a></div>
						</div>';
	}

	$html .= '
					</div>
				</div>

				<div class="gallery">
					<h3>Fotografie a videá</h3>
					<div class="list">
						<a href="/sites/default/files/angelika.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/angelika.jpg" /></a>
						<a href="/sites/default/files/bacova-zena.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/bacova-zena.jpg" /></a>
						<a href="/sites/default/files/bloodlines.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/bloodlines.jpg" /></a>
						<a href="/sites/default/files/carmen.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/carmen.jpg" /></a>
						<a href="/sites/default/files/don-giovanni.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/don-giovanni.jpg" /></a>
						<a href="/sites/default/files/desatoro.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/desatoro.jpg" /></a>
						<a href="/sites/default/files/figaro.jpg" data-rel="lightcase:inscenacia-gallery"><img src="/sites/default/files/figaro.jpg" /></a>
					</div>
				</div>

				<div class="dramaturgia">
					<h3>Dramaturgia, scéna a hudba</h3>
					<div class="list">

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Miriam Kičiňová</span><span class="rola">Dramaturgia</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Daniel Majling</span><span class="rola">Dramaturgia</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tom Ciller</span><span class="rola">Scéna a kostýmy</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Vladislav Šarišský</span><span class="rola">Hudba</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Annamária Juhásová</span><span class="rola">Hudba</span></a></div>

					</div>
				</div>

				<div class="obsadenie">
					<h3>Obsadenie</h3>
					<div class="list">

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Ľuboš Kostelný</span><span class="rola">Maximilián Aue</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Jozef Vajda</span><span class="rola">Maximilián Aue</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa</span><span class="rola">Maximilián Aue</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Alexander Bárta</span><span class="rola">Thomas Hauser</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Milan Ondrík</span><span class="rola">Thomas Hauser</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Marián Geišberg</span><span class="rola">Doktor Hohenegg / Üxküll</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Milan Ondrík</span><span class="rola">Vyšetrovateľ / Turek</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Richard Stanke</span><span class="rola">Úradník Korherr</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa</span><span class="rola">Úradník Korherr</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Jozef Vajda</span><span class="rola">Veliteľ /Moreau</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Martin Nahálka <em>(ako hosť)</em></span><span class="rola">Partenau</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Táňa Pauhofová</span><span class="rola">Una / Helena</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Emília Vášáryová</span><span class="rola">Weselohová / Matka</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Monika Horváthová <em>(poslucháčka VŠMU)</em></span><span class="rola">Čašníčka</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Monika Potokárov</span><span class="rola">Čašníčka</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa <em>(poslucháč VŠMU)</em></span><span class="rola">Čašník</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Barbora Palčíková <em>(poslucháčka VŠMU)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Martin Varínsky <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Aleš Junek <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tobias Lacho <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Ryan Bradshaw <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>

					</div>
				</div>

				<div class="vyroba">
					<h3>Výroba</h3>
					<div class="list">
						<div class="member"><span class="rola">Preklad</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Michala Marková</span></a>
						</span></div>
						<div class="member"><span class="rola">Dramatizácia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Daniel Majling</span></a>
						</span></div>
						<div class="member"><span class="rola">Réžia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Michal Vajdička</span></a>
						</span></div>
						<div class="member"><span class="rola">Dramaturgia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Daniel Majling</span></a>
						</span></div>
						<div class="member"><span class="rola">Scéna</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Pavol Andraško</span></a>
						</span></div>
						<div class="member"><span class="rola">Kostýmy</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Pavol Andraško</span></a>
						</span></div>
						<div class="member"><span class="rola">Hudba</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Marián Čekovský</span></a>
						</span></div>
					</div>
				</div>

			</div>';

	return $html;
}


// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------


function snd_core_page_predstavenie( $predstavenieId ) {

	$html = 'Detail predstavenia';

	$performance = snd_core_get_predstavenia_by_id($predstavenieId);

	// TODO 404
	if (!$performance) {
		drupal_not_found();
		exit;
	}

	$performance = snd_core_predstavenia_add_taxonomy_terms($performance);

	$inscenation = $performance['inscenation'];
	$date = new DateTime( $performance['date_from'] );
	$from = $date;
	$date = $from->format('j');
	$month = $from->format('n');
	$year = $from->format('Y');

	list($dateFrom, $timeFrom) = explode(' ', $performance['date_from']);
	list($dateTo, $timeTo) = explode(' ', $performance['date_to']);
	list($fromHour, $fromMinutes, $fromSeconds) = explode(':', $timeFrom);
	list($toHour, $toMinutes, $toSeconds) = explode(':', $timeTo);

	$place = $inscenation['place']['name'] . (isset($inscenation['hall']['name'])? ', '.$inscenation['hall']['name'] : '');
	$placeClass = 'tid-'.$inscenation['place']['tid'] . (isset($inscenation['hall']['tid'])? ' tid-'.$inscenation['hall']['tid'] : '');

	$imageURL = str_replace('//', '/', base_path().$inscenation['image']);

	$html = '
			<div class="predstavenie">
				<div class="image"><img src="'.$imageURL.'" /></div>
				<div class="info">
					<h3 class="author"><label>Autor:</label><span class="space"> </span><span class="value">'.$inscenation['author'].'</span></h3>
					<h2 class="title" title="'.$inscenation['title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$inscenation['title'].'</span></h2>
					<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span><span class="value">'.$inscenation['artistic_body']['name'].'</span></div>
					<div class="teaser"><span>'.$inscenation['teaser'].'</span></div>
					<div class="date"><label>Dátum konania:</label><span class="space"> </span><span class="value"><span class="weekday">'.$weekdayName.'</span> <span class="on-date">'.$date.'.'.$month.'.'.$year.'</span> <span class="time-from">'.$fromHour.':'.$fromMinutes.'</span> <span class="time-to">'.$toHour.':'.$toMinutes.'</span></span></div>
					<div class="clearfix info-row">
						<div class="place '.$placeClass.'" title="'.$place.'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$place.'</span></div>
						<div class="premiere"><label>Premiéra:</label><span class="space"> </span><span class="value">31. marca a 1. apríla</span></div>
					</div>
				</div>
				<div class="description"><span>'.$inscenation['description'].'</span></div>

				<div class="gallery">
					<h3>Fotografie a videá</h3>
					<div class="list">
						<a href="/sites/default/files/angelika.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/angelika.jpg" /></a>
						<a href="/sites/default/files/bacova-zena.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bacova-zena.jpg" /></a>
						<a href="/sites/default/files/bloodlines.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bloodlines.jpg" /></a>
						<a href="/sites/default/files/carmen.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/carmen.jpg" /></a>
						<a href="/sites/default/files/don-giovanni.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/don-giovanni.jpg" /></a>
						<a href="/sites/default/files/desatoro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/desatoro.jpg" /></a>
						<a href="/sites/default/files/figaro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/figaro.jpg" /></a>
					</div>
				</div>

				<div class="dramaturgia">
					<h3>Dramaturgia, scéna a hudba</h3>
					<div class="list">

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Miriam Kičiňová</span><span class="rola">Dramaturgia</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Daniel Majling</span><span class="rola">Dramaturgia</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tom Ciller</span><span class="rola">Scéna a kostýmy</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Vladislav Šarišský</span><span class="rola">Hudba</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Annamária Juhásová</span><span class="rola">Hudba</span></a></div>

					</div>
				</div>

				<div class="obsadenie">
					<h3>Obsadenie</h3>
					<div class="list">

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Ľuboš Kostelný</span><span class="rola">Maximilián Aue</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Jozef Vajda</span><span class="rola">Maximilián Aue</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa</span><span class="rola">Maximilián Aue</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Alexander Bárta</span><span class="rola">Thomas Hauser</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Milan Ondrík</span><span class="rola">Thomas Hauser</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Marián Geišberg</span><span class="rola">Doktor Hohenegg / Üxküll</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Milan Ondrík</span><span class="rola">Vyšetrovateľ / Turek</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Richard Stanke</span><span class="rola">Úradník Korherr</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa</span><span class="rola">Úradník Korherr</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Jozef Vajda</span><span class="rola">Veliteľ /Moreau</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Martin Nahálka <em>(ako hosť)</em></span><span class="rola">Partenau</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Táňa Pauhofová</span><span class="rola">Una / Helena</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Emília Vášáryová</span><span class="rola">Weselohová / Matka</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Monika Horváthová <em>(poslucháčka VŠMU)</em></span><span class="rola">Čašníčka</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Monika Potokárov</span><span class="rola">Čašníčka</span></a></div>

						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tomáš Stopa <em>(poslucháč VŠMU)</em></span><span class="rola">Čašník</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Barbora Palčíková <em>(poslucháčka VŠMU)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Martin Varínsky <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Aleš Junek <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Tobias Lacho <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>
						<div class="member"><a href="#"><img src="/sites/default/files/avatar.svg" /><span class="name">Ryan Bradshaw <em>(ako hosť)</em></span><span class="rola">Zbor</span></a></div>

					</div>
				</div>

				<div class="vyroba">
					<h3>Výroba</h3>
					<div class="list">
						<div class="member"><span class="rola">Preklad</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Michala Marková</span></a>
						</span></div>
						<div class="member"><span class="rola">Dramatizácia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Daniel Majling</span></a>
						</span></div>
						<div class="member"><span class="rola">Réžia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Michal Vajdička</span></a>
						</span></div>
						<div class="member"><span class="rola">Dramaturgia</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Daniel Majling</span></a>
						</span></div>
						<div class="member"><span class="rola">Scéna</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Pavol Andraško</span></a>
						</span></div>
						<div class="member"><span class="rola">Kostýmy</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Pavol Andraško</span></a>
						</span></div>
						<div class="member"><span class="rola">Hudba</span><span class="space"> </span><span class="value">
							<span class="person"><a href="#"><span class="name">Marián Čekovský</span></a>
						</span></div>
					</div>
				</div>

			</div>';

	return $html;
}


function snd_core_page_profil( $profilId ) {

	$info = snd_core_get_profil_by_id($profilId);

	// TODO 404
	if (!$info) {
		drupal_not_found();
		exit;
	}

	drupal_set_title($info['name']);

	$imageURL = '/sites/default/files/pan-mimo.jpg';
	$name = $info['name'];
	$positions = array(
		array('Činohra', 'Umelecký súbor', 'Herecký súbor', 'Herec'),
		array('Opera', 'Prvý hlas', 'Baritón'),
	);

	$html = '
			<div class="person">
				<div class="profile">
					<div class="info">
						<div class="image"><img src="'.$imageURL.'" /></div>
						<h2 class="title" title="'.$name.'">'.$name.'</h2>
						<div class="positions">';

	$wrapper = function($part){ return '<span class="part">' . $part . '</span>'; };
	foreach($positions as $position) {
		$position = array_map($wrapper, $position);
		$html .= '<div class="position">'.implode('<span class="space"> </span>', $position).'</div>';
	}

	$html .= '
						</div>
						<div class="birth">
							<span class="label">Narodený</span><span class="space"> </span><span class="date">11.09.1985</span><span class="space"> </span><span class="palce">Banská Bystrica</span>
						</div>
						<div class="angazman">
							<span class="label">Angažmán</span><span class="space"> </span><span class="place">Činohra SND</span><span class="space"> </span><span class="delimiter">od</span><span class="space"> </span><span class="date">01.09.2014</span>
						</div>
					</div>
					<div class="body">
						<h3>Súpis postáv v činohre SND</h3>
						<p>Sezóna 2002/2003<br>B.S.Timrava/P.Pavlac: Veľké šťastie (Levita)</p>

						<p>Sezóna 2003/2004<br>J.B.P.Moliére: Mizantrop (Oront)<br>L.Lahola/P.Pavlac: Rozhovor s nepriateľom (Kampen)</p>

						<p>Sezóna 2004/2005<br>M.McDonagh: Ujo Vankúšik /The Pillowman/ (Katurian)<br>G.Feydeau: Tak sa na mňa prilepila (Fontanet)</p>

						<p>Sezóna 2005/2006<br>Marivaux: Stratégie a rozmary (Dorant)</p>

						<p>Sezóna 2006/2007<br>W. Shakespeare: Hamlet (Laertes)</p>

						<p>Sezóna 2007/2008<br>V. Hugo: Kráľ sa zabáva (Kráľ František I.)<br>W. Shakespeare: Skrotenie zlej ženy (Lucentio)<br>M. Kukučín, P. Pavlac: Dom v stráni (Niko Dubčič)</p>

						<h3>Súpis postáv v iných divadlách</h3>
						<p>VŠMU a SKD MARTIN<br>M.McDonagh: Opustený západ (Valene Connor) 2003</p>

						<p>VŠMU<br>W.Shakespeare: Othello (Jago) 2004</p>

						<p>NÁRODNÍ DIVADLO BRNO<br>G.Feydeau: Taková ženská na krku (Fontanet) 2005</p>

						<p>LETNÉ SHAKESPEAROVSKÉ SLÁVNOSTI<br>W.Shakespeare: Kupec benátsky (Bassanio) 2005</p>

						<p>DIVADLO ARÉNA<br>R. O’ Brien: Rocky Horror Show (Frank) 2005</p>

						<p>DIVADLO NOVÁ SCÉNA<br>A.Vášová/D.Ursíny/J.Štrasser: Neberte nám princeznú (Miro) 2006</p>

						<p>Súpis ocenení<br>Nominácia v kategórii OBJAV ROKA za postavu Levitu v inscenácii Veľké šťastie v rámci Divadelných ocenení sezóny DOSKY 2003</p>

						<p>Cena za NAJLEPŠÍ HERECKÝ VÝKON za postavu Valena Connora v hre Opustený západ na festivale Isropolitana projekt 2004</p>

						<p>V roku 2008 mu bola udelená Výročná cena literárneho fondu za postavu Kráľa Františka v hre Kráľ sa zabáva, s prihliadnutím k postave Nika Dubčiča v hre Dom v stráni</p>
					</div>
				</div>
				<div class="gallery">
					<h3>Fotografie a videá</h3>
					<div class="list">
						<a href="/sites/default/files/angelika.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/angelika.jpg" /></a>
						<a href="/sites/default/files/bacova-zena.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bacova-zena.jpg" /></a>
						<a href="/sites/default/files/bloodlines.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bloodlines.jpg" /></a>
						<a href="/sites/default/files/carmen.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/carmen.jpg" /></a>
						<a href="/sites/default/files/don-giovanni.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/don-giovanni.jpg" /></a>
						<a href="/sites/default/files/desatoro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/desatoro.jpg" /></a>
						<a href="/sites/default/files/figaro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/figaro.jpg" /></a>
					</div>
				</div>
				<div class="predstavenia">
					<h3>Predstavenia</h3>
					'.snd_core_fragment_program_season().'
					<div class="predstavenia-rows">
						<div class="row"><div class="postava">Vallentin Coverly</div><div class="hra"><a href="#"><span class="author">Tom Stoppard</span><span class="space"> </span><span class="name">Arkádia</span></a></div></div>
						<div class="row"><div class="postava">Bača Ondrej</div><div class="hra"><a href="#"><span class="author">Ivan Stodola</span><span class="space"> </span><span class="name">Bačova žena</span></a></div></div>
						<div class="row"><div class="postava">Samo</div><div class="hra"><a href="#"><span class="author">Božena Slančíková-Timrava, Daniel Majling</span><span class="space"> </span><span class="name">Bál</span></a></div></div>
						<div class="row"><div class="postava">Lajos Kossuth</div><div class="hra"><a href="#"><span class="author">Karol Horák</span><span class="space"> </span><span class="name">Prorok Štúr a jeho tiene alebo Zjavenie, obetovanie a nanebovstúpenie proroka Ľudovíta a jeho učeníkov</span></a></div></div>
						<div class="row"><div class="postava">Lucentio, jeho syn</div><div class="hra"><a href="#"><span class="author">William Shakespeare</span><span class="space"> </span><span class="name">Skrotenie zlej ženy</span></a></div></div>
						<div class="row"><div class="postava">Fontanet</div><div class="hra"><a href="#"><span class="author">Georges Fevdeau</span><span class="space"> </span><span class="name">Tak sa na mňa prilepila</span></a></div></div>
					</div>
				</div>
			</div>';

	return $html;
}


function snd_core_page_kontakt() {

	$buildings = array(
		'new' => array(
			'name' => 'Nová budova SND',
			'address' => 'Pribinová 17, 819 01 Bratislava',
			'map' => 'http://www.maps.google.com',
		),
		'old' => array(
			'name' => 'Historická budova SND',
			'address' => 'Hviezdoslavovo nám. 1, 811 02 Bratislava',
			'map' => 'http://www.maps.google.com',
		),
	);

	$data = array(
		array(
			'id' => rand(0, 100),
			'name' => 'Slovenské národné divadlo',
			'contacts' => array(
				array(
					'id' => rand(0, 100),
					'title' => 'Všeobecné informácie',
					'name' => 'Ústredňa SND',
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'Asistentka gen. riaditeľa',
					'name' => 'Ústredňa SND',
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'Generálny riaditeľ',
					'name' => 'Marián chudovský',
					'profileId' => 55,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
			),
		),
		array(
			'id' => rand(0, 100),
			'name' => 'Vstupenky a abonentky',
			'contacts' => array(
				array(
					'id' => rand(0, 100),
					'title' => 'Pokladnice',
					'name' => 'Rezervácia a kúpa vstupeniek',
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'Hromadné objednávanie činohra',
					'name' => 'Mgr. Martina Majerová',
					'profileId' => 37,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'Abonentky',
					'name' => 'Anna Mináriková',
					'profileId' => 1234,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'Hromad. objed. opera a balet',
					'name' => 'Anna Mináriková',
					'profileId' => 3,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
					),
				array(
					'id' => rand(0, 100),
					'title' => 'Manažér predaja zahraničie',
					'name' => 'Mgr. Mária Hrdličkováá',
					'profileId' => 367,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
			),
		),
		array(
			'id' => rand(0, 100),
			'name' => 'Mediálne oddelenie',
			'contacts' => array(
				array(
					'id' => rand(0, 100),
					'title' => 'Tlačová tajomníčka',
					'name' => 'PhDr. Izabela Pažítková',
					'profileId' => 258,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
				array(
					'id' => rand(0, 100),
					'title' => 'PR manažér',
					'name' => 'Mgr. Art Natalie Dongová',
					'profileId' => 8,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
			),
		),
		array(
			'id' => rand(0, 100),
			'name' => 'Činohra',
			'contacts' => array(
				array(
					'id' => rand(0, 100),
					'title' => 'Riaditeľ čińohry',
					'name' => 'Roman Polák',
					'profileId' => 15,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
			),
		),
		array(
			'id' => rand(0, 100),
			'name' => 'Opera',
			'contacts' => array(
				array(
					'id' => rand(0, 100),
					'title' => 'Riaditeľ opery',
					'name' => 'Slavomír Jakubek',
					'profileId' => 17,
					'address' => 'kancelária č. 158',
					'telephone' => '+421 2 1234 5678',
					'cellphone' => '+421 901 123 456',
					'email' => 'smtg@snd.sk',
				),
			),
		),
	);

	$html = '<div class="contacts">';

	$html .= '<div class="category category-head"><div class="category-name-wrapper"><h3 class="category-name">Budovy</h3></div>';
	foreach($buildings as $key => $building) {

		$html .= '<div class="category-value category-value-'.$key.'">
			<h2 class="name"><span class="value">'.$building['name'].'</span><h2>
			<div class="address"><span class="value">'.$building['address'].'</span></div>
			<div class="map"><span class="value"><a href="'.$building['map'].'">ako sa k nám dostanete</a></span></div>
		</div>';
	}
	$html .= '</div>';

	foreach($data as $list) {

		$html .= '<div class="category category-'.$list['id'].'"><div class="category-name-wrapper"><h3 class="category-name">'.$list['name'].'</h3></div>';
		$html .= '<div class="list">';

		$count = count($list['contacts']);
		$halfElem = ceil($count/2);

		$i = 1;
		foreach($list['contacts'] as $item) {

			if($i == 1) {
				$html .= '<div class="contacts">';
			}

			$html .= '<div id="contact-'.$item['id'].'" class="item">
				<h4 class="title"><span class="value">'.$item['title'].'</span></h4>
				<div class="name"><span class="value">' . (isset($item['profileId'])?'<a href="/profil/'.$item['profileId'].'">'.$item['name'].'</a>' : $item['name']) . '</span></div>
				<div class="info">
					<div class="address"><span class="value">'.$item['address'].'</span></div>
					<div class="telephone"><label>telefón:</label><span class="space"> </span><span class="value"><a href="tel:'.$item['telephone'].'">'.$item['telephone'].'</a></span></div>
					<div class="cellphone"><label>mobil:</label><span class="space"> </span><span class="value"><a href="tel:'.$item['cellphone'].'">'.$item['cellphone'].'</a></span></div>
					<div class="email"><label>e-mail:</label><span class="space"> </span><span class="value"><a href="mailto:'.$item['email'].'">'.$item['email'].'</a></span></div>
				</div>
			</div>';

			if($count > 1 && $i == $halfElem) {
				$html .= '</div><div class="contacts">';
			}

			if($i == $count) {
				$html .= '</div>';
			}

			$i++;
		}

		$html .= '</div>';
		$html .= '</div>';
	}

	$html .= '</div>';

	return $html;
}

function snd_core_page_archiv_predstaveni() {

	global $language;

	$season = snd_core_parse_season_year( $season );

	$html = '
		<div class="archiv-predstaveni-block">

			<div class="search-name">
				<label for="search-keyword">'.t("Searching").'</label>
				<input type="text" placeholder="'.t("Name, surname or position...").'" name="search-keyword" id="search-keyword">
			</div>
			<form class="archiv-search" onsubmit="return false">

				<div class="first-letter">
					<label for="archiv-first-letter">'.t("By title").'</label>
					<select name="archiv-first-letter" id="archiv-first-letter"><option value="0">'.t("Choose letter").'</option>';

	$numbers = range(1, 9);
	foreach($numbers as $number) {

		$html .= '<option value="'.$number.'">'.$number.'</option>';
	}

	$letters = range('A', 'Z');
	foreach($letters as $letter) {

		$html .= '<option value="'.$letter.'">'.$letter.'</option>';
	}

	$html .= '		</select>
				</div>


				<div class="artistic-body">
					<label for="archiv-artistic-body">'.t("By artistic body").'</label>
					<select name="archiv-artistic-body" id="archiv-artistic-body"><option value="0">'.t("Choose artistic body").'</option>';

	$artisticBodyArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_ARTISTIC_BODY_VID));
	foreach($taxonomy as $term) {

		$html .= '<option value="'.$term->tid.'">'.$term->name.'</option>';
	}

	$html .= '		</select>
				</div>



				<div class="author">
					<label for="archiv-author">'.t("By author").'</label>
					<select name="archiv-author" id="archiv-author">
						<option value="0">'.t("Choose author").'</option>
						<option value="1">Autor 1</option>
						<option value="2">Autor 2</option>
						<option value="2">Autor 3</option>
					</select>
				</div>


				'.snd_core_fragment_program_season($season).'


				<div class="place">
					<label for="archiv-place">'.t("By buildings").'</label>
					<select name="archiv-place" id="archiv-place"><option value="0">'.t("Choose scene").'</option>';

	$placeArr = array();
	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_PLACE_VID, 0, 1));
	foreach($taxonomy as $term) {

		$html .= '<option value="'.$term->tid.'">'.$term->name.'</option>';
	}

	$html .= '		</select>
				</div>


				<input type="submit" value="'.t("Search").'">
			</form>

			<div class="archiv-predstaveni">';

	// TODO: zmenit source na stare predstavenia
	$inscenacie = snd_core_get_inscenacie();
	$inscenacie = snd_core_inscenacie_add_taxonomy_terms($inscenacie);

	$detailLinkPrefix = _inscenacia_multilang()[$language->language]['url'];

	foreach ($inscenacie as $inscenation) {

		if (!$artisticBody || $artisticBody==$inscenation['artistic_body']['tid']) {

			$place = $inscenation['place']['name'] . (isset($inscenation['hall']['name'])? ', '.$inscenation['hall']['name'] : '');
			$placeClass = 'tid-'.$inscenation['place']['tid'] . (isset($inscenation['hall']['tid'])? ' tid-'.$inscenation['hall']['tid'] : '');

			$imageURL = str_replace('//', '/', base_path().$inscenation['image']);

			$detailLink = url($detailLinkPrefix.'/'.$inscenation['id']);

			$html .= '
					<div class="predstavenie tid-'.$inscenation['artistic_body']['tid'].'">
						<div class="image"><img src="'.$imageURL.'" /></div>
						<div class="info">
							<div class="author"><label>Autor:</label><span class="space"> </span><span class="value">'.$inscenation['author'].'</span></div>
							<div class="title" title="'.$inscenation['title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$inscenation['title'].'</span></div>
							<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span><span class="value">'.$inscenation['artistic_body']['name'].'</span></div>
							<div class="place '.$placeClass.'" title="'.$place.'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$place.'</span></div>
						</div>
						<div class="description"><span>'.$inscenation['description'].'</span></div>
						<div class="detail-link"><a href="'.$detailLink.'">&nbsp;</a></div>
					</div>';
		}
	}

	$html .= '
			</div>
		</div>';
	return $html;
}

function snd_core_page_vedenie() {

	$taxonomy = i18n_taxonomy_localize_terms(taxonomy_get_tree(TAXONOMY_MANAGEMENT_VID));

	$html = '<div class="vedenie">';
	$html .= '<div class="ctgr">
				<span class="ctgrs">';

	foreach($taxonomy as $term) {

		$term = (array) $term;
		$html .= '<span class="vedenie-ctgr"><input type="radio" name="vedenie-ctgr" id="vedenie-ctgr-'.$term['tid'].'" value="'.$term['tid'].'" data-show="tid-'.$term['tid'].'" /><span class="space"> </span><label for="vedenie-ctgr-'.$term['tid'].'" title="'.$term['name'].'">'.$term['name'].'</label></span>';
	}

	$html .= '</span>
			</div>';

	$html .= '</div>';

	return $html;
}

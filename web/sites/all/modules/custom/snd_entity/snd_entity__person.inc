<?php

function snd_entity__person_menu_enrich( &$items ) {

	// VIEW (public)
	$languages = snd_entity_common_get_languages();
	$urls = snd_entity__person_config_get_public_url_names();
	foreach ($languages as $lang) {
		$items[ $urls[$lang] . '/%'] = array(
			'type' => MENU_CALLBACK,
			'page arguments' => array(0, 1, 2),
			'page callback' => 'snd_entity__person_view',
			'access arguments' => array('access content'),
		);
	}
	//die_r($items);
	// VIEW (admin)
	$items['admin/'.snd_entity__person_config_get_form_url_name().'/%'] = $items[ $urls[snd_entity_common_get_default_language()] . '/%' ];
	$items['admin/'.snd_entity__person_config_get_form_url_name().'/%']['access callback'] = 'user_is_logged_in'; // TODO
	
	// LIST
	$items['admin/'.snd_entity__person_config_get_form_url_name()] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Person list',
		'page callback' => 'snd_entity__person_list',
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// CREATE
	$items['admin/'.snd_entity__person_config_get_form_url_name().'/new'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Add person',
		'page callback' => 'snd_entity__person_new',
		'access callback' => 'user_is_logged_in', // TODO
	);
		
	// EDIT
	$items['admin/'.snd_entity__person_config_get_form_url_name().'/%/edit'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Edit person',
		'page callback' => 'snd_entity__person_edit',
		'page arguments' => array(2),
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// DELETE
	$items['admin/'.snd_entity__person_config_get_form_url_name().'/%/delete'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Delete person',
		'page callback' => 'snd_entity__person_delete',
		'page arguments' => array(2),
		'access callback' => 'user_is_logged_in', // TODO
	);

	// AJAX service - zoznam autorov 
	$items['api/person/getAuthors'] = array(
		'type' => MENU_CALLBACK,
		'page callback' => 'snd_entity__api_person_getAuthors',
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// AJAX service - zoznam vsetkych aktivnych osob
	$items['api/person/getPersons'] = array(
		'type' => MENU_CALLBACK,
		'page callback' => 'snd_entity__api_person_getPersons',
		'access callback' => 'user_is_logged_in', // TODO
	);
	
}


function snd_entity__person_theme_enrich( &$array ) {
	$array['snd_entity__person'] = array(
		'template' => 'person-profile',
		'variables' => array(
			'person' => NULL,
		),
	);
}



// --------------------------------------------------------------------------------------------------------------------------------------------
// - SETUP ------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

function snd_entity__person_get_entity_name() {
	return 'person';
}

function snd_entity__person_config_get_form_id() {
	return 'snd_entity__person_form';
}

function snd_entity__person_config_get_public_url_names() {
	return array('sk' => 'profil', 'en' => 'profile', 'de' => 'profil');
}

function snd_entity__person_config_get_form_url_name() {
	return 'person';
}

function snd_entity__person_config_get_table_name() {
	return 'custom_snd_person';
}

function snd_entity__person_config_get_table_alias() {
	return 'p';
}

function snd_entity__person_config_get_public_url_name( $lang='en' ) {
	$url_names = snd_entity__person_config_get_public_url_names();
	return $url_names[$lang];
}

// identifikator je Meno a Priezvisko - pouziva sa do hlasok
function snd_entity__person_get_text_identifier($data) {
	$lang = snd_entity_common_get_default_language();
	
	$first_name = snd_entity_common_get_multilang_string($data, 'first_name_', $lang);
	$last_name = snd_entity_common_get_multilang_string($data, 'last_name_', $lang); 

	return '#'.$data['id'].' '.$first_name.' '.$last_name;
}

function snd_entity__person_add_body_class_on_detail_page( &$variables ) {
	global $language;	
	$lang = $language->language;
	$path = drupal_get_path_alias();
	// person detail
	$urls = snd_entity__person_config_get_public_url_names();
	if (strpos($path, $urls[$lang].'/')===0)
		$variables['classes_array'][] = 'page-person-detail';
}

// --------------------------------------------------------------------------------------------------------------------------------------------
// - MAIN ACTIONS -----------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------


// LIST
function snd_entity__person_list() {
	snd_entity_common_add_assets();
	
	// Adds filter form to the build array.
	$form = drupal_get_form('snd_entity__person_list_filter_form');
  
	// Add person link
	$add_link_text = 'Pridať osobu';
	array_unshift( $form, array('#markup' => '<ul class="action-links"><li>'.l($add_link_text, 'admin/'.snd_entity__person_config_get_form_url_name().'/new').'</li></ul>') );

	$header = array(
		array('data' => 'Id', 'field' => 'id'),
		array('data' => 'Titul pred menom', 'field' => 'pre_nominal_titles_', '#multilang' => true),
		array('data' => 'Krstné meno', 'field' => 'first_name_', '#multilang' => true),
		array('data' => 'Priezvisko', 'field' => 'last_name_', '#multilang' => true),
		array('data' => 'Titul za menom', 'field' => 'post_nominal_titles_', '#multilang' => true),
		//array('data' => 'Má pozíciu', 'field' => 'positions', '#function' => 'snd_entity__person_transform_has_position'),
		array('data' => 'Verejné', 'field' => 'active', '#function' => 'snd_entity__person_transform_active_state'),
		array('data' => 'Operácie'), 
	);
	
	$url = 'admin/'.snd_entity__person_config_get_form_url_name().'/';
	
	global $language;	
	$lang = $language->language;
	$urls = snd_entity__person_config_get_public_url_names();
	$public_url = $urls[$lang];

	$languages = snd_entity_common_get_languages();
	$filter_conditions = db_and();
	
	// filter: contains_string
	if (isset($form['filter']['contains_string']) && !empty($form['filter']['contains_string']['#default_value'])) {
		$or = db_or();
		$str = $form['filter']['contains_string']['#default_value'];
		foreach($languages as $lang) {
			$or->condition('pre_nominal_titles_'.$lang,  '%'.$str.'%',  'LIKE');
			$or->condition('first_name_'.$lang,  '%'.$str.'%',  'LIKE');
			$or->condition('last_name_'.$lang,  '%'.$str.'%',  'LIKE');
			$or->condition('post_nominal_titles_'.$lang,  '%'.$str.'%',  'LIKE');
		}
		$or->condition('id',  '%'.$str.'%',  'LIKE');
		$filter_conditions->condition($or);
	}
	
	// TEMP
	if (isset($form['filter']['position_id']) && !empty($form['filter']['position_id']['#default_value'])) {
		
		$position_id = $form['filter']['position_id']['#default_value'];
		$taxonomy = snd_entity_common_get_taxonomy_localized_terms( TAXONOMY_PERSON_CATEGORIES_VID, snd_entity_common_get_default_language() );	
		$positions = snd_entity_common_get_taxonomy_leaf_termId_by_parent_termId($taxonomy, $position_id);
		$or = db_or();
		foreach($positions as $position) {
			$or->condition('positions',  '(^|;)'.$position.'(;|$)',  'REGEXP');		
		}
		$filter_conditions->condition($or);
	}
	

	// filter: has_position
	if (isset($form['filter']['has_position'])) {
		if ($form['filter']['has_position']['#default_value']=='1')
			$filter_conditions->condition('positions',  '', '<>');
		if ($form['filter']['has_position']['#default_value']=='0')
			$filter_conditions->condition('positions',  '', '=');
	}	
	
	// filter: is_author
	if (isset($form['filter']['is_author']) && $form['filter']['is_author']['#default_value']!='') {		
		$filter_conditions->condition('is_author',  $form['filter']['is_author']['#default_value']);
	}
	
	// filter: active
	if (isset($form['filter']['active']) && $form['filter']['active']['#default_value']!='') {		
		$filter_conditions->condition('active',  $form['filter']['active']['#default_value']);
	}
	
	
	snd_entity_common_get_list( $form, $header, snd_entity__person_config_get_table_name(), snd_entity__person_config_get_table_alias(), $url, $public_url, $filter_conditions );

	return $form;
}

function snd_entity__person_transform_active_state( $value ) {
	return $value ? 'zverejnené' : '---';
}

/*function snd_entity__person_transform_has_position( $value ) {
	return $value!='' ? 'áno' : 'nie';
}*/

function snd_entity__person_list_filter_form($form, &$form_state) {
	
	//print('<pre>'.print_r($form_state,1).'</pre>');
	//$form_state['values'] = $_SESSION['form_values'];
	
	$session_key = snd_entity_common_list_filter_get_session_key($form_state);
	$filter_expanded = isset($_SESSION[$session_key]);
	
	$form['filter'] = array(
		'#type' => 'fieldset',
		'#collapsible' => true,
		'#collapsed' => !$filter_expanded,
		'#attributes' => array('class' => array('inline-form')),
		'#title' => 'Filter'
	);
	
		$form['filter']['contains_string'] = array(
			'#type' => 'textfield',
			'#title' => 'Obsahuje reťazec',
		);
		
		$form['filter']['active'] = array(
			'#type' => 'select',
			'#title' => 'Verejné',
			'#options' => array(''=>'', '1' => 'zverejnené', '0' => 'nezverejnené'),
		);
		
		$taxonomy = snd_entity_common_get_taxonomy_localized_terms( TAXONOMY_PERSON_CATEGORIES_VID, snd_entity_common_get_default_language() );		
		$options = snd_entity_common_get_options_from_taxonomy($taxonomy, true);
		$form['filter']['position_id'] = array(
			'#type' => 'select',
			'#title' => 'Pozicia',
			//'#attributes' => array('size' => '24'),
			'#options' => $options,
		);

		$form['filter']['has_position'] = array(
			'#type' => 'select',
			'#title' => 'Má pozíciu',
			'#options' => array(''=>'', '1' => 'áno', '0' => 'nie'),
		);
		
		$form['filter']['is_author'] = array(
			'#type' => 'select',
			'#title' => 'Je autor',
			'#options' => array(''=>'', '1' => 'áno', '0' => 'nie'),
		);

	$form['filter']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
		'#submit' => array('snd_entity_common_list_filter_form_submit'),
	);

	$form['filter']['reset'] = array(
		'#type' => 'submit',
		'#value' => t('Reset'),
		'#submit' => array('snd_entity_common_list_filter_form_reset'),
	);
	
	snd_entity_common_default_values( $form['filter'], $_SESSION[$session_key] );
	
	return $form;
}

// NEW (CREATE)
function snd_entity__person_new() {
	snd_entity_common_add_assets();
	$form_id = snd_entity__person_config_get_form_id();
	$form_state = array();
	
	// default values
	$form_state['#default_values'] = array();
	
	return drupal_build_form($form_id, $form_state);
}

// VIEW (DETAIL)
function snd_entity__person_view( $path, $id, $name_in_url ) {
	global $language;
	
	$lang = $language->language;
	$languages = snd_entity_common_get_languages();
	$urls = snd_entity__person_config_get_public_url_names();
	
	$person = snd_entity__api_person_get( $id, true ); // true = load all data

	if (!$person) {
		drupal_not_found();
		exit;
	}
	
	// check if path mutation is corresponding to current language
	// and correct name form in url
	if ($path != $urls[$lang] || $name_in_url!=$person['url_alias_'.$lang]) {
		drupal_goto($urls[$lang].'/'.$id.'/'.$person['url_alias_'.$lang]);
		exit;
	}
		
	drupal_set_title($person['full_name_with_titles']);
	
	$person['positions'] = snd_admin_api_person_categories_paths( explode(';', $person['positions']) );
	
	return theme('snd_entity__person', array('person' => $person));
}

// EDIT (UPDATE)
function snd_entity__person_edit( $id ) {
	snd_entity_common_add_assets();
	$form_id = snd_entity__person_config_get_form_id();
	$form_state = array();
	
	// default values
	$form_state['#default_values'] = snd_entity__api_person_get( $id );
	
	$form_state['#id'] = $id;
	
	return drupal_build_form($form_id, $form_state);
}

// DELETE
function snd_entity__person_delete( $id ) {
	$person = snd_entity__api_person_get( $id );
	
	if (!$person) {
		drupal_not_found();
		exit;
	}
	
	$identifier = snd_entity__person_get_text_identifier($person);	
	drupal_set_title('Naozaj chcete odstrániť osobu "'.$identifier.'"?');
	
	$form_id = snd_entity__person_config_get_form_id() . '_delete';
	$form_state = array('#id' => $id);	
	return drupal_build_form($form_id, $form_state);
}


// --------------------------------------------------------------------------------------------------------------------------------------------
// - FORM DEFINITION --------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

// form structrure
function snd_entity__person_form($node, $form_state) {
	$form = array();
	
	$form['#attributes']['enctype'] = "multipart/form-data"; // when file uploads are present
	//$form['#attributes'] = array('class' => 'classname');
	
	/*
	// Form API Reference - https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.x/
	
	// TEMPLATE SIMPLE
	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Enter your name'),
	);
	
	// TEMPLATE FULL
	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Enter your name'),
		'#description' => t('Your first name goes here'),
		'#attributes' => array('id' => 'xxx', 'class' => array('special-classname')),
		'#prefix' => '<div class="custom-wrappper">',
		'#suffix' => '</div>',
		'#required' => true,
		'#default_value'=>'input value',
	);
	*/
	
	//snd_entity_common_add_textfield( $form, 'name', t('Enter your name'), t('Your first name goes here')); 
	
	// NAME PREVIEW
	$form['test'] = array(
		'#markup' => '<div class="widget-multilang-preview"><span class="empty">meno zatiaľ nie je zadané</span></div>',
    );
	
	// FIELDSET
	$form['complete_name'] = array(
        '#type' => 'fieldset',
        '#title' => 'Celé meno osoby',
		'#collapsible' => true, 
		'#collapsed' => false, 
    );
	
		$description = 'Titul alebo viac titulov, ktoré sa zobrazujú v danom jazyku pred menom. Zmazať hodnotu v inom jazyku môžete zadaním medzery.';
		
		snd_entity_common_add_multilang_field( $form['complete_name'], 'pre_nominal_titles', 'textfield', 'Titul pred menom', $description);
		snd_entity_common_add_multilang_field( $form['complete_name'], 'first_name', 'textfield', 'Krstné meno', 'V každom jazyku môže byť zadané inak.');
		snd_entity_common_add_multilang_field( $form['complete_name'], 'last_name', 'textfield', 'Priezvisko', 'V iných jazykoch môže byť zadané napríklad bez koncovky "ová".');
		snd_entity_common_add_multilang_field( $form['complete_name'], 'post_nominal_titles', 'textfield', 'Titul za menom', $description);	
	
	// FIELDSET
	$form['personal_data'] = array(
        '#type' => 'fieldset',
        '#title' => 'Osobné údaje',
		'#collapsible' => true, 
		'#collapsed' => false, 
    );
	
		// RADIOS
		$form['personal_data']['sex'] = array(
			'#type' => 'radios',
			'#title' => 'Pohlavie',
			'#attributes' => array('class' => array('inline-form-items')),
			'#required' => true,
			'#options' => array('male'=>'muž', 'female'=>'žena'),
		);
		
		// DATE
		$form['personal_data']['born'] = array(
			'#type' => 'date_select',
			'#title' => 'Narodený/á',
			'#date_format' => 'd-m-Y',
			//'#required' => true,
			'#date_year_range' => '-250:+0',
			'#prefix' => '<div class="float-left clear-left">',
			'#suffix' => '</div>',
		);
		
		// MULTILANG TEXTFIELD
		snd_entity_common_add_multilang_field( $form['personal_data'], 'birth_place', 'textfield', 'Miesto narodenia');
		$form['personal_data']['birth_place']['#prefix'] = '<div class="after-date-float-left">';
		$form['personal_data']['birth_place']['#suffix'] = '</div>';
		
		// DATE
		$form['personal_data']['died'] = array(
			'#type' => 'date_select',
			'#title' => 'Zosnulý/á',
			'#date_format' => 'd-m-Y',
			'#date_year_range' => '-250:+0',
			'#prefix' => '<div class="float-left clear-left">',
			'#suffix' => '</div>',
		);
		
		// MULTILANG TEXTFIELD
		$item = snd_entity_common_add_multilang_field( $form['personal_data'], 'death_place', 'textfield', 'Miesto úmrtia');
		$form['personal_data']['death_place']['#prefix'] = '<div class="after-date-float-left">';
		$form['personal_data']['death_place']['#suffix'] = '</div>';
		  
		// CHECKBOX		
		$form['personal_data']['show_in_celebrities'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Zobrazovať v osobnostiach SND</strong>',
			'#prefix' => '<div class="clear-left">',
			'#suffix' => '</div>',
		);
	
		// IMAGE
		$form['personal_data']['image'] = array(
			'#type' => 'managed_file',
			'#title' => 'Fotografia',
			'#theme' => 'snd_entity_common_thumb_upload',
			'#size' => SND_ENTITY_COMMON_INPUT_SIZE,
			'#upload_validators' => array('file_validate_extensions' => array('png jpg jpeg gif')),
			'#description' => "Povolené formáty: jpg, gif, png",
			'#upload_location' => 'public://',
		); 		
	
	
	// FIELDSET
	$form['profile'] = array(
        '#type' => 'fieldset',
        '#title' => 'Profil',
		'#collapsible' => true, 
		'#collapsed' => false, 
    );
	
		// CHECKBOXES
		$form['profile']['sections'] = array(
			'#type' => 'checkboxes',
			'#title' => 'Pôsobí v sekciách',
			'#attributes' => array('class' => array('inline-form-items')),
			'#options' => array('snd'=>'SND', 'cinohra'=>'Činohra', 'opera'=>'Opera', 'balet'=>'Balet'),
			//'#default_value' => array('snd', 'opera'),
		);
		
		// CHECKBOX
		$form['profile']['show_in_artistic_body'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Zobrazovať túto osobu na stránkach umeleckého súboru</strong>',
		);
		
		// CHECKBOX
		$form['profile']['is_author'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Zobrazuje sa v zozname autorov</strong> inscenácií (pre admin účely)',
		);
		
		// CHECKBOX
		$form['profile']['like_a_host'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Ako hosť</strong> (zobrazuje sa v obsadení)',
		);
		// CHECKBOX
		$form['profile']['student_of_vsmu'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Poslucháč VŠMU</strong>	(zobrazuje sa v obsadení)',
		);
		// CHECKBOX
		$form['profile']['student_of_conservatory'] = array(
			'#type' => 'checkbox',
			'#title' => '<strong>Poslucháč konzervatória</strong> (zobrazuje sa v obsadení)',
		);
		
		// CUSTOM WIDGET (textfield)
		$form['profile']['positions'] = array(
			'#type' => 'textfield',
			'#title' => 'Pozície',
			'#description' => 'Môžete zadať aj viac pozícií.',
			//'#default_value' => '82;139;57874',
			'#prefix' => '<div class="widget-positions">',
			'#suffix' => '</div>',
		);
		
		//snd_entity_common_add_multilang_field( $form['profile'], 'description', 'textarea', 'Popis');
		snd_entity_common_add_multilang_wysiwig( $form['profile'], 'description', 'Popis');
		snd_entity_common_add_multilang_wysiwig( $form['profile'], 'cv', 'Životopis');
	
	
	// FIELDSET
	$form['gallery'] = array(
        '#type' => 'fieldset',
		// v zatvorke vypise pocet obrazkov
        '#title' => 'Galéria ('.count(array_filter( explode(';', $form_state['#default_values']['gallery']) )).')',
		'#collapsible' => true, 
		'#collapsed' => true, 
    );
	
	// pre NEW (CREATE)
	if (!isset($form_state['#id'])) {
		// nedaju sa pridavat este obrazky do galerie (az pre UDPATE)
		$form['gallery']['no-gallery'] = array(
			'#markup' => '<p>Obrázky sa do galérie dajú pridávať až po vytvorení osoby.</p>',
		);
	}
	// pre UPDATE
	else {
		// definicia pola na nahravanie multi obrazkov (PLUPLOAD)
		$form['gallery']['gallery_plupload'] = array(
			'#type' => 'plupload',
			'#title' => 'Nahrávanie fotografií',
			'#description' => 'Nahrajte jeden, alebo viacero obrázkov. Obrázky možno nahrať aj pomocou Drag & Drop.',
			'#autoupload' => false,
			'#autosubmit' => false,
			'#submit_element' => '#edit-submit',
			'#upload_validators' => array( 'file_validate_extensions' => array('jpg jpeg gif png') ),
			'#plupload_settings' => array(
				'runtimes' => 'html5',
				'chunk_size' => '1mb',
			),
		);
		
		// extra css needed
		$form['#attached']['css'] = array(
		  drupal_get_path('module', 'snd_entity') . '/snd_entity_plupload.css',
		);
	
		// CUSTOM WIDGET (textfield)
		$html = '';
		$fids = array_filter( explode(';', $form_state['#default_values']['gallery']) );
		if (!empty($fids)) {
			$files = file_load_multiple($fids);
			foreach($files as $file) {
				$imageHTML = theme('image_style', array('path' => $file->uri, 'style_name' => 'gallery_edit'));
				$html .= '<div class="image" id="gallery-image-'.$file->fid.'">'.$imageHTML.'<span class="order" title="posunúť vľavo">&leftarrow;</span><span class="remove" title="odstrániť">&times;</span></div>';
			}
		}
		
		// .widget-gallery
		$form['gallery']['gallery-widget'] = array(
			'#markup' => '<div class="widget-gallery">'.$html.'</div>'.
						 '<p>Zmeny v galérii treba uložiť, inak sa neprejavia.</p>',
		);
		
		$form['gallery']['gallery'] = array(
			'#type' => 'hidden', // hidden "textfield"
			'#title' => 'Obrázky galérie',
			'#description' => 'Id obrázkov oddelené bodkočiarkou.',
			'#attributes' => array('class' => array('gallery-images')), // .gallery-images je povinna sucast .widget-gallery
		);
			
		$form['gallery']['gallery_remove_images'] = array(
			'#type' => 'hidden', // hidden "textfield"
			'#title' => 'Obrázky galérie na odstránenie',
			'#description' => 'Id obrázkov oddelené bodkočiarkou.',
			'#attributes' => array('class' => array('gallery-remove-images')), // .gallery-remove-images je povinna sucast .widget-gallery
		);
	}
	
	// FIELDSET
	$form['others'] = array(
        '#type' => 'fieldset',
        '#title' => 'Ostatné',
		'#collapsible' => true, 
		'#collapsed' => true, 
    );
	
		// CHECKBOX
		$form['others']['active'] = array(
			'#type' => 'checkbox',
			'#title' => 'Zverejnené',
			'#default_value' => 1,
		);
		
		// MULTILANG TEXTFIELD
		snd_entity_common_add_multilang_field( $form['others'], 'url_alias', 'textfield', 'URL alias', 'Ako bude vyzerat meno osoby v linke URL. Generuje sa automaticky, ale môžete zmeniť.');
		
		// READONLY TEXTFIELD
		$form['others']['history'] = array(
			'#type' => 'textarea',
			'#title' => 'História zmien',
			'#cols' => SND_ENTITY_COMMON_TEXTAREA_SIZE,
			'#rows' => 3,
			'#readonly' => true,
			'#attributes' => array('size' => SND_ENTITY_COMMON_INPUT_SIZE, 'readonly' => 'readonly', 'disabled' => 'disabled'), 
			'#prefix' => '<div class="standard-width">',
			'#suffix' => '</div>',
		);
			
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);

	snd_entity_common_default_values( $form, $form_state['#default_values'] );

	return $form;
}


// form validation
function snd_entity__person_form_validate($form, &$form_state) {
	$value = $form_state['values'];	

	// krstne meno je nepovinne
	//snd_entity_common_validate_multilang_field($form_state, 'first_name_');
	
	snd_entity_common_validate_multilang_field($form_state, 'last_name_');
	
	/*
	
	if (empty($value['name']))
		form_set_error('name','Name cannot be empty.');
	
	if (preg_match('/[0-9]/', $value['last_name_sk']))
		form_set_error('last_name_sk','Numbers are not allowed in Last name.');
	
	if (empty($form_state['values']['last_name']))
		form_set_error('last_name','Last name cannot be empty');
	
	if (filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL) == false)
		form_set_error('email','Email is not valid');
	
	*/
}


// form save
function snd_entity__person_form_submit($form, $form_state) {

	$subfolderName = snd_entity__person_get_entity_name();
	$entityName = snd_entity__person_get_entity_name();
	
	$isNew = !isset($form_state['#id']);
	$values = &$form_state['values'];

	// save gallery images
	snd_entity_common_save_gallery_from_form( $form, $form_state, $entityName, $subfolderName );
		
	if ($isNew) {
		$id = snd_entity__api_person_create( $values );
	}
	else {
		$id = $form_state['#id'];
		$result = snd_entity__api_person_update( $id, $values );
		
		if ($result===false)
			drupal_set_message('Nebola však vykonaná žiadna zmena.');
	}
	
	// saves basic image (single)
	$image = snd_entity_common_save_image($form, $form_state, 'image', $entityName, $id, $subfolderName);

	// message
	$values['id'] = $id;
	$identifier = snd_entity__person_get_text_identifier($values);
	drupal_set_message('Osoba "'.$identifier.'" bola uložená.');
			
	drupal_goto('admin/'.snd_entity__person_config_get_form_url_name());
}



// form structrure for DELETE form
function snd_entity__person_form_delete($node, $form_state) {
	$form = array();

	// NAME PREVIEW
	$form['test'] = array(
		'#markup' => '<p>Táto akcia sa už nedá vrátiť!</p>',
    );
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Delete')
	);
	
	return $form;
}


// submitting DELETE form
function snd_entity__person_form_delete_submit($node, $form_state) {
	
	$id = $form_state['#id'];
	$person = snd_entity__api_person_get( $id );
	
	$identifier = snd_entity__person_get_text_identifier($person);
	drupal_set_message('Osoba "'.$identifier.'" bola odstránená.');
	
	snd_entity__api_person_delete( $id );
	
	drupal_goto('admin/'.snd_entity__person_config_get_form_url_name());
}

// @$action 'create' | 'modify' | 'delete'
function snd_entity__person_prepare_values_for_save( $values, $action, $history='' ) {
	// general prepare of form values
	$values = snd_entity_common_get_params( $values );
	
	$values['section_snd'] = $values['sections']['snd'];
	$values['section_cinohra'] = $values['sections']['cinohra'];
	$values['section_opera'] = $values['sections']['opera'];
	$values['section_balet'] = $values['sections']['balet'];
	unset($values['sections']);
	
	
	unset($values['gallery_plupload']);
	unset($values['gallery_remove_images']);
	
	// convert selected values to 0 or 1
	$values = snd_entity_common_convert_params_boolean( $values, array('active', 'show_in_celebrities', 'section_snd', 'section_cinohra', 'section_opera', 'section_balet', 'show_in_artistic_body', 'show_in_celebrities', 'is_author', 'like_a_host', 'student_of_vsmu', 'student_of_conservatory') );
	
	// URL aliases
	$pre_nominal_titles = array();
	$first_name = array();
	$last_name = array();
	$post_nominal_titles = array();
	$languages = snd_entity_common_get_languages();
	foreach ($languages as $lang) {
		$pre_nominal_titles[$lang] = snd_entity_common_get_multilang_string($values, 'pre_nominal_titles_', $lang);
		$first_name[$lang] = snd_entity_common_get_multilang_string($values, 'first_name_', $lang);
		$last_name[$lang] = snd_entity_common_get_multilang_string($values, 'last_name_', $lang);
		$post_nominal_titles[$lang] = snd_entity_common_get_multilang_string($values, 'post_nominal_titles_', $lang);
	}
	module_load_include('inc', 'pathauto');
	foreach ($languages as $lang) {
		if (trim($values['url_alias_'.$lang])=='') {
			$values['url_alias_'.$lang] = trim($pre_nominal_titles[$lang].' '.$first_name[$lang].' '.$last_name[$lang].' '.$post_nominal_titles[$lang]);
		}
		$values['url_alias_'.$lang] = pathauto_cleanstring( $values['url_alias_'.$lang] );
	}
	
	if (!$values['image'])
		$values['image'] = 0;
	
	// history
	$values['history'] = snd_entity_common_add_history_record($action, $history);
	
	return $values;
}

function snd_entity__person_postprocess_data_when_get( &$result ) {

	global $language;
	$lang = $language->language;

	snd_entity__person_renderFullName( $result, $lang );

	$result['description'] = snd_entity_common_get_multilang_string($result, 'description_', $lang);
	$result['#cv'] = snd_entity_common_get_multilang_string($result, 'cv_', $lang);
	$result['#birth_place'] = snd_entity_common_get_multilang_string($result, 'birth_place_', $lang);
	$result['#death_place'] = snd_entity_common_get_multilang_string($result, 'death_place_', $lang);
	
	$result['sections'] = array(
		'snd' => $result['section_snd'],
		'cinohra' => $result['section_cinohra'],
		'opera' => $result['section_opera'],
		'balet' => $result['section_balet'],
	);

	return $result;
}



// --------------------------------------------------------------------------------------------------------------------------------------------
// - PERSON MODEL (API) -----------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------



// API GET
function snd_entity__api_person_get( $id, $load_complete_data=false ) {
	
	$query = db_select( snd_entity__person_config_get_table_name(), snd_entity__person_config_get_table_alias() )
				->fields(snd_entity__person_config_get_table_alias())
				->condition('id', $id);
				
    $result = $query->execute();
	
	if ($result->rowCount()==0)
		return false;
	
	$item = $result->fetchAssoc();
	$item = snd_entity__person_postprocess_data_when_get($item);	
	
	// image
	if ($item['image']) {
		$file = file_load($item['image']);
		$item['#image'] = $file;
	}
	else
		$item['#image'] = null;
	
	if ($load_complete_data) {
		// authoring, production, starring
		$nested_with_inscenations = snd_entity__inscenation_getByPersonId( $id );
		// merge it into main object
		$item = array_merge($item, $nested_with_inscenations);
	}
	
	// gallery
	if ($load_complete_data) 
		snd_entity_common_get_gallery_images($item);
	
	return $item;
}


// API CREATE
function snd_entity__api_person_create( $form_values ) {
	
	$values = snd_entity__person_prepare_values_for_save( $form_values, 'create', '' );
		
	$id = db_insert( snd_entity__person_config_get_table_name() )
			->fields($values)
			->execute();
			
	// generate #link for hook
	$values['id'] = $id;
	snd_entity__person_renderViewLink( $values, snd_entity_common_get_default_language() );
	
	// hooks
	module_invoke_all('snd_entity_person_create', $values);
			
	// clears page cache
	cache_clear_all(NULL, 'cache_page');
	
	return $id;
}


// API UPDATE
function snd_entity__api_person_update( $id, $form_values ) {
	
	// read actual data
	$person = snd_entity__api_person_get( $id );
	
	$values = snd_entity__person_prepare_values_for_save( $form_values, 'modify [...]', $person['history'] );

	$is_same = snd_entity_common_is_same($values, $person);
	if ($is_same!==true) {
		// timestamp
		$values['modified'] = date("Y-m-d H:i:s", time());
		
		// writes, wich fields has been modified
		$values['history'] = str_replace('[...]', '['.implode(', ', $is_same).']', $values['history']);
		
		// db UPDATE query
		$num_updated = db_update( snd_entity__person_config_get_table_name() ) 
						->fields($values)
						->condition('id', $id)
						->execute();
		
		// generate #link for hook
		$values['id'] = $id;
		snd_entity__person_renderViewLink( $values, snd_entity_common_get_default_language() );
		
		// hooks
		module_invoke_all('snd_entity_person_update', $values);
		
		// clears page cache
		cache_clear_all(NULL, 'cache_page');
	
		return $num_updated>0;
	}
	else
		return false;
}


// API DELETE
function snd_entity__api_person_delete( $id ) {
	global $user;
	
	// read actual data
	$person = snd_entity__api_person_get( $id );
	$action = 'delete';
	
	// timestamp	
	$values = array(
		'modified' => date("Y-m-d H:i:s", time()),
		'deleted' => 1,
		'history' => trim( date("Y-m-d H:i:s").' - '.$user->name.' - '.$action . "\n" . $person['history'] ),
	);
	
	// db UPDATE query
	$num_updated = db_update( snd_entity__person_config_get_table_name() ) 
					->fields($values)
					->condition('id', $id)
					->execute();

	// hooks 
	module_invoke_all('snd_entity_person_delete', $id);
	
	// clears page cache
	cache_clear_all(NULL, 'cache_page');
	
	return $num_updated>0;
		
	/*$num_deleted = db_delete( snd_entity__person_config_get_table_name() )
					  ->condition('id', $id)
					  ->execute();
					  
	return $num_deleted>0;*/
}



// AJAX service - zoznam autorov 	
function snd_entity__api_person_getAuthors() {
	$query = db_select( snd_entity__person_config_get_table_name(), snd_entity__person_config_get_table_alias() )
				->fields(snd_entity__person_config_get_table_alias())
				->condition('is_author', '1')
				->condition('active', '1')
				->condition('deleted', '0')
				->orderBy('last_name_sk', 'asc')
				->orderBy('last_name_en', 'asc')
				->orderBy('last_name_de', 'asc')
				->orderBy('first_name_sk', 'asc')
				->orderBy('first_name_en', 'asc')
				->orderBy('first_name_de', 'asc');
	
	$lang = snd_entity_common_get_default_language();			
	$result = $query->execute();
	$authors = array();
	foreach ($result as $item) {
		$item =  (array) $item;
		snd_entity__person_renderFullName( $item, $lang );
	
		$authors[] = array('tid' => $item['id'], 'name' => $item['full_name_with_postnominal_titles']. ' (' . $item['id'] .')');
	}
	drupal_json_output($authors);
}


// pouziva sa ako doplnok k inym entitam pre zobrazenie
// AJAX service - zoznam vsetkych aktivnych osob (podla zadanych Id)
function snd_entity__api_person_getPersonsByIds( $ids = array() ) {
	$query = db_select( snd_entity__person_config_get_table_name(), snd_entity__person_config_get_table_alias() )
				->fields(snd_entity__person_config_get_table_alias())
				->condition('active', '1')
				->condition('deleted', '0');
	
	if (count($ids)>0)
		$query->condition('id', $ids, 'IN');
	
	$lang = snd_entity_common_get_default_language();			
	$result = $query->execute();
	$authors = array();
	foreach ($result as $item) {
		$item =  (array) $item;
		snd_entity__person_renderFullName( $item, $lang );
		snd_entity__person_renderViewLink($item, $lang);
		$authors[] = array('id' => $item['id'], 'name' => $item['full_name_with_postnominal_titles'], 'link' => $item['#link']);
	}
	
	return $authors;
}


// AJAX service - zoznam vsetkych aktivnych osob 	
function snd_entity__api_person_getPersons() {
	$query = db_select( snd_entity__person_config_get_table_name(), snd_entity__person_config_get_table_alias() )
				->fields(snd_entity__person_config_get_table_alias())
				->condition('active', '1')
				->condition('deleted', '0')
				->orderBy('last_name_sk', 'asc')
				->orderBy('last_name_en', 'asc')
				->orderBy('last_name_de', 'asc')
				->orderBy('first_name_sk', 'asc')
				->orderBy('first_name_en', 'asc')
				->orderBy('first_name_de', 'asc');
	
	if (count($ids)>0)
		$query->condition('id', $ids, 'IN');
	
	$lang = snd_entity_common_get_default_language();			
	$result = $query->execute();
	$authors = array();
	foreach ($result as $item) {
		$item =  (array) $item;
		snd_entity__person_renderFullName( $item, $lang );
	
		$authors[] = array('id' => $item['id'], 'name' => $item['full_name_with_postnominal_titles']. ' (' . $item['id'] .')');
	}
	drupal_json_output($authors);
}

function snd_entity__person_renderFullName( &$item, $lang ) {

	$item['pre_nominal_titles'] = trim(snd_entity_common_get_multilang_string($item, 'pre_nominal_titles_', $lang));
	$item['first_name'] = trim(snd_entity_common_get_multilang_string($item, 'first_name_', $lang));
	$item['last_name'] = trim(snd_entity_common_get_multilang_string($item, 'last_name_', $lang));
	$item['post_nominal_titles'] = trim(snd_entity_common_get_multilang_string($item, 'post_nominal_titles_', $lang));

	$item['full_name_with_titles'] = trim($item['pre_nominal_titles'].' '.$item['first_name'].' '.$item['last_name'].' '.$item['post_nominal_titles']);
	$item['full_name_with_postnominal_titles'] = trim($item['last_name'].' '.$item['first_name'].' '.$item['post_nominal_titles']);
}

function snd_entity__person_renderViewLink( &$item, $lang=null  ) {
	if ($lang) {
		$urlDetail = snd_entity__person_config_get_public_url_names();
		$urlDetail = $urlDetail[$lang]; 
		$item['#link'] = snd_entity_common_get_language_url_prefix($lang) . '/'.$urlDetail.'/'.$item['id'];
		if ($item['url_alias_'.$lang])
			$item['#link'] .= '/'.$item['url_alias_'.$lang];
	} else {
		$languages = snd_entity_common_get_languages();
		foreach($languages as $lang) {
			snd_entity__person_renderViewLink( $item, $lang );
			$item['#link_'.$lang] = $item['#link'];
		}
		unset($item['#link']);
	}
}


/* --------------------------------------- */
/* ---- CUSTOM PAGES --------------------- */
/* --------------------------------------- */

function snd_entity__person_page_celebrities() {

	global $language;
	$lang = $language->language;

	// person table
	$pTab = snd_entity__person_config_get_table_name();
	// person table alias
	$pTabAlias = snd_entity__person_config_get_table_alias();

	$html = '<div class="osobnosti">';
	$html .= '<div class="search">
				<form class="osobnosti-search-by-name" onsubmit="return false">
				<label for="search-artist">Podľa hladaného mena</label>
				<input type="text" placeholder="" name="search-osobnosti" id="search-osobnosti" />
				<input type="submit" value="'.t('Search').'" />
			</form>
			</div>';

	$query = db_select( $pTab, $pTabAlias )
			->fields($pTabAlias)
			->condition('active', '1')
			->condition('deleted', '0')
			->condition('show_in_celebrities', 1)
			->orderBy('last_name_sk', 'asc')
			->orderBy('last_name_en', 'asc')
			->orderBy('last_name_de', 'asc')
			->orderBy('first_name_sk', 'asc')
			->orderBy('first_name_en', 'asc')
			->orderBy('first_name_de', 'asc');

	$result = $query->execute();

	$persons = array();
	$imageFids = array();

	foreach($result as $item) {

		$item = (array) $item;
		snd_entity__person_renderFullName($item, $lang);
		snd_entity__person_renderViewLink($item, $lang);

		// z obrazkov pozbieram idecka (neskor nacitam naraz)
		if($item['image'] != 0) {
			$imageFids[] = $item['image'];
		}

		$persons[] = $item;
	}

	// nacitam obrazky naraz pre vsetky osoby
	$files = count($imageFids)>0 ? file_load_multiple($imageFids) : array();

	// doplnim obrazky do ludi
	foreach($persons as &$person) {

		// image
		$fid = $person['image'];
		$person['image'] = ($fid && isset($files[$fid])) ? $files[$fid] : null;
	}

	$html .= '<div class="list">';

	foreach($persons as $item) {

		$description = snd_entity_common_get_multilang_string($item, 'description_', $lang);

		$name = $item['full_name_with_titles'];
		$position = '';

		$item['positions'] = snd_admin_api_person_categories_paths( explode(';', $item['positions']) );

		// image		
		if (!$item['image'])
			$imageHTML = '<div class="image no-image"></div>';
		else
			$imageHTML = '<div class="image">'.theme('image_style', array('path' => $item['image']->uri, 'style_name' => 'osobnosti_photo', 'class' => array('image'))).'</div>';

		$html .= '<div id="person-'.$item['id'].'" class="item"><a href="' . $item['#link'] . '">
				'.$imageHTML.'
				<div class="info">
					<div class="name"><span class="value">' . $name . '</span></div>';

		foreach($item['positions'] as $position) {
			$html .= '
					<div class="position"><span class="value">' . $position['name'] . '</span></div>';
		}

		$html .= '
					<div class="description"><span class="value">' . $description . '</span></div>
				</div>
			</a></div>';
	}

	$html .= '</div>';
	$html .= '</div>';

	return $html;
}


// zisti ci sa prave zobrazuje detail osoby
function snd_entity__person_isCurrentLinkDetailPage() {
	list($path) = explode('/', current_path() );
	
	global $language;	
	$lang = $language->language;
	
	$urls = snd_entity__person_config_get_public_url_names();
	$url = $urls[$lang];
	
	return $path==$url;
}
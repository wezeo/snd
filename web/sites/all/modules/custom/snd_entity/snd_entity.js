/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

    
	// initialization of all widgets...
	$(function () {
        BASE_PATH = Drupal.settings.basePath + Drupal.settings.pathPrefix;
        multilangInit();
        widgetPositionsInit();
        widgetAuthorsInit();
        initWidgetNamePreview();
		initWidgetGalleryAdministration();
		initWidgetSubinscenations();
    });
	
	
	
	
	// ====================================================================================================================================
	
	var NO_OPTION_TEXT = 'vyberte si...',
        REFRESH_TEXT = 'Znovu načítať hodnoty',
        ADD_POSITION_TEXT = 'Pridať pozíciu',
        NO_POSITION_TEXT = 'Zatiaľ nie sú zadané žiadne pozície!',
        ADD_AUTHOR_TEXT = 'Pridať autora',
        NO_AUTHOR_TEXT = 'Zatiaľ nie sú zadaní žiadny autori!',
        POSITIONS_TEXTS = {
            noItem: NO_POSITION_TEXT,
            noOption: NO_OPTION_TEXT,
            addItem: ADD_POSITION_TEXT,
            refresh: REFRESH_TEXT
        },
        AUTHOR_TEXTS = {
	        noItem: NO_AUTHOR_TEXT,
            noOption: NO_OPTION_TEXT,
            addItem: ADD_AUTHOR_TEXT,
            refresh: REFRESH_TEXT
	    },
        REMOVE_TEXT = 'odstrániť',
        BASE_PATH;

	
	// MULTILANG PREVIEW WIDGET =======================================================================================================================
	
	// Staci dat pred multilang element <div class="widget-multilang-preview"><span class="empty">default empty message</span></div>
	
	function initWidgetNamePreview() {
		$('.widget-multilang-preview').each(function(){
			var $wnp = $(this);
			if ($wnp.length) {
				var partsIds = [], //['edit-pre-nominal-titles-', 'edit-first-name-', 'edit-last-name-', 'edit-post-nominal-titles-'],
					languages = ['sk', 'en', 'de'];

				$wnp.next().find('input, textarea').each(function(){
					if (this.id.indexOf('-'+languages[0])>0)
						partsIds.push(this.id.replace(languages[0],''));
				});
				//console.log(partsIds);

				$wnp.next().bind('change keydown keypress keyup copy cut', function () {
                    widgetNamePreviewRenderNames($wnp, languages, partsIds);
                });
				widgetNamePreviewRenderNames($wnp, languages, partsIds);
				setTimeout(function () {
                    widgetNamePreviewRenderNames($wnp, languages, partsIds);
                }, 1000);
			}
		});
	}

    function widgetNamePreviewRenderNames($wnp, languages, partsIds) {
        var names = {},
            hasValue = false,
            defaultHTML = $wnp;
        for (var i=0; i<languages.length; i++) {
            var name = [];
            for (var j=0; j<partsIds.length; j++) {
                name.push( widgetNamePreviewGetMultilangValue(partsIds[j], languages[i], languages) );
            }
            names[languages[i]] = name.join(' ').trim();

            if (names[languages[i]]!='')
                hasValue = true;
        }

        if (hasValue) {
            widgetNamePreviewPrintValues( $wnp, languages, names );
        }
        else {
            $wnp.html(defaultHTML);
        }
    }

    // return given language or other by priority sk, en, de
    function widgetNamePreviewGetMultilangValue(idPrefix, lang, languages) {
        var values = [];
        for (var i=0; i<languages.length; i++) {
            values[languages[i]] = $('#'+idPrefix+languages[i]).val();
        }
        var retVal = values[lang];
        for (i=0; i<languages.length; i++) {
            retVal = retVal || values[languages[i]];
        }
        return retVal;
    }


    function widgetNamePreviewPrintValues( $wnp, languages, values ) {
        var alreadyPrinted = {},
            rows = [];
        for (var i=0; i<languages.length; i++) {
            var lang = languages[i],
                langs = [];
            if (!alreadyPrinted[lang]) {
                var value = values[lang];
                langs.push(lang);
                for (var j=i+1; j<languages.length; j++) {
                    if (value==values[languages[j]]) {
                        langs.push(languages[j]);
                        alreadyPrinted[languages[j]] = true;
                    }
                }
                rows.push('<div><span class="lang">'+langs.join('/')+':</span> <span class="value">'+value+'</span></div>');
            }
        }

        $wnp.html( rows.join('') ).removeClass('count-1 count-2 count-3').addClass('count-'+rows.length);
    }
	
	
    // MULTILANG ====================================================================================================================================

    function multilangInit() {
	    $('.multilang-text').each(function (i, fieldset) {
	        var $fieldset = $(fieldset),
	            $fieldsetTitle = $fieldset.find('.fieldset-title'),
	            $inputs = $fieldset.find('input, textarea'),
                textareas = !!$inputs.not('input').length,
                isRichText = $fieldset.hasClass('rich-textarea'),
	            $inputWraps,
	            $description = $fieldset.find('.fieldset-description'),
                $customWraper = $('<div class="custom-wrappper multilang-text">'+
                                    '<div class="form-item">'+
                                        '<div class="multi-wrapper">' +
                                            '<span class="lang-tabs">'+
                                                '<span class="lang-label lang-label-sk selected">sk</span>'+
                                                '<span class="lang-label lang-label-en">en</span>'+
                                                '<span class="lang-label lang-label-de">de</span>'+
                                                '<span class="lang-label lang-label-all">all</span>'+
                                            '</span>'+
                                            '<label>' + $fieldsetTitle.html() + '</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'),
                $tabs = $customWraper.find('.lang-label'),
                $formItem = $customWraper.find('.form-item');

            $inputs
                .each(function (i, input) {
                    var $input = $(input);

                    if(!textareas)
                        $input.wrap('<div></div>');

                    if($input.hasClass('error'))
                        $tabs.eq(i).addClass('error');
                    if($input.val()){
                        $input.attr('data-prev-value', true);
                        $tabs.eq(i).addClass('has-value');
                    }
                });

            // reverse, because jQuery parents method returns in reversed order
            $inputWraps = isRichText ? [].reverse.call($inputs.parents('.text-format-wrapper')) : $inputs.parent();

            if(isRichText)
                $inputWraps
                    .find('.form-type-textarea > label')
                    .hide();

            $formItem
                .addClass(textareas ? 'form-type-textfield' : 'form-type-textarea')
                .children('.multi-wrapper')
                    .append($inputWraps);

            $inputWraps
                .wrapAll('<div class="inputs-wrap"></div>')
                .addClass('input-wrap')
                .hide()
                .eq(0)
                    .show();

            multilangInitShowHideLogic($tabs, $inputWraps);

            if($description.length)
                $description
                    .removeClass('fieldset-description')
                    .addClass('description')
                    .appendTo($customWraper.children('.form-item'));

            $fieldset.replaceWith($customWraper);

            // console.log('$inputs', $inputs);
            $inputs.bind('propertychange input', multilangInputChange);
            $customWraper.click(multilangClickLangTabs);
        });

        $('input.error').first().focus();

		if (window.CKEDITOR) {
			CKEDITOR.on( 'instanceReady', function( evt ) {
				var editor = evt.editor,
					$inputWraps = $(editor.element.$).parents('.inputs-wrap').children(),
					$tabs = $inputWraps.parents('.form-item').find('.lang-tabs').children();

				$(editor.document.$)
					.contents()
					.find("body")
						.bind('blur focus cut paste', callHandler);

				editor.on('key', callHandler); // because CKEditor somehow blocks keydown event when deleting all content at once

				// initial setup
				multilangCKEditorEventHandler(editor);
				setTimeout(multilangInitShowHideLogic, 0, $tabs, $inputWraps);

				function callHandler() {
					setTimeout(multilangCKEditorEventHandler, 0, editor);
				}
			} );
		}
    }
    
    function multilangCKEditorEventHandler(editor) {
        var data = editor.getData(),
            $inputWrap = $(editor.element.$).parents('.input-wrap'),
            $tabs = $inputWrap.parents('.form-item').find('.lang-tabs').children();

        if(data && data.match(/\w/) && data != '<p>&nbsp;</p>')
            // has value
            $tabs.eq($inputWrap.index()).addClass('has-value');
        else
            $tabs.eq($inputWrap.index()).removeClass('has-value');

    }
    
    function multilangClickLangTabs(e) {
        var $tab = $(e.target);
        if(!$tab.hasClass('lang-label'))
            return;

        var $parent = $tab.parent(),
            $inputWraps = $parent.siblings().find('.input-wrap');

        if($tab.hasClass('lang-label-all'))
            $inputWraps
                .show()
                .eq(0)
                .children()
                    .focus();
        else
            $inputWraps
                .hide()
                .eq($tab.index())
                    .show()
                        .children()
                        .focus();

        $parent
            .children()
                .removeClass('selected');
        $tab.addClass('selected');
    }

    function multilangInitShowHideLogic($tabs, $inputWraps) {
        if($tabs.filter('.has-value').length)
            $inputWraps.hide().eq(
                $tabs
                    .removeClass('selected')
                    .filter('.has-value')
                    .first()
                    .addClass('selected')
                    .index()
            ).show();

        if($tabs.filter('.error').length)
            $inputWraps.hide().eq(
                $tabs
                    .removeClass('selected')
                    .filter('.error')
                    .first()
                    .addClass('selected')
                    .index()
            ).show();
    }

    function multilangInputChange(e) { // because change method fires on blur
        if(!e.target.id)
            return;

        var valueChanged = false;

        if (e.type=='propertychange') // http://stackoverflow.com/a/17384341
            valueChanged = e.originalEvent.propertyName=='value';
        else
            valueChanged = true;
            
        if (valueChanged) {
            var $input = $(e.target),
                val = $input.val(),
                prev = $input.attr('data-prev-value'),
                $tabs = $input.parents('.form-item').find('.lang-tabs').children();

            // on change handle has-value class on tab
            if(val && !prev){
                $input.attr('data-prev-value', true);
                $tabs.eq($input.parent().index()).addClass('has-value');
            }else if(!val && prev){
                $input.attr('data-prev-value', '');
                $tabs.eq($input.parent().index()).removeClass('has-value');
            }

            // handle "all" tab has-value class
            if($tabs.filter('.has-value').not('.lang-label-all').length > 2)
                $tabs.last().addClass('has-value');
            else
                $tabs.last().removeClass('has-value');

            // clear error class on change
            if($input.hasClass('error')){
                $input.removeClass('error');
                $tabs.eq($input.parent().index()).removeClass('error');
            }
        }

    }


    // WIDGET-AUTHOR ====================================================================================================================================

	var authors = [],
        authorsLoadedCallbacks = [];
	
    function widgetAuthorsInit(){
        $('.widget-author').each(function(i, widget){
            var $widget = $(widget);
            widgetSharedInit(i, $widget, null, AUTHOR_TEXTS);
            $widget
                .find('.refresh-ui-button')
                .click(function (e) {
                    e.preventDefault();
                    $widget.find('.add-ui-selects > select').html('');
                    fetchAuthors('refresh');
                });
        });

        fetchAuthors();
    }


    function fetchAuthors(refresh){
        $('.widget-author')
            .find('.spinner')
                .addClass('spinning');
        $.getJSON(BASE_PATH + 'api/person/getAuthors', function(data) {
            authors = data;
            widgetAuthorListReady(data, refresh);
        });
    }

    function widgetAuthorsReverse($input, $list) {
        var inputTids = $input.val().split(';');

        $.each(authors, function (i, author) {
            if(author.name === null)
                return;

            if(inputTids.indexOf(author.tid.toString()) != -1)
                widgetSharedAddItemToList($list, author.tid, author.name);
        });
    }

    function widgetAuthorListReady(authors, refresh) {
        widgetSharedListReady('.widget-author', authors, null, 'autoAdd', refresh);
        $('.widget-author').each(function (i, widget) {
           var $widget = $(widget);
            widgetAuthorsReverse($widget.find('input'), $widget.find('.list'));
        });
    }


    // WIDGET-POSITIONS ====================================================================================================================================

    function widgetPositionsInit(){
        $('.widget-positions').each(function(i, widget){
            var $widget = $(widget);
            widgetSharedInit(i, $widget, widgetPositionsReverse, POSITIONS_TEXTS);
            $widget
                .find('.refresh-ui-button')
                .click(function (e) {
                    e.preventDefault();
                    $widget.find('.add-ui-selects > select').html('');
                    fetchPositions('refresh');
                })
        });

        fetchPositions();
    }

    function fetchPositions(refresh) {
        $.getJSON(BASE_PATH + 'api/person-categories', function (positions) {
            widgetPositionsListReady(positions, refresh);
        });
    }

    function widgetPositionsReverse($input, $list, $widget) {
        $.getJSON(BASE_PATH + 'api/person-categories/paths?tids=' + $input.val().replace(/;/g,','), function(positions){
            widgetPositionsFillPositions($list, positions);
            widgetSharedUpdateInput($widget);
        });
    }

    function widgetPositionsListReady(positions, refresh) {
        widgetSharedListReady('.widget-positions', positions, widgetPositionRecursive, 'autoAdd', refresh);
    }

    function widgetPositionRecursive(val, $widget){
        $widget
            .find('.spinner')
            .addClass('spinning');
        $.getJSON(BASE_PATH + 'api/person-categories?parent=' + val, function (positions) {
            widgetSharedAddSelect($widget, positions, widgetPositionRecursive, 'autoAdd');
        });
    }

    function widgetPositionsFillPositions($list, positions) {
        $.each(positions, function (i, position) {
            if(position.name === null)
                return;
            
            var listItemText = '';

            if(position.path)
                $.each(position.path, function (ii, pathItem) {
                    listItemText += (listItemText ? ' / ' : '') + pathItem;
                });
            listItemText += (listItemText ? ' / ' : '') + position.name;

            widgetSharedAddItemToList($list, position.tid, listItemText);
        });
    }

    // SHARED FUNCTIONS between widget author and position
    function widgetSharedInit(i, $widget, reverseCallback, texts){
        var $input = $widget.find('input'),
            $list = $('<ul class="list"><li class="list-item list-item-empty">' + texts.noItem + '</li></ul>'),
            $noPosition = $list.find('.list-item-empty'),
            $addSection = $('<div class="add-ui">' +
                '<div class="add-ui-selects">' +
                '<select class="form-select add-ui-dummy"><option value="">' + texts.noOption + '</option></select>' +
                '</div>' +
                '<button type="button" class="add-ui-button button" disabled>' + texts.addItem + '</button>' +
                '<button type="button" class="refresh-ui-button button" title="' + texts.refresh + '"><span class="spinner">&orarr;</span> </button>' +
                '</div>');

        if($input.val()){
            if(reverseCallback)
                reverseCallback($input, $list, $widget);
            $noPosition.hide();
        }

        $widget
            .delegate( '.list-item-delete', 'click', function (e) {
                e.preventDefault();

                $(this).parent().remove();

                if($list.children().length == 1) // NO_AUTHOR_TEXT only
                    $noPosition.show();

                widgetSharedUpdateInput($widget);
            } )
            .find('label')
            .after($list)
            .end();

        $input
            .hide()
            .before($addSection);

        $addSection
            .find('.add-ui-button')
            .hide()
                .click(function (e) {
                    e.preventDefault();

                    var listItemText = '',
                        positionID = 'id?';

                    $noPosition.hide();
                    $addSection.find('.form-select').each(function (i, select) {
                        positionID = select.value;
                        listItemText += (listItemText ? ' / ' : '') + $(select).children(':selected').text();
                    });

                    widgetSharedAddItemToList($list, positionID, listItemText);
                    widgetSharedUpdateInput($widget);

                    $widget.find('.form-select')
                        .first()
                        .val('')
                        .nextAll()
                        .remove();

                    $widget.find('.add-ui-button').attr('disabled', true);
                });

    }

    function widgetSharedAddSelect($widget, data, hasChildCb, autoAdd, refresh) {
        var $select = refresh ? $widget.find('.add-ui-selects > select').eq(0) : $('<select class="form-select"></select>'),
            $addButton = $widget.find('.add-ui-button');

        $widget
            .find('.spinner')
            .removeClass('spinning');

        $select.append('<option value="">' + NO_OPTION_TEXT + '</option>');

        if(refresh)
            $widget.find('.add-ui-selects > select').remove();

        if(!autoAdd)
            $addButton.show();

        $.each(data, function(i, item) {
            // console.log('option', item);
            $select.append($("<option></option>")
                .attr("value",item.tid)
                .attr("data-has-child", !!item.hasChild)
                .text(item.name));
        });
        // console.log('-------------------------------------------------------------------');

        if(data.length == 1){
            $select
                .children()
                .first() // NO_OPTION_TEXT
                .remove();
            $addButton.removeAttr('disabled');
        }

        $select
            .appendTo($widget.find('.add-ui-selects'))
            .change(function () {
                var $currentSelect = $(this);

                $currentSelect
                    .nextAll()
                    .remove();

                if(!this.value) // NO_OPTION case
                    return;

                if($currentSelect.children(':selected').attr('data-has-child') === 'true' && hasChildCb){
                    hasChildCb($currentSelect.val(), $widget);
                    $addButton.attr('disabled', true);
                }else{
                    $addButton.removeAttr('disabled');
                    if(autoAdd)
                        $addButton.click();
                }

            });

        if(autoAdd && data.length == 1)
            $addButton.click();
    }

    function widgetSharedListReady(selector, data, cb, aa, refresh) {
        $(selector).each(function(i, widget){
            var $widget = $(widget);

            $widget
                .find('.add-ui-dummy')
                    .remove();

            widgetSharedAddSelect($widget, data, cb, aa, refresh);
        });
    }

    function widgetSharedAddItemToList($list, positionID, listItemText) {
        $('<li class="list-item button"><a href="#" class="list-item-delete">'+REMOVE_TEXT+'</a></li>')
            .prepend(listItemText)
            .attr('data-item-id', positionID)
            .appendTo($list);
    }

    function widgetSharedUpdateInput($widget) {
        var result = '';
        $widget.find('[data-item-id]').each(function (i, item) {
            result += (result ? ';' : '') + $(item).attr('data-item-id');
        });
        $widget.find('input').val(result);
    }

	
	// WIDGET-GALLERY ====================================================================================================================================
	function initWidgetGalleryAdministration() {
		
		// for each gallery widget
		$('.widget-gallery').each(function(){
			var $gallery = $(this),
				$imagesField = $gallery.parent().find('.gallery-images'),
				$removeImagesField = $gallery.parent().find('.gallery-remove-images');
			
			// remove (toggle)
			$gallery.find('.image .remove').bind('click', function(){
				$(this).closest('.image').toggleClass('removed');
				updateFields();
			});
			
			// change order (move to left)
			$gallery.find('.image .order').bind('click', function(){
				var $image = $(this).closest('.image');
				$image.insertBefore( $image.prev() );
				updateFields();
			});
			
			// update input values .gallery-images and .gallery-remove-images
			function updateFields() {
				var ids = [],
					removedIds = [];
				$gallery.find('.image').each(function(){
					var $image = $(this),
						id = $image.attr('id').replace(/[a-z\-]/g,'');
					if (!$image.is('.removed'))
						ids.push(id);
					else
						removedIds.push(id);
					
					// update form fields
					$imagesField.val( ids.join(';') );
					$removeImagesField.val( removedIds.join(';') );
				});
			}
		})
		
	}
	
	
	// WIDGET-GALLERY ====================================================================================================================================
	// for change order of subinscenations
	function initWidgetSubinscenations() {
		$('.widget-subinscenations').each(function(){
			var $widget = $(this);

			$widget.find('.subinscenation .order').click(function(e){
				var $row = $(this).closest('.subinscenation');
				$row.insertBefore( $row.prev() );
				recalculateInputValue();				
			});
			
			function recalculateInputValue() {
				var $input = $widget.find('input[name="subinscenations"]'),
					ids = [];
				$widget.find('.subinscenation').each(function(){
					ids.push( $(this).attr('data-id') );
				});
				$input.val( ids.join(';') );
			}
		})
	}
	
	
	// plupload vylepsenie
	$(function(){
		if ($('.plupload-element').length) {
			var uploader = $('.plupload-element').pluploadQueue();

			// add remove all files at once button
			var removeSel = 'a.plupload_remove';
			$('div.plupload_buttons').append('<a href="#" class="plupload_button plupload_remove">'+Drupal.t('Remove all files')+'</a>');
			$(removeSel).addClass('display-none-important');

			// remove all files at once handler
			$('a.plupload_remove').first().click(function(e) {
				e.preventDefault();
				uploader.splice();
				$(removeSel).addClass('display-none-important');
			});
			
			// shows "remove" button on every file
			var interval = 1000;
			setTimeout(function () {
				$('.plupload_file_action *').addClass('display-inline-block-important');
				setTimeout( arguments.callee, interval );
			}, interval);
				
			// show delete icon for single files in the queue after files have been added (override default plupload behaviour)
			uploader.bind('FilesAdded', function(up, files) {			
				$(removeSel).removeClass('display-none-important');	             
			});
			
			// show delete icon for single files in the queue after one has been removed (override default plupload behaviour)
			uploader.bind('FilesRemoved', function(up, files) {
				if (up.files.length==0)
					$(removeSel).addClass('display-none-important');
			});
		}
	});
	
})(jQuery, Drupal, this, this.document);
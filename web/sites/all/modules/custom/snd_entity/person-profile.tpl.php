<?php

/**
 * @file
 * Template of person profile.
 */
	
	// pouzi $person['full_name_with_titles']
	//die_r($person);
	

	$positions = array();
	foreach($person['positions'] as $position) {
		if (!is_array($position['path'])) {
			$position['path'] = array();
		}
		
		$position['path'][] = $position['name'];
		$positions[] = implode(' / ', $position['path']);
	}

	
	echo '
	<div class="person">
		<div class="profile">
			<div class="info">';
			
				// image
				if (!$person['#image'])
					echo '
				<div class="image no-image"></div>';
				else {
					$imageHTML = theme('image_style', array('path' => $person['#image']->uri, 'style_name' => 'person_profile_photo'));
					echo '
				<div class="image">'.$imageHTML.'</div>';
				}
				
				echo '				
				<h2 class="title" title="'.$person['full_name_with_titles'].'">'.$person['full_name_with_titles'].'</h2>
				<div class="positions">';
					foreach ($positions as $position) {
						echo '<div class="position"><span class="part">'.$position.'</span></div>';
					}
				echo '</div>';
				
				if ($person['born'] || $person['#birth_place']){
					echo '
					<div class="birth">
						<span class="label">'.(($person['sex'] == 'male') ? 'Narodený' : 'Narodená').'</span>
						<span class="space"> </span>';
						if ($person['born']) {
							echo '<span class="date">'.date("d. m. Y", strToTime($person['born'])).'</span>';
						}
						echo '<span class="space"> </span>
						<span class="place">'.$person['#birth_place'].'</span>
					</div>
					';
				}
				if ($person['died'] || $person['#death_place']){
					echo '<div class="death">
						<span class="label">'.(($person['sex'] == 'male') ? 'Zosnulý' : 'Zosnulá').'</span>
						<span class="space"> </span>';
						if ($person['died']){
							echo '<span class="date">'.date("d. m. Y", strToTime($person['died'])).'</span>';
						}
						echo '<span class="space"> </span>
						<span class="place">'.$person['#death_place'].'</span>
					</div>';
				}
				
				$sections = $person['sections'];
				if($sections['snd'] || $sections['cinohra'] || $sections['opera'] || $sections['balet']) {
					echo '<div class="angazman">
						<span class="label">'.t('Engagement').'</span>
						<span class="space"> </span>
						<span class="place">';
							$items = array();
							if($sections['snd'])
								$items[] = t('SND');
							
							if($sections['cinohra'])
								$items[] = t('Drama');

							if($sections['opera'])
								$items[] = t('Opera');

							if($sections['balet'])
								$items[] = t('Ballet');
							
							echo implode(', ', $items);							
						echo '</span>
					</div>';
				}
			echo '</div>
			<div class="body">
				<div class="description">'.$person['description'].'</div>
				<div class="cv">'.$person['#cv'].'</div>
			</div>
		</div>';

		if(is_array($person['#gallery']) && !empty($person['#gallery'])) {

			echo '<div class="field field-name-field-gallery field-type-image field-label-hidden"><div class="field-items">';

			foreach($person['#gallery'] as $galleryImage) {

				$imageSmallUrl = image_style_url('medium', $galleryImage->uri);
				$imageOpenedUrl = image_style_url('gallery_opened', $galleryImage->uri);
				echo '<div class="field-item"><a href="'.$imageOpenedUrl.'" class="colorbox" rel="gallery-person-'.$person['id'].'"><img typeof="foaf:Image" src="'.$imageSmallUrl.'" alt="" title=""></a></div>';
			}

			echo '</div></div>';
		}

	echo '</div>';

?>
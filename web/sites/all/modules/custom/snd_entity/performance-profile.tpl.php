<?php
/**
 * @file
 * Template of performance.
 */
 
	// pouzi $person['full_name_with_titles']
	

	//print('<pre>');
	//print( print_r($performance, 1) );
	//print('</pre>');

	//die_r($performance);

	// image
	if (!$performance['inscenation']['#image'])
		$imageHTML = '<div class="image no-image"></div>';
	else
		$imageHTML = '<div class="image">'.theme('image_style', array('path' => $performance['inscenation']['#image']->uri, 'style_name' => 'inscenation_main_detail')).'</div>';

	
 	echo '
	<div class="predstavenie inscenation-or-performance">
		'.$imageHTML.'
		<div class="info">
			<h3 class="author"><label>Autor:</label><span class="space"> </span><span class="value">';
			foreach ($performance['inscenation']['#authors'] as $author) {
				echo '<span>'.$author['name'].'</span>';
			}
			echo '</span></h3>
			<h2 class="title" title="'.$performance['inscenation']['title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$performance['inscenation']['title'].'</span></h2>
			<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span><span class="value">'.$performance['inscenation']['#artistic_body'].'</span></div>
			<div class="teaser"><span>'.$performance['inscenation']['teaser'].'</span></div>
			<div class="date"><label>Dátum konania</label><span class="space"> </span><span class="value"><span class="weekday"></span> <span class="on-date">'.$performance['#date'].'</span> <span class="time-from">'.$performance['#time_from'].'</span> <span class="time-to">'.$performance['#time_to'].'</span></span></div>
			<div class="clearfix info-row">
				<div class="place tid-'.$performance['place'].'" title="'.$performance['#place'].', '.$performance['#hall'].'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$performance['#place'].'<span class="other-place">'.$performance['#other_place'].'</span><span class="hall">'.$performance['#hall'].'</span></span></div>
				<!--<div class="premiere"><label>Premiéra:</label><span class="space"> </span><span class="value">31. marca a 1. apríla</span></div>-->
			</div>
		</div>';

		if ( !empty($performance['inscenation']['#inscenation_siblings']) ) {
			echo '<select id="inscenation-siblings">';
			foreach ($performance['inscenation']['#inscenation_siblings'] as $inscenation_sibling) {
				echo '<option data-url="'.$inscenation_sibling['#link'].'" '.(( $inscenation_sibling['id'] == $inscenation['id'] ) ? 'selected' : '').'>'.$inscenation_sibling['season_year'].'/'.($inscenation_sibling['season_year']+1).'</option>';
			}
			echo '</select>';
		}

		echo '<div class="performances">
			<h3>Plánované predstavenia</h3>
			<div class="list">';
		if(is_array($performance['inscenation']['performances'])) {
			foreach ($performance['inscenation']['performances'] as $performances) {
				echo '<div class="performance state-'.$performances['state'].'">
							<div class="date"><label>Dátum konania:</label><span class="space"> </span><span class="value"><span class="weekday">'.$performances['#weekday'].'</span> <span class="on-date">'.$performances['#date'].'</span></span></div>
							<div class="time"><span class="time-from">'.$performances['#time_from'].'</span> <span class="time-to">'.$performances['#time_to'].'</span></div>
							<div class="state"><span>'.$performances['#state'].'</span></div>
							<div class="action"><a href="'.$performances['#link'].'">Zobraziť detail</a></div>
						</div>';
			}
		}
			echo '</div>
		</div>';

		echo '<div class="description"><span>'.$performance['inscenation']['description'].'</span></div>';
		


		if ( !empty($performance['inscenation']['#subinscenations']) ) {
			echo '<h3>Táto inscenácia sa skladá z viacerých inscenácií:</h3>';
			foreach ($performance['inscenation']['#subinscenations'] as $subinscenation) {
				echo '<div class="sub-inscenations"><a href="'.$subinscenation['#link'].'">';
					foreach ($subinscenation['#authors'] as $subauthor) {
						echo '<span class="author">'.$subauthor['name'].'</span>';
					}
					echo ': '.$subinscenation['#title'].'
				</a></div>';
			}
		}

		echo '<div class="gallery">
			<h3>Fotografie a videá</h3>
			<div class="list">
				<a href="/sites/default/files/angelika.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/angelika.jpg" /></a>
				<a href="/sites/default/files/bacova-zena.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bacova-zena.jpg" /></a>
				<a href="/sites/default/files/bloodlines.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/bloodlines.jpg" /></a>
				<a href="/sites/default/files/carmen.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/carmen.jpg" /></a>
				<a href="/sites/default/files/don-giovanni.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/don-giovanni.jpg" /></a>
				<a href="/sites/default/files/desatoro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/desatoro.jpg" /></a>
				<a href="/sites/default/files/figaro.jpg" data-rel="lightcase:predstavenie-gallery"><img src="/sites/default/files/figaro.jpg" /></a>
			</div>
		</div>

		<div class="obsadenie">
			<h3>Obsadenie</h3>			
			<div class="list">';
				foreach ($performance['starring_values'] as $role => $persons) {
					echo '<div class="member"><span class="rola">'.$role.'</span><span class="space"> </span><span class="value">';
					foreach ($persons as $person) {
						echo '<span class="person"><a href="'.$person['url'].'"><span class="name">'.$person['name'].'</span></a>';
						if ( $person['note'] ) echo '<span class="note">, '.$person['note'].'</span>';
						echo '</span>';
					}
					echo '</div>';
				}

			echo '</div>
		</div>

		<div class="vyroba">
			<h3>Výroba</h3>
			<div class="list">';
				foreach ($performance['production_values'] as $role => $persons) {
					echo '<div class="member"><span class="rola">'.$role.'</span><span class="space"> </span><span class="value">';
					foreach ($persons as $person) {
						echo '<span class="person"><a href="'.$person['url'].'"><span class="name">'.$person['name'].'</span></a>';
						if ( $person['note'] ) echo '<span class="note">, '.$person['note'].'</span>';
						echo '</span>';
					}
					echo '</div>';
				}

			echo '</div>
		</div>';

		if(is_array($performance['inscenation']['#gallery']) && !empty($performance['inscenation']['#gallery'])) {

			echo '<div class="field field-name-field-gallery field-type-image field-label-hidden"><div class="field-items">';

			foreach($performance['inscenation']['#gallery'] as $galleryImage) {

				$imageSmallUrl = image_style_url('medium', $galleryImage->uri);
				$imageOpenedUrl = image_style_url('gallery_opened', $galleryImage->uri);
				echo '<div class="field-item"><a href="'.$imageOpenedUrl.'" class="colorbox" rel="gallery-inscenation-'.$performance['inscenation']['id'].'"><img typeof="foaf:Image" src="'.$imageSmallUrl.'" alt="" title=""></a></div>';
			}

			echo '</div></div>';
		}
	echo '
	</div>';

?>
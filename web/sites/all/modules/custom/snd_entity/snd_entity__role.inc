<?php

function snd_entity__role_menu_enrich( &$items ) {

	// LIST
	$items['admin/'.snd_entity__role_config_get_form_url_name()] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Zoznam rolí',
		'title callback' => false,
		'page callback' => 'snd_entity__role_list',
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// CREATE
	$items['admin/'.snd_entity__role_config_get_form_url_name().'/new'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Pridať rolu',
		'title callback' => false,
		'page callback' => 'snd_entity__role_new',
		'access callback' => 'user_is_logged_in', // TODO
	);
		
	// EDIT
	$items['admin/'.snd_entity__role_config_get_form_url_name().'/%/edit'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Editovať rolu',
		'title callback' => false,
		'page callback' => 'snd_entity__role_edit',
		'page arguments' => array(2),
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// DELETE
	$items['admin/'.snd_entity__role_config_get_form_url_name().'/%/delete'] = array(
		'type' => MENU_CALLBACK,
		'title' => 'Odstrániť rolu',
		'title callback' => false,
		'page callback' => 'snd_entity__role_delete',
		'page arguments' => array(2),
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// AJAX service - zoznam vsetkych aktivnych roli
	$items['api/role/getRoles'] = array(
		'type' => MENU_CALLBACK,
		'page callback' => 'snd_entity__api_role_getRoles',
		'page arguments' => array(3),
		'access callback' => 'user_is_logged_in', // TODO
	);
	
	// AJAX service - rychle pridanie role 	
	$items['api/role/addRole'] = array(
		'type' => MENU_CALLBACK,
		'page callback' => 'snd_entity__api_role_addRole',
		'access callback' => 'user_is_logged_in', // TODO
	);
}

// --------------------------------------------------------------------------------------------------------------------------------------------
// - SETUP ------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

function snd_entity__role_get_entity_name() {
	return 'role';
}

function snd_entity__role_config_get_form_id() {
	return 'snd_entity__role_form';
}

function snd_entity__role_config_get_public_url_names() {
	return array('sk' => 'profil', 'en' => 'profile', 'de' => 'profil');
}

function snd_entity__role_config_get_form_url_name() {
	return 'role';
}

function snd_entity__role_config_get_table_name() {
	return 'custom_snd_role';
}

function snd_entity__role_config_get_table_alias() {
	return 'r';
}

function snd_entity__role_config_get_public_url_name( $lang='en' ) {
	$url_names = snd_entity__role_config_get_public_url_names();
	return $url_names[$lang];
}

// identifikator je Meno a Priezvisko - pouziva sa do hlasok
function snd_entity__role_get_text_identifier($data) {
	$lang = snd_entity_common_get_default_language();
	
	$name = snd_entity_common_get_multilang_string($data, 'name_', $lang);
	
	return '#'.$data['id'].' '.$name;
}

// --------------------------------------------------------------------------------------------------------------------------------------------
// - MAIN ACTIONS -----------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------


// LIST
function snd_entity__role_list() {
	snd_entity_common_add_assets();
	
	// Adds filter form to the build array.
	$form = drupal_get_form('snd_entity__role_list_filter_form');
  
	// Add role link
	$add_link_text = 'Pridať rolu';
	array_unshift( $form, array('#markup' => '<ul class="action-links"><li>'.l($add_link_text, 'admin/'.snd_entity__role_config_get_form_url_name().'/new').'</li></ul>') );

	$header = array(
		array('data' => 'Id', 'field' => 'id'),
		array('data' => 'Názov SK', 'field' => 'name_sk'),
		array('data' => 'Názov EN', 'field' => 'name_en'),
		array('data' => 'Názov DE', 'field' => 'name_de'),
		array('data' => 'Typ', 'field' => 'is_character', '#function' => 'snd_entity__role_transform_is_character'),
		array('data' => 'Váha', 'field' => 'weight'),
		array('data' => 'Operácie'), 
	);
	
	$url = 'admin/'.snd_entity__role_config_get_form_url_name().'/';
	
	global $language;	
	$lang = $language->language;
	$urls = snd_entity__role_config_get_public_url_names();
	$public_url = $urls[$lang];

	$languages = snd_entity_common_get_languages();
	$filter_conditions = db_and();
	
	// filter: contains_string
	if (isset($form['filter']['contains_string']) && !empty($form['filter']['contains_string']['#default_value'])) {
		$or = db_or();
		$str = $form['filter']['contains_string']['#default_value'];
		foreach($languages as $lang) {
			$or->condition('name_'.$lang,  '%'.$str.'%',  'LIKE');
		}
		$or->condition('id',  '%'.$str.'%',  'LIKE');
		$filter_conditions->condition($or);
	}
	
	// filter: active
	if (isset($form['filter']['is_character']) && $form['filter']['is_character']['#default_value']!='') {		
		$filter_conditions->condition('is_character',  $form['filter']['is_character']['#default_value']);
	}
	
	snd_entity_common_get_list( $form, $header, snd_entity__role_config_get_table_name(), snd_entity__role_config_get_table_alias(), $url, $public_url, $filter_conditions, '', 'snd_entity__role_extend_link' );

	return $form;
}

function snd_entity__role_transform_is_character($value) {
	return $value==1 ? 'účinkujúci' : 'výroba';
}

function snd_entity__role_extend_link( $operations, $item ) {
	return str_replace('view', '', $operations);
}


function snd_entity__role_list_filter_form($form, &$form_state) {

	$session_key = snd_entity_common_list_filter_get_session_key($form_state);
	$filter_expanded = isset($_SESSION[$session_key]);
	
	$form['filter'] = array(
		'#type' => 'fieldset',
		'#collapsible' => true,
		'#collapsed' => !$filter_expanded,
		'#attributes' => array('class' => array('inline-form')),
		'#title' => 'Filter'
	);
	
		$form['filter']['contains_string'] = array(
			'#type' => 'textfield',
			'#title' => 'Obsahuje reťazec',
		);

		// SELECT typ
		$options = array('' => '', 0 => 'výroba', 1 => 'účinkujúci');
		$form['filter']['is_character'] = array(
			'#type' => 'select',
			'#title' => 'Typ role',
			'#options' => $options,
			'#default_value' => $_GET['artistic_body'],
		);

	$form['filter']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
		'#submit' => array('snd_entity_common_list_filter_form_submit'),
	);

	$form['filter']['reset'] = array(
		'#type' => 'submit',
		'#value' => t('Reset'),
		'#submit' => array('snd_entity_common_list_filter_form_reset'),
	);
	
	snd_entity_common_default_values( $form['filter'], $_SESSION[$session_key] );
	
	return $form;
}

// NEW (CREATE)
function snd_entity__role_new() {
	snd_entity_common_add_assets();
	$form_id = snd_entity__role_config_get_form_id();
	$form_state = array();
	
	// default values
	$form_state['#default_values'] = array();
	
	return drupal_build_form($form_id, $form_state);
}

// EDIT (UPDATE)
function snd_entity__role_edit( $id ) {
	snd_entity_common_add_assets();
	$form_id = snd_entity__role_config_get_form_id();
	$form_state = array();
	
	// default values
	$form_state['#default_values'] = snd_entity__api_role_get( $id );
	
	$form_state['#id'] = $id;
	
	return drupal_build_form($form_id, $form_state);
}

// DELETE
function snd_entity__role_delete( $id ) {
	$role = snd_entity__api_role_get( $id );
	
	if (!$role) {
		drupal_not_found();
		exit;
	}
	
	$identifier = snd_entity__role_get_text_identifier($role);	
	drupal_set_title('Naozaj chcete odstrániť rolu "'.$identifier.'"?');
	
	$form_id = snd_entity__role_config_get_form_id() . '_delete';
	$form_state = array('#id' => $id);	
	return drupal_build_form($form_id, $form_state);
}


// --------------------------------------------------------------------------------------------------------------------------------------------
// - FORM DEFINITION --------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------

// form structrure
function snd_entity__role_form($node, $form_state) {
	$form = array();
	
	snd_entity_common_add_multilang_field( $form, 'name', 'textfield', 'Názov role');
	
	// RADIOS
	$form['is_character'] = array(
		'#type' => 'radios',
		'#title' => 'Typ role',
		'#attributes' => array('class' => array('inline-form-items')),
		'#required' => true,
		'#options' => array(0=>'výroba', 1=>'účinkujúci'),
	);
	
	// SELECT weight
	$options = array();
	for($i=-50;$i<=50;$i++)
		$options[$i] = $i;
	$form['weight'] = array(
		'#type' => 'select',
		'#title' => 'Váha/poradie',
		'#description' => 'Určuje poradie v zozname rolí - prebíja abecedné poradie.',
		'#options' => $options,
		'#default_value' => 0,
	);
		
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);

	snd_entity_common_default_values( $form, $form_state['#default_values'] );
	
	return $form;
}


// form validation
function snd_entity__role_form_validate($form, &$form_state) {
	$value = $form_state['values'];	

	snd_entity_common_validate_multilang_field($form_state, 'name_');
}


// form save
function snd_entity__role_form_submit($form, $form_state) {

	//die_r($image); 
	
	if (!isset($form_state['#id'])) {
		$id = snd_entity__api_role_create( $form_state['values'] );
	}
	else {
		$id = $form_state['#id'];
		$result = snd_entity__api_role_update( $id, $form_state['values'] );
		
		if ($result===false)
			drupal_set_message('Nebola však vykonaná žiadna zmena.');
	}
	
	// message
	$form_state['values']['id'] = $id;
	$identifier = snd_entity__role_get_text_identifier($form_state['values']);
	drupal_set_message('Rola "'.$identifier.'" bola uložená.');
		
	drupal_goto('admin/'.snd_entity__role_config_get_form_url_name());
}



// form structrure for DELETE form
function snd_entity__role_form_delete($node, $form_state) {
	$form = array();

	// NAME PREVIEW
	$form['test'] = array(
		'#markup' => '<p>Táto akcia sa už nedá vrátiť!</p>',
    );
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Delete')
	);
	
	return $form;
}


// submitting DELETE form
function snd_entity__role_form_delete_submit($node, $form_state) {
	
	$id = $form_state['#id'];
	$role = snd_entity__api_role_get( $id );
	
	$identifier = snd_entity__role_get_text_identifier($role);
	drupal_set_message('Rola "'.$identifier.'" bola odstránená.');
	
	snd_entity__api_role_delete( $id );

	drupal_goto('admin/'.snd_entity__role_config_get_form_url_name());
}

// @$action 'create' | 'modify' | 'delete'
function snd_entity__role_prepare_values_for_save( $values, $action, $history='' ) {
	// general prepare of form values
	$values = snd_entity_common_get_params( $values );
	return $values;
}

function snd_entity__role_postprocess_data_when_get( &$result ) {
	global $language;
	
	$lang = $language->language;
	$result['name'] = snd_entity_common_get_multilang_string($result, 'name_', $lang);
	
	return $result;
}



// --------------------------------------------------------------------------------------------------------------------------------------------
// - ROLE MODEL (API) -----------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------



// API GET
function snd_entity__api_role_get( $id ) {
	
	$query = db_select( snd_entity__role_config_get_table_name(), snd_entity__role_config_get_table_alias() )
				->fields(snd_entity__role_config_get_table_alias())
				->condition('id', $id);
				
    $result = $query->execute();
	
	if ($result->rowCount()==0)
		return false;
	
	$item = $result->fetchAssoc();
	$item = snd_entity__role_postprocess_data_when_get($item);	
	
	return $item;
}


// API CREATE
function snd_entity__api_role_create( $form_values ) {
	
	$values = snd_entity__role_prepare_values_for_save( $form_values, 'create');
		
	$id = db_insert( snd_entity__role_config_get_table_name() )
			->fields($values)
			->execute();
			
	// clears page cache
	cache_clear_all(NULL, 'cache_page');
	
	return $id;
}


// API UPDATE
function snd_entity__api_role_update( $id, $form_values ) {
	
	// read actual data
	$role = snd_entity__api_role_get( $id );
	
	$values = snd_entity__role_prepare_values_for_save( $form_values, 'modify [...]');

	// db UPDATE query
	$num_updated = db_update( snd_entity__role_config_get_table_name() ) 
					->fields($values)
					->condition('id', $id)
					->execute();
	
	// clears page cache
	cache_clear_all(NULL, 'cache_page');
	
	return $num_updated>0;
}


// API DELETE
function snd_entity__api_role_delete( $id ) {
	global $user;
	
	// read actual data
	$role = snd_entity__api_role_get( $id );
	$action = 'delete';
	
	// timestamp	
	$values = array(
		'deleted' => 1,
	);
	
	// db UPDATE query
	$num_updated = db_update( snd_entity__role_config_get_table_name() ) 
					->fields($values)
					->condition('id', $id)
					->execute();
	
	// clears page cache
	cache_clear_all(NULL, 'cache_page');
	
	return $num_updated>0;
}


// AJAX service - zoznam vsetkych aktivnych roli 	
function snd_entity__api_role_getRoles($is_character) {
	
	$query = db_select( snd_entity__role_config_get_table_name(), snd_entity__role_config_get_table_alias() )
				->fields(snd_entity__role_config_get_table_alias())
				->condition('deleted', '0')
				->orderBy('weight', 'asc')
				->orderBy('name_sk', 'asc')
				->orderBy('name_en', 'asc')
				->orderBy('name_de', 'asc');
	
	if (is_numeric($is_character))
		$query->condition('is_character', $is_character);
	
	$lang = snd_entity_common_get_default_language();			
	$result = $query->execute();
	$roles = array();
	foreach ($result as $item) {
		$item =  (array) $item;
		$item['name'] = snd_entity_common_get_multilang_string($item, 'name_', $lang);
		$roles[] = array('id' => $item['id'], 'name' => $item['name']. ' (' . $item['id'] .')');
	}
	drupal_json_output($roles);
}

// AJAX service - rychle pridanie role 	
function snd_entity__api_role_addRole() {
	
	$values = array('name_sk' => $_GET['name'], 'is_character' => $_GET['character']);
		
	$id = db_insert( snd_entity__role_config_get_table_name() )
			->fields($values)
			->execute();
			
	drupal_json_output($id);
}
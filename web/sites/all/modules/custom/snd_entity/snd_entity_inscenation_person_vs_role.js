/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

	var BASE_PATH,
		$widgets;
	
	
	$(function () {
		
		BASE_PATH = Drupal.settings.basePath + Drupal.settings.pathPrefix;
	   
		$widgets = $('.widget-person-vs-role');

		$widgets.each(function(){
			initPersonVsRole(this);
		});
		
		refreshEnums();		
    });
	
	
	
	
	
	var inputCounter = 1,
		inputId = 'fwpvsr';
	
	function getNewId() {
		return inputId+(inputCounter++);
	}
	
	function initPersonVsRole( container ) {
		
		var $widget = $(container),
			$container = $widget.addClass('inline-form').find('.fieldset-wrapper'),
			isPerformance = $widget.is('.performance-version');
		
		var $list = $(
				'<div class="list">'+
					'<table><tbody></tbody></table>'+
				'</div>').appendTo($container);
		
		if (!isPerformance) {
			var id = getNewId(),
				$roleSelectContainer = $(
					'<div class="form-item form-type-select">'+
						'<label for="'+id+'">Existujúca rola</label>'+
						'<select id="'+id+'" class="form-select select-role"><option value=""></option></select>'+
					'</div>');
			
			var id = getNewId(),
				$roleTextContainer = $(
					'<div class="form-item form-type-textfield">'+
						'<label for="'+id+'">Nová rola (sk)</label>'+
						'<input size="24" type="text" id="'+id+'" class="form-text new-role">'+
					'</div>');
					
			var id = getNewId(),
				$personSelectContainer = $(
					'<div class="form-item form-type-select">'+
						'<label for="'+id+'">Osoba</label>'+
						'<select id="'+id+'" class="form-select select-persons"><option value=""></option></select>'+
					'</div>');
					
			var $addButton = $('<input type="button" value="Pridať" class="form-submit add-button">').click(function(){
					submitPersonVsRole( this );
				}),
				$refreshButton = $('<input type="button" value="Obnoviť" class="form-submit refresh-button">').click(function(){
					loadPersons();
					loadRoles( this );
				});
				
			$container
				.append($roleSelectContainer)
				.append($roleTextContainer)
				.append($personSelectContainer)
				.append('<br />')
				.append($addButton)
				.append($refreshButton);
		}
	}
	
	function refreshEnums() {
		loadPersons();
		$widgets.each(function(){
			loadRoles(this);
		});
	}
	
	var personsById = [];
	
	function loadPersons() {
		$.getJSON(BASE_PATH + 'api/person/getPersons', function(data) {
			
			for (var i=0; i<data.length; i++) {
				personsById[data[i].id] = data[i].name;
			}
			
			$('.select-persons').each(function(){
				var options = '<option value=""></option>';
				for (var i=0; i<data.length; i++) {
					options += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
					personsById[data[i].id] = data[i].name;
				}
				$(this).html(options);				
			});
			reconstructAllLists();
		});
	}
	
	var rolesById = [];
	
	function loadRoles( el, callback ) {
		var $widget = $(el).closest('.widget-person-vs-role'),
			is_character = $widget.is('.characters') ? 1 : 0;
		$.getJSON(BASE_PATH + 'api/role/getRoles/'+is_character, function(data) {
			
			for (var i=0; i<data.length; i++) {
				rolesById[data[i].id] = data[i].name;
			}
				
			$widget.find('.select-role').each(function(){
				var options = '<option value=""></option>';
				for (var i=0; i<data.length; i++) {
					options += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				$(this).html(options);
			});
			
			reconstructAllLists();
			
			if (callback)
				callback();
		});
	}
	
	function submitPersonVsRole( el ) {
		var $widget = $(el).closest('.widget-person-vs-role'),
			is_character = $widget.is('.characters') ? 1 : 0;
			existingRoleId = $widget.find('.select-role').val(),
			newRole = $widget.find('.new-role').val(),
			personId = $widget.find('.select-persons').val();
		if (!existingRoleId && !newRole)
			return alert('Musíte vybrať existujúcu rolu, alebo zadať novú.');
		if (!personId)
			return alert('Musíte vybrať osobu.');
		// prioritu ma zadana nova rola
		if (newRole) {
			var backupRoleId = $widget.find('.select-role').val();
			
			// vytvori novu rolu
			$.getJSON(BASE_PATH + 'api/role/addRole/?name='+encodeURIComponent(newRole)+'&character='+is_character, function(newRoleId) {
				loadRoles( $widget[0], function(){
					addPersonVsRole( $widget[0], newRoleId, personId );
					$widget.find('.select-role').val(newRoleId);
				});				
			});
		}
		else {
			// prida hned (nevytvara novu rolu)
			addPersonVsRole( $widget[0], existingRoleId, personId );
		}
		$widget.find('.new-role').val('');
		$widget.find('.select-persons').val('');
	}
	
	function addPersonVsRole( widgetEl, roleId, personId ) {
		var $widget = $(widgetEl),
			$row = findOrCreateTableRow( widgetEl, roleId ),
			$person = findOrCreatePersonElement( $row, personId );
			
		updateHiddenField( widgetEl );
	}
	
	function findOrCreateTableRow( widgetEl, roleId ) {
		var $widget = $(widgetEl),
			$rows = $widget.find('tr'),
			$foundRow;
			
		// find row by roleId
		$rows.each(function(){
			if (this.roleId==roleId) {
				$foundRow = $(this);
				return false;
			}				
		});

		// or creates new one
		if (!$foundRow && rolesById[roleId]) {
			var $tr = $('<tr><td class="role" width="15%">'+replaceCount(rolesById[roleId])+':</td><td class="persons"></td>'+
							'<td class="vs-actions">'+
								'<a href="#" class="up" title="presunúť vyššie">&uparrow;</a>'+
								'<a href="#" class="remove" title="odstrániť celý riadok">&times;</a>'+
							'</td></tr>');
			$tr.find('.up').click(moveUpRow);
			$tr.find('.remove').click(removeRow);
			$tr[0].roleId = roleId;
			$widget.find('tbody').append($tr);
			$foundRow = $tr;
		}

		return $foundRow;
	}
	
	function findOrCreatePersonElement( $tableRow, personId ) {
		var $persons = $tableRow.find('.person'),
			$foundPerson;
			
		// find row by roleId
		$persons.each(function(){
			if (this.personId==personId) {
				$foundPerson = $(this);
				return false;
			}				
		});

		// or creates new one
		if (!$foundPerson && personsById[personId]) {
			var $person = $('<span class="person">'+replaceCount(personsById[personId])+'&nbsp;'+
							'<span class="vs-actions">'+
								'<a href="#" class="left" title="presunúť doľava">&leftarrow;</a>'+
								'<a href="#" class="remove" title="odstrániť osobu">&times;</a>'+
							'</span></span>');
			$person.find('.left').click(moveLeftPerson);
			$person.find('.remove').click(removePerson);
			$person[0].personId = personId;
			$tableRow.find('.persons').append($person);
			$foundPerson = $person;
		}

		return $foundPerson;
	}
	
	function moveUpRow( e ) {
		e.preventDefault();
		var $this = $(e.target),
			$tr = $this.closest('tr'),
			$prev = $tr.prev();
		if ($prev.is('tr')) {
			$prev.insertAfter($tr);
			updateHiddenField( e.target );
		}
	}
	
	function removeRow( e ) {
		e.preventDefault();
		if (window.confirm('Chcete naozaj odstrániť túto rolu z inscenácie?')) {
			var $widget = (e.target).closest('.widget-person-vs-role');
			$(e.target).closest('tr').remove();
			updateHiddenField( $widget );
		}
	}
	
	function moveLeftPerson( e ) {
		e.preventDefault();
		var $this = $(e.target),
			$person = $this.closest('.person'),
			$prev = $person.prev();
		if ($prev.is('.person')) {
			$prev.insertAfter($person);
			updateHiddenField( e.target );
		}
	}
	
	function removePerson( e ) {
		e.preventDefault();
		// for inscenation
		if ($(e.target).closest('.performance-version').length==0) {
			if (window.confirm('Chcete naozaj odstrániť túto osobu z tejto role?')) {
				var $widget = $(e.target).closest('.widget-person-vs-role'),
					$persons = $(e.target).closest('.persons');
				$(e.target).closest('.person').remove();
				// if it was last
				if ($persons.find('.person').length==0) {				
					e.target = $widget.find('.vs-actions .remove')[0];
					return removeRow( e );
				}
				updateHiddenField( $widget );
			}
		}
		// for performance
		else {
			var $person = $(e.target).closest('.person');
			$person.toggleClass('removed');
			$person.find('a.remove').html( $person.is('.removed') ? '&olarr;' : '&times;' );
			updateExcludedPeopleForPerformance();
		}
	}
	
	function replaceCount(str) {
		return str.replace(/\( ?[0-9]+\)/,'');
	}
		
	function updateHiddenField( widgetEl ) {
		var $widget = $(widgetEl).closest('.widget-person-vs-role');		
		
		var roles = [],
			role;
		
		$widget.find('.list tr').each(function(){
			role = this.roleId;
			var persons = [];
			$(this).find('.person').each(function(){
				persons.push(this.personId)
			});
			role += ':' + persons.join(',');
			roles.push(role);
		});

		var inputValue = roles.join(';');
		$widget.find('.value-person-vs-role').val(inputValue);
	}
	
	//4:1663;3:1374,3,1368;11:1567
	function reconstructList( widgetEl ) {
		var $widget = $(widgetEl).closest('.widget-person-vs-role');
			inputValue = $widget.find('.value-person-vs-role').val();
		
		// mock value
		//inputValue = '4:1663;3:1374,3,1368;11:1567';
		
		var rows = inputValue.trim().split(';');		
		if (rows.length>0 && rows[0]!='') {
			for (var i=0; i<rows.length; i++) {
				var parts = rows[i].split(':'),
					roleId = parts[0],
					personIds = parts[1].split(','),				
					$row = findOrCreateTableRow( $widget[0], roleId );
				
				if ($row) 				
					for (var j=0; j<personIds.length; j++) 
						findOrCreatePersonElement( $row, personIds[j] );
			}
		}
	}
	
	function reconstructAllLists() {
		if ($widgets) {
			$widgets.each(function(){
				reconstructList( this );
			});
			
			if ($widgets.is('.performance-version'))
				reconstructExludedPeopleForPerformance();
		}
	}
  
  
	function updateExcludedPeopleForPerformance() {
		var $peopleContainer = $('#edit-people'),
			$excludeField = $peopleContainer.find('.value-exclude-persons'),
			rolesById = {};
		$peopleContainer.find('.list table tr .person.removed').each(function(){
			var personId = this.personId,
				roleId = $(this).closest('tr')[0].roleId;
			if (!rolesById[roleId])
				rolesById[roleId] = [];
			rolesById[roleId].push(personId);
		});
		var parts = [];
		for(var roleId in rolesById) {
			if (!isNaN(parseInt(roleId))) {
				parts.push(roleId+':'+rolesById[roleId].join(','));
			}
		}
		// vyrobim takyto tvar 925:2065;59:2066,2067,1825;3:2066;4:1825;926:2068;35:2069;39:2070
		// rola1:osoba1,osoba2;rola2:osoba3,osoba4,osoba5	
		$excludeField.val( parts.join(';') );
	}
	
	function reconstructExludedPeopleForPerformance() {
		var $peopleContainer = $('#edit-people'),
			$excludeField = $peopleContainer.find('.value-exclude-persons'),
			exludedPeople = $excludeField.val(),
			parts = exludedPeople.split(';'),
			rolesById = {};
		// urobim si najprv tabulku podla role
		if (parts[0]!='') {
			for (var i=0; i<parts.length; i++) {
				var res = parts[i].split(':'),
					roleId = res[0],
					personIds = res[1].split(','),
					personsInKeys = {};
				for (var j=0; j<personIds.length; j++)
					personsInKeys[ personIds[j] ] = true;	
				rolesById[roleId] = personsInKeys;
			}
		}
		// potom prejdem zoznamy a nastavim odstranenym osobam class .removed
		$peopleContainer.find('.list table tr').each(function(){
			var roleId = this.roleId;
			if (rolesById[roleId]) {
				$(this).find('.person').each(function(){
					if (rolesById[roleId][this.personId])
						$(this).addClass('removed')
							.find('a.remove').html('&olarr;');
				});
			}
		});
	}

})(jQuery, Drupal, this, this.document);
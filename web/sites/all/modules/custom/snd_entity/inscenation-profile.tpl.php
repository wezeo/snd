<?php
/**
 * @file
 * Template of inscenation.
 */

	// pouzi $person['full_name_with_titles']
	//die_r($inscenation);

	//global $language;
	//$lang = $language->language;

	// image
	if (!$inscenation['#image'])
		$imageHTML = '<div class="image no-image"></div>';
	else
		$imageHTML = '<div class="image">'.theme('image_style', array('path' => $inscenation['#image']->uri, 'style_name' => 'inscenation_main_detail')).'</div>';
		
		
	echo '
	<div class="inscenacia inscenation-or-performance">
		'.$imageHTML.'
		<div class="info">
			<h3 class="author"><label>Autor:</label><span class="space"> </span><span class="value">';
		if(is_array($inscenation['#authors'])) {
			foreach ($inscenation['#authors'] as $author) {
				echo '<span>'.$author['name'].'</span>';
			}
		}
		echo '</span></h3>
			<h2 class="title" title="'.$inscenation['title'].'"><label>Názov:</label><span class="space"> </span><span class="value">'.$inscenation['title'].'</span></h2>
			<div class="artistic-body"><label>'.t('Artistic body').':</label><span class="space"> </span><span class="value">'.$inscenation['#artistic_body'].'</span></div>
			<div class="teaser"><span>'.$inscenation['teaser'].'</span></div>
			<div class="clearfix info-row">
				<div class="place tid-'.$inscenation['place'].'" title="'.$inscenation['#place'].', '.$inscenation['#hall'].'"><label>Miesto konania:</label><span class="space"> </span><span class="value">'.$inscenation['#place'].'<span class="hall">'.$inscenation['#hall'].'</span></span></div>
				<div class="premiere"><label>Premiéra:</label><span class="space"> </span>
					<span class="value">';
					foreach ($inscenation['#premieres'] as $premiere) {
						echo '<span>'.$premiere['#date'].'</span>';
					}
					echo '</span>
				</div>
			</div>
		</div>';

		if ( !empty($inscenation['#inscenation_siblings']) ) {
			echo '<h3>Sezóna:</h3>';
			echo '<select id="inscenation-siblings">';
			foreach ($inscenation['#inscenation_siblings'] as $inscenation_sibling) {
				echo '<option data-url="'.$inscenation_sibling['#link'].'" '.(( $inscenation_sibling['id'] == $inscenation['id'] ) ? 'selected' : '').'>'.$inscenation_sibling['season_year'].'/'.($inscenation_sibling['season_year']+1).'</option>';
			}
			echo '</select>';
		}

		echo '<div class="performances">
			<h3>Plánované predstavenia</h3>
			<div class="list">';
		if(is_array($inscenation['performances'])) {
			foreach ($inscenation['performances'] as $performances) {
				echo '<div class="performance state-'.$performances['state'].'">
							<div class="date"><label>Dátum konania:</label><span class="space"> </span><span class="value"><span class="weekday">'.$performances['#weekday'].'</span> <span class="on-date">'.$performances['#date'].'</span></span></div>
							<div class="time"><span class="time-from">'.$performances['#time_from'].'</span> <span class="time-to">'.$performances['#time_to'].'</span></div>
							<div class="state"><span>'.$performances['#state'].'</span></div>
							<div class="action"><a href="'.$performances['#link'].'">Zobraziť detail</a></div>
						</div>';
			}
		}
			echo '</div>
		</div>

		<div class="description"><span>'.$inscenation['description'].'</span></div>';
		
		if ( !empty($inscenation['#subinscenations']) ) {
			echo '<h3>Táto inscenácia sa skladá z viacerých inscenácií:</h3>';
			foreach ($inscenation['#subinscenations'] as $subinscenation) {
				echo '<div class="sub-inscenations"><a href="'.$subinscenation['#link'].'">';
					foreach ($subinscenation['#authors'] as $subauthor) {
						echo '<span class="author">'.$subauthor['name'].'</span>';
					}
					echo ': '.$subinscenation['#title'].'
				</a></div>';
			}
		}

		if ( !empty($inscenation['#subinscenation_of']) ) {
			echo '<h3>Táto inscenácia je súčasťou inscenácie:</h3>';
			echo '<div class="subinscenation_of"><a href="'.$inscenation['#subinscenation_of']['#link'].'">';
			foreach ($inscenation['#subinscenation_of']['#authors'] as $authors) {
				echo '<span class="author">'.$authors['name'].'</span>';
			}
			echo ': '.$inscenation['#subinscenation_of']['#title'];
			echo '</a></div>';
		}

		
		echo '
		<div class="vyroba">
			<h3>Tvorcovia</h3>
			<div class="list">';
			if(is_array($inscenation['production_values'])) {
				foreach ($inscenation['production_values'] as $role => $persons) {
					echo '<div class="member"><span class="rola">'.$role.'</span><span class="space"> </span><span class="value">';
					foreach ($persons as $person) {
						echo '<span class="person"><a href="'.$person['url'].'"><span class="name">'.$person['name'].'</span></a>';
						if ( $person['note'] ) echo '<span class="note">, '.$person['note'].'</span>';
						echo '</span>';
					}
					echo '</div>';
				}
			}

			echo '</div>
		</div>

		<div class="obsadenie">
			<h3>Obsadenie</h3>
			<div class="list">';
			if(is_array($inscenation['starring_values'])) {
				foreach ($inscenation['starring_values'] as $role => $persons) {
					echo '<div class="member"><span class="rola">'.$role.'</span><span class="space"> </span><span class="value">';
					foreach ($persons as $person) {
						echo '<span class="person"><a href="'.$person['url'].'"><span class="name">'.$person['name'].'</span></a>';
						if ( $person['note'] ) echo '<span class="note">, '.$person['note'].'</span>';
						echo '</span>';
					}
					echo '</div>';
				}
			}
			echo '</div>
		</div>';

		if(is_array($inscenation['#gallery']) && !empty($inscenation['#gallery'])) {

			echo '<div class="field field-name-field-gallery field-type-image field-label-hidden"><div class="field-items">';

			foreach($inscenation['#gallery'] as $galleryImage) {

				$imageSmallUrl = image_style_url('medium', $galleryImage->uri);
				$imageOpenedUrl = image_style_url('gallery_opened', $galleryImage->uri);
				echo '<div class="field-item"><a href="'.$imageOpenedUrl.'" class="colorbox" rel="gallery-inscenation-'.$inscenation['id'].'"><img typeof="foaf:Image" src="'.$imageSmallUrl.'" alt="" title=""></a></div>';
			}

			echo '</div></div>';
		}

	echo '</div>';

?>
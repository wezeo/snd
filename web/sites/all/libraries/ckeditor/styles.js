﻿/**
 * Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

function ck$( obj ) {
	var html = this.ckHtmlLang ? this.ckHtmlLang : document.getElementsByTagName('html');
	var htmlLang = this.ckHtmlLang || (html[0] ? html[0].getAttribute('xml:lang') || html[0].getAttribute('lang') : null);
	var lang = CKEDITOR.config.language || htmlLang || CKEDITOR.config.defaultLanguage;
	for (var firstKey in obj) break;
	return obj[lang] || obj['en'] || obj[firstKey];
};

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */
	
	// These styles are already available in the "Format" combo ("format" plugin),
	// so they are not needed here by default. You may enable them to avoid
	// placing the "Format" combo in the toolbar, maintaining the same features.
	
	{ name: ck$({en: 'Paragraph', sk: 'Normálny odsek'}),	element: 'p' },
	//{ name: 'Heading 1',		element: 'h1' },
	{ name: ck$({en: 'Heading 2', sk: 'Nadpis 2'}),		element: 'h2' },
	{ name: ck$({en: 'Heading 3', sk: 'Nadpis 3'}),		element: 'h3' },
	{ name: ck$({en: 'Heading 4', sk: 'Nadpis 4'}),		element: 'h4' },
	{ name: ck$({en: 'Heading 5', sk: 'Nadpis 5'}),		element: 'h5' },
	//{ name: 'Heading 6',		element: 'h6' },
	//{ name: 'Preformatted Text',element: 'pre' },
	{ name: ck$({en: 'Address', sk: 'Adresa'}),			element: 'address' },
	{ name: ck$({en: 'BlockQuote', sk: 'Blok citát'}),			element: 'blockquote' },
	

	/*{ name: 'Italic Title',		element: 'h2', styles: { 'font-style': 'italic' } },
	{ name: 'Subtitle',			element: 'h3', styles: { 'color': '#aaa', 'font-style': 'italic' } },
	{
		name: 'Special Container',
		element: 'div',
		styles: {
			padding: '5px 10px',
			background: '#eee',
			border: '1px solid #ccc'
		}
	},*/

	/* Inline Styles */

	// These are core styles available as toolbar buttons. You may opt enabling
	// some of them in the Styles combo, removing them from the toolbar.
	// (This requires the "stylescombo" plugin)
	/*
	{ name: 'Strong',			element: 'strong', overrides: 'b' },
	{ name: 'Emphasis',			element: 'em'	, overrides: 'i' },
	{ name: 'Underline',		element: 'u' },
	{ name: 'Strikethrough',	element: 'strike' },
	{ name: 'Subscript',		element: 'sub' },
	{ name: 'Superscript',		element: 'sup' },
	*/

	/*{ name: 'Marker',			element: 'span', attributes: { 'class': 'marker' } },

	{ name: 'Big',				element: 'big' },
	{ name: 'Small',			element: 'small' },
	{ name: 'Typewriter',		element: 'tt' },

	{ name: 'Computer Code',	element: 'code' },
	{ name: 'Keyboard Phrase',	element: 'kbd' },
	{ name: 'Sample Text',		element: 'samp' },
	{ name: 'Variable',			element: 'var' },

	{ name: 'Deleted Text',		element: 'del' },
	{ name: 'Inserted Text',	element: 'ins' },*/

	{ name: ck$({en: 'Cited Work', sk: 'Citát práce'}),		element: 'cite' },
	{ name: ck$({en: 'Inline Quotation', sk: 'Citát v riadku'}),	element: 'q' },

	//{ name: 'Language: RTL',	element: 'span', attributes: { 'dir': 'rtl' } },
	//{ name: 'Language: LTR',	element: 'span', attributes: { 'dir': 'ltr' } },

	/* Object Styles */

	{
		name: ck$({en: 'Image (left)', sk: 'Obrázok doľava'}),
		element: 'img',
		attributes: { 'class': 'left' }
	},

	{
		name: ck$({en: 'Image (right)', sk: 'Obrázok doprava'}),
		element: 'img',
		attributes: { 'class': 'right' }
	},

	{
		name: ck$({en: 'Compact table', sk: 'Kompaktná tabuľka'}),
		element: 'table',
		attributes: {
			cellpadding: '5',
			cellspacing: '0',
			border: '1',
			bordercolor: '#ccc'
		},
		styles: {
			'border-collapse': 'collapse'
		}
	},

	{ name: ck$({en: 'Borderless Table', sk: 'Tabuľka bez rámčekov'}),		element: 'table',	styles: { 'border-style': 'hidden' } },
	
	{ name: ck$({en: 'Decimal Ordered List', sk: 'Číselný zoradený zoznam'}),	element: 'ol',		styles: { 'list-style-type': 'decimal' } },
	{ name: ck$({en: 'Latin Ordered List', sk: 'Abecedný zoradený zoznam'}),	element: 'ol',		styles: { 'list-style-type': 'lower-latin' } },
	{ name: ck$({en: 'Roman Ordered List', sk: 'Rímsky zoradený zoznam'}),	element: 'ol',		styles: { 'list-style-type': 'upper-roman' } },
	
	{ name: ck$({en: 'Disc Bulleted List', sk: 'Guličkový zoznam'}),	element: 'ul',		styles: { 'list-style-type': 'disc' } },
	{ name: ck$({en: 'Square Bulleted List', sk: 'Štvorčekový zoznam'}),	element: 'ul',		styles: { 'list-style-type': 'square' } },
	{ name: ck$({en: 'Circle Bulleted List', sk: 'Krúžkový zoznam'}),	element: 'ul',		styles: { 'list-style-type': 'circle' } }
]);


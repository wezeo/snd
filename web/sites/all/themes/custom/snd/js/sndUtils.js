window.sndUtils = {
    propChange: function (fn) {
        return 	function filterByArtist(e) {
            var valueChanged = false;
            if (e.type=='propertychange') { // http://stackoverflow.com/a/17384341
                valueChanged = e.originalEvent.propertyName=='value';
            } else {
                valueChanged = true;
            }
            if (valueChanged) {
               fn(e);
            }
        }
    }
};

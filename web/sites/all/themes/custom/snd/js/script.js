/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

	$(function() {

		// documentation at http://responsive.lab.wezeo.com/
		JS.Responsive.addHorizontalSizePoint( 'x-small',  481 )
		.addHorizontalSizePoint( 'small', 769 )
		.addHorizontalSizePoint( 'medium', 1025 )
		.addHorizontalSizePoint( 'large',  1225 )
		.addHorizontalSizePoint( 'x-large',  1440 )
		.addHorizontalSizePoint( 'xx-large',  1920 );

		var isTouchDevice = 'ontouchstart' in document.documentElement,
			DOWN = isTouchDevice ? 'touchstart' : 'mousedown',
			MOVE = isTouchDevice ? 'touchmove'  : 'mousemove',
			UP   = isTouchDevice ? 'touchend'   : 'mouseup',
			mousewheel = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x

		var tmpPath = window.location.pathname.match(/^(\/\w{2})?\/[\w\-]+/),
			RELATIVE_PATH = tmpPath && tmpPath.length ? tmpPath[0] + '/' : '/';

		// Request date: 4.10.2016
		// Implementation date: 
		template(
			'Obalit partnerov',
			'Jednotlive skupiny log partnerov obalit do divov aby tvorili riadky',
			true,
			false,
			function() {
				$('#content #block-views-partners-block-hp .item-list').slice(0,2).wrapAll('<div class="partners-row first-row" />');
				$('#content #block-views-partners-block-hp .item-list').slice(2,5).wrapAll('<div class="partners-row second-row" />');
				$('#content #block-views-partners-block-hp .item-list').slice(5,6).wrapAll('<div class="partners-row third-row" />');
				$('#content #block-views-partners-block-hp .item-list').slice(6,8).wrapAll('<div class="partners-row fourth-row" />');
			}
		);
		
		// Request date: 4.10.2016
		// Implementation date: 
		template(
			'Do h3 pridat span',
			'Obsah nazvov partnerov obalit do span',
			true,
			false,
			function() {
				$('#content #block-views-partners-block-hp .item-list > h3').wrapInner('<span />');
			}
		);
		
		// Request date: 4.10.2016
		// Implementation date: 
		template(
			'Medialny partneri rozdelit',
			'3. riadok v ktorom su medialni partneri rozdelit li na 2 riadky',
			true,
			false,
			function() {
				var liCount = $('#content #block-views-partners-block-hp .third-row li').length;
				$('#content #block-views-partners-block-hp .third-row li').slice(0, liCount/2).wrapAll('<div class="logo-row" />');
				$('#content #block-views-partners-block-hp .third-row li').slice(liCount/2, liCount).wrapAll('<div class="logo-row" />');
				$('#content #block-views-partners-block-hp .third-row .logo-row').wrapAll('<div class="logo-wrapper" />');
			}
		);
		
		// Request date: 11.10.2016
		// Implementation date: 
		template(
			'Obalit nadpis s body',
			'V novinkach na uvode obalit nadpis s textom do jedneho div',
			true,
			false,
			function() {
				$('#content #block-views-news-homepage-block .views-row').each(function(){
					$(this).find('.views-field-title-field, .views-field-body').wrapAll('<div class="news-wrap" />');
				});
			}
		);
		
		// Request date: 24.10.2016
		// Implementation date: 
		template(
			'Beta verzia popup',
			'Pridat na vsetky podstranky popup s oznamom o beta verzii',
			true,
			$('body > #beta-version').length,
			function() {
				$('body').append('<div class="popup-modal" id="beta-version"><div class="popup-content"><div class="close"></div><div class="popup-content-inner"> <div class="title">Vážení návštevníci,<br><br>vitajte na BETA verzii novej webovej stránky Slovenského národného divadla, ktorú pre Vás pripravujeme v spolupráci so spoločnosťou WEZEO.</div><p> Na výslednej podobe nového webu SND intenzívne pracujeme. Obrazový ani textový obsah ešte nie je aktuálny, momentálne robíme všetko preto, aby v krátkej dobe bol. Rovnako dolaďujeme malé zmeny, ktoré súvisia s dizajnom a funkcionalitou webu. Výsledky by ste mali vidieť v priebehu najbližších dní.</p><p>Napriek tomu sme si povedali, že nastal ten správny čas na získanie spätnej väzby od Vás, našich divákov. Napokon, naša nová stránka bude prioritne určená práve Vám. Preto nás zaujíma, či sa Vám páči, či sa na nej ľahko orientujete, prípadne, čo by ste na nej zmenili.</p><p>Upozorňujeme, že zverejnená BETA verzia je aktívna iba pre desktopové zariadenia.</p><p>Veríme, že aj vďaka vašim pripomienkam už v decembri predstavíme verejnosti ostrú verziu webu, s ktorým budeme všetci spokojní. Prípadnú spätnú väzbu nám môžete zanechať prostredníctvom nástroja FEEDBACK v pravom dolnom rohu.</p><p>Ďakujeme za ochotu a ústretovosť</p><p>Vaše Slovenské národné divadlo</p></div></div></div>');
			}
		);
		
		// Request date: 24.10.2016
		// Implementation date: 
		template(
			'Obalit cisla v pageri',
			'Cisla stranok v pageri obalit do divu',
			true,
			$('.pager .number-wrapper').length,
			function() {
				$('.pager').find('.pager-item, .pager-current').wrapAll('<span class="number-wrapper" />');
			}
		);
		
		// Request date: 26.10.2016
		// Implementation date: 
		template(
			'Uprava v strukture vysuvacich blokov z laveho baru',
			'',
			true,
			$('.overlay-right').length,
			function() {
				$('.block-snd-sidebar').after('<div class="overlay-right" />');
			}
		);
		
		//////////////////////////////////////////////////////////////////////////////////////////////////

		/*
		// v lavom menu schovam neaktivne linky v prvej urovni
		var $leftMenu = $('#block-system-main-menu');
		$leftMenu.find('ul.menu:first-child > li:not(.active-trail)').hide();
		*/
		
		//
		// IMAGE CENTERING FUNCTION
		//
		$.fn.centerImage = function(parentString) {
			var _this = this;
			
			$(window).on('resize', function() {
			
				_this.each(function() {
					var $image = $(this),
						$parent;

					if (parentString)
						$parent = $image.closest(parentString);
					else
						$parent = $image.parent();
					
					var _image = new Image();
					_image.src = $image.attr("src");

					var imageW  = _image.width,
						imageH  = _image.height,
						imageR  = imageW / imageH,
						parentW = $parent.width(),
						parentH = $parent.height(),
						parentR = parentW / parentH;

					if ( imageR > parentR )
						$image.css({ width: 'auto', height: '100%' });
					else
						$image.css({ width: '100%', height: 'auto' });
				});
			}).trigger('resize');
		};
		
		//
		// SMOOTH SCROLL INIT
		//
		//JS.SmoothScroll.init();
		
		//
		// ZOOM DISABLE
		//
		// TODO		
		$(document).on('gesturestart', function(e) {
			e.preventDefault();
		});
		
		//
		// SELEC2 INIT
		//
		function select2Init() {
			if ( $('html').is('.small-less.touch') )
				return;
			$('select').select2({
				minimumResultsForSearch: -1
			});
		}
		select2Init();
		// after ajax selects are loaded again without select2
		$(document).on('ajaxComplete', function() {
			select2Init();
		});
			
		//
		// GALLERY PLUGIN LIGHTCASE INIT
		//
		$('a[data-rel^=lightcase]').lightcase();
		$(document).on('ajaxComplete', function() {
			$('a[data-rel^=lightcase]').lightcase();
		});
		
		//
		// LANGUAGE SWITCH TOGGLE
		//
		var $languageSwitch = $('#block-locale-language');
		$languageSwitch.find('.content').click(function() {
			$(this).toggleClass('open').children().toggleClass('open');
		});	
		
		//
		// CUSTOM LOGO FOR PAGES CINOHRA, OPERA, BALET
		//
		var $menu = $('#block-snd-menu-custom-depth-main-menu-block');
		var $logo = $('#header #site-name a');
		if ( $menu.find('.tracking-cinohra').hasClass('active-trail') )
			$logo.addClass('logo-cinohra logo-custom');
		if ( $menu.find('.tracking-opera').hasClass('active-trail') )
			$logo.addClass('logo-opera logo-custom');
		if ( $menu.find('.tracking-balet').hasClass('active-trail') )
			$logo.addClass('logo-balet logo-custom');
		
		//
		// LEFT SIDEBAR MENU OPEN/CLOSE
		//
		var $leftBar = $('#header'),
			$leftMenuItem = $leftBar.find('#block-menu-menu-left-menu .menu li a'),
			$newsletterBlock = $('#block-simplenews-0');
		
		$leftMenuItem.on('click', function(e) {
			var $this = $(this),
				$item = $this.parent();
			
			// program icon should not open block but go to a program page
			if ( $item.hasClass('calendar') )
				return;
			
			e.preventDefault();

			$('.block-snd-sidebar').removeClass('open');

			// click on search
			if ( $item.hasClass('search') ) {
				if ( $this.hasClass('open') ) {
					$leftBar.find('#block-snd-sidebar-sidebar-search-block').removeClass('open');
					$this.removeClass('open');
					$('#sidebar-search-form .search-text-input').focusout();
					return
				}
				else {
					$leftBar.find('#block-snd-sidebar-sidebar-search-block').toggleClass('open');
					$('#sidebar-search-form .search-text-input').focus();
				}
			}
			// click on ticket
			if ( $item.hasClass('purchase') ) {
				if ( $this.hasClass('open') ) {
					$leftBar.find('#block-snd-sidebar-sidebar-reservations-block').removeClass('open');
					$this.removeClass('open');
					return;
				}
				else
					$leftBar.find('#block-snd-sidebar-sidebar-reservations-block').toggleClass('open');
			}
			// click on newsletter
			if ( $item.hasClass('newsletter') ) {
				if ( $this.hasClass('open') ) {
					$leftBar.find('#block-simplenews-0').removeClass('open');
					$this.removeClass('open');
					$leftBar.find('#block-simplenews-0 #edit-realname').focusout();
					return;
				}
				else {
					$leftBar.find('#block-simplenews-0').toggleClass('open');
					$leftBar.find('#block-simplenews-0 #edit-realname').focus();
				}
			}
			
			$leftMenuItem.removeClass('open');
			$this.addClass('open');
		});
		
		// AD CLOSE ICON TO NEWSLETTER BLOCK
		$newsletterBlock.find('> .content').prepend('<div class="close" />')
		
		// close left bar item by close button
		$leftBar.find('.block-snd-sidebar').find('.close').on('click', function() {
			var $block = $(this).parents('.block-snd-sidebar');
			$block.removeClass('open');
			if ( $block.is('#block-snd-sidebar-sidebar-search-block') ) {
				$leftMenuItem.parent().filter('.search').find('a').removeClass('open');
				$('#sidebar-search-form .search-text-input').focusout();
			}
			if ( $block.is('#block-snd-sidebar-sidebar-reservations-block') )
				$leftMenuItem.parent().filter('.purchase').find('a').removeClass('open');
			if ( $block.is('#block-simplenews-0') ) {
				$leftMenuItem.parent().filter('.newsletter').find('a').removeClass('open');
				$leftBar.find('#block-simplenews-0 #edit-realname').focusout();
			}
		});
		// close left bar item by right overlay click
		$leftBar.find('.block-snd-sidebar').next('.overlay-right').on('click', function() {
			var $block = $(this).prev('.block-snd-sidebar');
			$block.removeClass('open');
			if ( $block.is('#block-snd-sidebar-sidebar-search-block') ) {
				$leftMenuItem.parent().filter('.search').find('a').removeClass('open');
				$('#sidebar-search-form .search-text-input').focusout();
			}
			if ( $block.is('#block-snd-sidebar-sidebar-reservations-block') )
				$leftMenuItem.parent().filter('.purchase').find('a').removeClass('open');
			if ( $block.is('#block-simplenews-0') ) {
				$leftMenuItem.parent().filter('.newsletter').find('a').removeClass('open');
				$leftBar.find('#block-simplenews-0 #edit-realname').focusout();
			}
		});
		
		//
		// LEFT SIDEBAR NEWSLETTER CHECKBOX CONTROL
		//
		$newsletterBlock.find('#edit-newsletters').after(
			'<div>'
			 	+'<input type="checkbox" name="interestedInAll" class="interestedInAll" id="interestedInAll6341" value="1">'
			 	+'<label for="interestedInAll6341" class="value">'+Drupal.t('I want to receive all news')+'</label>'
			+'</div>'
		).before('<div class="interest-label">'+Drupal.t('I am interested in')+'</div>');
		// check / uncheck 'all news'
		$newsletterBlock.find('.interestedInAll').on('change', function() {
			var $this = $(this);
			if ( $this.is(':checked') ) {
				// set all check
				$this.closest('#block-simplenews-0').find('#edit-newsletters input[type="checkbox"]').prop( "checked", true );
			}
			else 
				$this.closest('#block-simplenews-0').find('#edit-newsletters input[type="checkbox"]').prop( "checked", false );
		});
		// if not all are checked
		$newsletterBlock.find('#edit-newsletters .form-item input[type=checkbox]').on('change', function() {
			// if not all are checked
			if ( !$(this).is(':checked') )
				$newsletterBlock.find('.interestedInAll').prop( "checked", false );
			// if all are checked check 
			var checkboxLength = $(this).parent().siblings().find('input[type=checkbox]').length;
			if ( $(this).parent().siblings().find('input[type=checkbox]:checked').length == checkboxLength && $(this).is(':checked') )
				$newsletterBlock.find('.interestedInAll').prop( "checked", true );
		});
		// move email input after name
		$('.form-item-realname').after($('.form-item-mail'));
		
		//
		// LEFT SIDEBAR ICON TOOLTIPS
		//
		$leftMenuItem.wrapInner('<span />');
		
		//
		// MENU BLOCKS EQUAL HEIGHT
		//
		// delay because broken words in menu items change column height
		function equalMenuHeights() {
			if ( $('html').hasClass('large-more') ) {
				setTimeout(function() {
					// each main menu column
					$('#block-snd-menu-custom-depth-main-menu-block > .content > .menu > li').each(function() {
						var height = 0;
						var $menuColumn = $(this).find('.menu');
						// each submenu column
						$menuColumn.each(function() {
							var thisHeight = $(this).outerHeight();
							// is column height bigger than previous, store it
							if ( height < thisHeight )
								height = thisHeight;
						});
						// set the biggest height to all submenu columns
						$menuColumn.css('height', height);
					});

					// offset menu
					var menuColumnWidth = 250;
					$('#block-snd-menu-custom-depth-main-menu-block > .content > .menu > li > .menu > .expanded > .menu').each(function() {
						var $this = $(this);
						if ($this.offset().left > $(window).width() - menuColumnWidth)
							$this.addClass('on-left');
					});
				},500);
			}
			if ( $('html').hasClass('large-less') ) {
				$('#block-snd-menu-custom-depth-main-menu-block > .content > .menu > li .menu').css('height', '');
			}
		}
		
		//
		// SEARCH FILTER TOGGLE
		//
		var $searchBlock = $('#block-snd-sidebar-sidebar-search-block, #page-search'),
			$searchLimit = $searchBlock.find('.search-limit-section');
		$searchLimit.filter('.closed').hide();
		$searchBlock.find('.search-limit-title').on('click', function() {
			$searchLimit.slideToggle(300);
		})
		
		//
		// HOMEPAGE SLIDER INIT
		//
		$(".front #block-views-sliders-block .view-content").owlCarousel({
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem: true,
			transitionStyle : 'fade',
			autoPlay: 7000
		});
		
		//
		// HOMEPAGE SLIDER IMAGE COVER SIZE
		//
		function sliderImageResponsive() {
			var $container = $('.front .sidebars .region-sidebar-first #block-views-sliders-block .view-content .views-row'),
				$img	   = $container.find('.field-content img'),
				containerW = $container.outerWidth(),
				containerH = $container.outerHeight(),
				containerR = containerW / containerH,
				imgRatio   = 1000 / 1400;
			if ( containerR > imgRatio )
				$img.css({width: '100%', height: 'auto'});
			else 
				$img.css({height: '100vh', width: 'auto'});
		}

		//
		// PROGRAM MONTH SELECT
		//
		$('.months').on('click', '.program-month', function() {
			// console.log('href', RELATIVE_PATH + $('#program-season').val().replace('/', '-') + '/' + $(this).find('input').val());
			redirect(RELATIVE_PATH + $('#program-season').val().replace('/', '-') + '/' + $(this).find('input').val());
		});
		

		//
		// PROGRAM SEASON SELECT
		//
		$('#program-season').on('change', function() {
			redirect(RELATIVE_PATH + $(this).val().replace('/', '-') + '/' + $('.months').find('input:checked').val());
		});
		
		//
		// REPERTOIR SEASON SELECT
		//
		$('#program-season-repertoir').on('change', function() {
			// var tmpPath = window.location.pathname.match(/^(\/\w{2})?\/\w+/);
				// RELATIVE_PATH = tmpPath && tmpPath.length ? tmpPath[0] + '/' : '/';
			var urlArr = window.location.pathname.split('/');
			var relPath ='';
			//check na jazyk v adrese (multilang)
			if (urlArr.length && urlArr[1].length==2)
				relPath = '/'+ urlArr[1] + '/' + urlArr[2];
			else				
				relPath = '/'+ urlArr[1];
			var path = relPath + '/' + $(this).val().replace('/', '-');
			// console.log(path);
			redirect(path);
		});

		//
		// PROGRAM FILTER SHOW / HIDE
		//
		var $filter = $('.program-filter'),
			$filterOpenButton = $filter.find('> button'), // green button to open/close filter
			$programFilter = $filter.siblings('.program-criterias'); // div with filter
		$filterOpenButton.on('click', function() {
			$(this).toggleClass('open');
			$programFilter.slideToggle(300);
		});
		
		//
		// HIDE WHEN CLICK OUTSIDE SOMETHING
		//
		$(document).on('click', function(e) {
			//PROGRAM FILTER
			//if (  $('html').is('.medium-more') && !$(e.target).is('.program-filter') && !$(e.target).is('.program-filter *') ) {
				//$programFilter.slideUp(300);
				//$filterOpenButton.removeClass('open');
			//}
			
			// LANGUAGE SELECTION
			if ( !$(e.target).is('#block-locale-language') && !$(e.target).is('#block-locale-language *') )
				$languageSwitch.find('.content').removeClass('open').children().removeClass('open');
		});
		
		//
		// PROGRAM FILTER CHECKBOX LOGIC
		//
		var $filterColumn = $filter.find('.program-criterium');
		// click on filter group items
		$filterColumn.find('h3 input[type="checkbox"]').on('click', function() {
			var $this = $(this);
			if ( $this.is(':checked') ) {
				// set all check
				$this.closest('.program-criterium').find('.program-criterium-options input[type="checkbox"]').prop( "checked", true )
					.trigger('change'); // http://stackoverflow.com/q/24410581
			}
			else {
				// set all uncheck
				$this.closest('.program-criterium').find('.program-criterium-options input[type="checkbox"]').prop( "checked", false )
					.trigger('change'); // http://stackoverflow.com/q/24410581
			}
			if ( $this.is('.mid-state') )
				$this.removeClass('mid-state');
		});
		// click on filter sub items
		$filterColumn.find('.program-criterium-options input[type="checkbox"]').on('click', function() {
			var count = $(this).closest('.program-criterium-options').find('input[type="checkbox"]').length,
				checkedCount = $(this).closest('.program-criterium-options').find('input[type="checkbox"]:checked').length,
				$groupCheckbox = $(this).closest('.program-criterium').find('h3 input[type="checkbox"]');
			// if all checked
			if ( count === checkedCount )
				$groupCheckbox.prop( "checked", true ).removeClass('mid-state'); // set group checkbox checked
			// if none checked
			else if ( checkedCount === 0 )
				$groupCheckbox.prop( "checked", false ).removeClass('mid-state'); // set group checkbox unchecked
			// if some checked
			else
				$groupCheckbox.addClass('mid-state'); // // set group checbox mid-state
		});
		
		//
		// INSCENATION SEASON FILTER SWITCH
		//
		console.log('aaaa');
		$('#inscenation-siblings').on('change', function() {
			var selectedOptionLink = $(this).find('option:selected').data('url');
			window.location.href = selectedOptionLink;
		});
		
		//
		// INSCENATION IMAGE GALLERY SLIDER INIT
		//
		$(".inscenation-or-performance").find('.gallery .list').owlCarousel({
			items : 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			navigation: true
		});
		
		//
		// HISTORY BACK LINK
		//
		$('.history-back-link').on('click', function(e) {
			e.preventDefault();
			window.history.back();
		});
		
		//
		// MANAGEMENT SWITCH
		//
		if ( $('.vedenie').length ) {
			var selected;
			var $item_detach;
			var $item = $('#block-views-management-block .views-row');
			var $itemParent = $item.parent();
			$('input[name="vedenie-ctgr"]').on('change', function() {
				selected = $(this).data('show');
				$item_detach = $item.detach();
				$item_detach.filter('.'+selected).appendTo( $itemParent ).hide().fadeIn(300);
				$item_detach = null;
				$('#block-views-management-block .views-field-field-profile-image img').centerImage('#block-views-management-block .views-field-field-profile-image');
			});
		}
		
		//
		// JOB OPPORTUNITIES SWITCH
		//
		function jobSwitch(){
			var $jobSwitcher = $('.jobs-switcher'),
				$firstBlock  = $('#block-views-jobs-jobs-artists-block'),
				$secondBlock = $('#block-views-jobs-jobs-others-block');

			$jobSwitcher.find('.artists a').addClass('active');
			$firstBlock.find('.pager, .view-content').show();

			$jobSwitcher.find('a').on('click', function(e) {
				e.preventDefault();
				$firstBlock.find('.pager, .view-content').hide();
				$secondBlock.hide();
				$jobSwitcher.find('a').removeClass('active');
				$(this).addClass('active');
				if ( $(this).parent().is('.artists') )
					$firstBlock.find('.pager, .view-content').fadeIn(300);
				if ( $(this).parent().is('.others') )
					$secondBlock.fadeIn(300);

			});
		}
		jobSwitch()
		
		
		$(document).on('ajaxComplete', function() {
			jobSwitch();
			
		});
		
		//
		// PERSON PROFILE PERFORMANCES SEASON SWITCH
		//
		var $profilPerformance = $('.page-profil .predstavenia'),
			$profileSeasonSelect = $profilPerformance.find('.season select');
		$profileSeasonSelect.on('change', function() {
			var selected = $(this).val();
			$profilPerformance.find('.production-year').hide();
			$profilPerformance.find('.production-year').filter('.year-'+selected).fadeIn(300);
		});
		$profileSeasonSelect.select2('destroy');
		$profilPerformance.find('.season select').find('option:last').prop('selected', true).trigger('change');
		$profileSeasonSelect.select2({
			minimumResultsForSearch: -1
		});
		
		//
		// ARTICLE DETAIL HEADING DYNAMIC MARGIN BOTTOM
		//
		if ( $('.node-type-news').length && $('.field-url-button').length ) {
			$('.page--title').css('marginBottom', '70px');
		}
		
		//
		// PROGRAM POSTERS SELECT AUTO SUBMIT
		//
		$('#edit-field-history-season-value').on('change', function() {
			$('#edit-submit-posters').trigger('click');
		});
		
		//
		// VIDEO SEASON SELECT AUTO SUBMIT
		//
		$('body').on('change', '#edit-field-season-tid', function() {
			$('#edit-submit-videos').trigger('click');
		});
		
		//
		// RESPONSIVE TABLES
		//
		$('table').wrap('<div class="responsive-table" />');
		
		$('.responsive-table').mCustomScrollbar({
			theme:"dark",
			axis:"x",
			mouseWheel:{ deltaFactor: 100, preventDefault: false },
			position: "top",
			documentTouchScroll: false,
			contentTouchScroll: false,
			callbacks:{
				onScroll: function(e){
					console.log(this);
				}
			}
		});
		
		//
		// MANAGEMENT SWITCH
		//
		var $vedenie  = $('.vedenie'),
			$category = $vedenie.find('.category');
		$category.hide();
		$vedenie.find('input[name="vedenie-ctgr"]').on('change', function() {
			var selected = $(this).filter(':checked').val();
			$category.hide();
			$vedenie.find('.category-'+selected).fadeIn(300);
		}).first().trigger('click');
		
		//
		// CREAT POPUP function
		//
		$.fn.customPopup = function( config ) {
			var $popup = $(this);
			
			if ( !$popup.length )
				return;
				
			$popup.appendTo($('body')).wrap('<div class="popup-content-inner" />').parent().wrap('<div class="popup-content" />').parent().prepend('<div class="close" />').wrap('<div class="popup-modal" />');
			
			var classes = $popup.attr('class');
			if (typeof classes == 'string') {
				classes = classes.split(' ');
				for (i = 0; i < classes.length; i++) {
					$popup.closest('.popup-modal').addClass('popup-'+classes[i]);
				}
			}

			// popup vertical centering
			$('.popup-modal').each(function() {
				var $popup  = $(this),
					windowH = $(window).height(),
					popupH  = $popup.show().children().outerHeight();
				$popup.hide();

				if ( windowH - 80 > popupH )
					$popup.children().css('top', (windowH - popupH) / 2 );
			});
			
			if ( config == 'show' ) {
				$popup.closest('.popup-modal').show();
			}
		};
		
		// popup close
		$(document).on('click', '.popup-modal', function(e) {
			if ( $(e.target).is('.popup-modal *:not(.close)') )
				return;
			$(this).fadeOut(300);
		});
		
		// CUSTOM POPUP INIT
		var $suggestionsForm = $('#block-webform-client-block-14103')
		$suggestionsForm.customPopup();
		$('.messages').customPopup('show');
		
		//
		// TODO popup open
		//
		$('.staznosti-podnety #form-suggestion-modal').on('click', function() {
			$suggestionsForm.closest('.popup-modal').fadeIn(300);
		});
		
		//
		// DISPLAY BETA VERSION POPUP WHEN FIRST VISIT
		//
		var cookie = document.cookie;
		if (cookie.indexOf('visited=yes') < 0) {
			document.cookie = "visited=yes";
			cookie = document.cookie;
			$('#beta-version').show();
		}
		
		//
		// NEWSLETTER REQUIRED INPUTS
		//
		$('#simplenews-subscriptions-multi-block-form').find('#edit-realname, #edit-mail').attr('required', 'required');
		
		//
		// INIT RESOINSIVE FUNCTIONS
		//
		setTimeout(function() {
			dayColumnHeight();
		}, 300);
		
		sliderImageResponsive();
		responsive();
		equalMenuHeights();
		programFilter();
		popupMousewheel();
		menuMousewheel();
		
		$(window).on('resize', function() {
			sliderImageResponsive();
			equalMenuHeights();
		});
		
		JS.Responsive.addOnChangeHadler( function(e){

			if ( e.changedSizePointHorizontal ) {
				responsive();
				dayColumnHeight();
				programFilter();
				popupMousewheel();
				menuMousewheel();
			}
		})
		
		//
		// RESPONSIVE DESIGN FUNCITON
		//
		function responsive() {
			var $mobileMenu = $('#block-snd-menu-custom-depth-main-menu-block');
			// mobile
			if ( $('html').is('.large-less') && !$('.hamburger').length ) {
				// add hamburger button to open mobile menu
				$('body').prepend('<div class="hamburger"><span /><span /><span /></div>');
				$('.hamburger').on('click', function() {
					$(this).addClass('menu-open');
					$mobileMenu.fadeIn(300);
				});
				
				// add close button to mobile menu
				$mobileMenu.prepend('<div class="close" />');
				$mobileMenu.find('.close').on('click', function() {
					$('.hamburger').removeClass('menu-open');
					$mobileMenu.fadeOut(300);
				});
				// mobile menu control
				$mobileMenu.find('.menu .expanded > a').on('click', function(e) {
					e.preventDefault();
					if ( !$(this).hasClass('open') )
						$(this).parent().siblings().find('.open').removeClass('open').siblings('ul').slideUp(300);
					$(this).toggleClass('open').siblings('ul').slideToggle(300);
				});
			}
			
			if ( $('html').is('.small-less') && !$mobileMenu.find('#block-menu-menu-social-sidebar-menu').length ) {
				// social media icons to mobile menu
				$('#block-menu-menu-social-sidebar-menu').appendTo($mobileMenu.find('> .content'));
				
				// move search icon from header
				$('#block-menu-menu-left-menu .menu li.search a').clone().addClass('mobile-search').prependTo($('body'));
				$('body > .mobile-search').on('click', function(e) {
					e.preventDefault();
					$leftBar.find('#block-snd-sidebar-sidebar-search-block').addClass('open');
				});
			}
			
			// desktop
			if ( $('html').is('.large-more') && $('.hamburger').length ) {
				$('.hamburger').remove();
				$mobileMenu.css('display', '');
				$mobileMenu.find('.menu').css({display: '', overflow: ''});
				$mobileMenu.find('.close').remove();
				$mobileMenu.find('.menu .expanded > a').off('click');
			}
			
			if ( $('html').is('.small-more') && $mobileMenu.find('#block-menu-menu-social-sidebar-menu').length ) {
				// social media icons back to left sidebar
				$('#block-menu-menu-social-sidebar-menu').appendTo($('.region-header'));
				// remove search icon
				$('body > .mobile-search').remove();
			}
		}
		
		//
		// RESPONSIVE PROGRAM FILTER
		//
		function programFilter() {
			if ( 
					$('body').attr('class').indexOf('page-program') > 0
					|| $('#block-snd-core-program-modry-salon-block').length
					|| $('#block-snd-core-program-pre-deti-block').length
					|| $('#block-snd-core-program-operne-studio-block').length
				) {
				var $programBlock = $('.program-block');
				// mobile
				if ( $('html').hasClass('medium-less') && !$('#program-filter-popup').length ) {
					// add button to open mobile filter
					$programBlock.prepend('<button class="open-filter-mobile">Rozšírený filter</button');
					$('.open-filter-mobile').on('click', function() {
						$('#program-filter-popup').fadeIn(300);
					});
					// add filter popup
					$programBlock.prepend('<div id="program-filter-popup" class="popup-modal"><div class="popup-content"><div class="close" /><div class="popup-content-inner"><ul><li class="podla-umelca"><a href="#">Podľa umelca</a><div></div></li><li class="umelecky-subor"><a href="#">Umelecký súbor</a><div></div></li><li class="programovy-filter"><a href="#">Programový filter</a><div></div></li><li class="sezona"><a href="#">Sezóna</a><div></div></li><li class="mesiac"><a href="#">Mesiac</a><div></div></li></ul></div></div></div>');
					var $filterContent = $programBlock.find('#program-filter-popup .popup-content');
					// add filter to popup
					$programBlock.find('.program-search-by-artist').prependTo($filterContent.find('.podla-umelca div'));
					$programBlock.find('.program-artistic-body').prependTo($filterContent.find('.umelecky-subor div'));
					$programBlock.find('.program-filter, .program-criterias').prependTo($filterContent.find('.programovy-filter div'));
					$programBlock.find('.season').prependTo($filterContent.find('.sezona div'));
					$programBlock.find('.month').prependTo($filterContent.find('.mesiac div'));
					// if page is modry salon remove some filters
					if ( 
							$('#block-snd-core-program-modry-salon-block').length
							|| $('#block-snd-core-program-pre-deti-block').length 
							|| $('#block-snd-core-program-operne-studio-block').length
						)
					{
						$filterContent.find('.podla-umelca, .umelecky-subor, .programovy-filter').remove();
					}
					// if EN or DE language translate labels in mobile filter
					if ( $('body').hasClass('i18n-en') || $('body').hasClass('i18n-de') ) {
						$filterContent.find('ul > li').each(function() {
							var $this = $(this),
								translateText;
							if ( $this.hasClass('programovy-filter') ) {
								translateText = $this.find('button').first().text();
							}
							else {
								translateText = $this.find('label').first().text();
							}
							$this.children('a').text(translateText);
						});
					}
					
					// mobile filter controls
					$filterContent.find('ul > li > a').on('click', function(e) {
						e.preventDefault();
						if ( !$(this).hasClass('open') )
							$(this).parent().siblings().find('.open').removeClass('open').siblings('div').slideUp(300);
						$(this).toggleClass('open').siblings('div').slideToggle(300);
					});
					
					$filterContent.find('.popup-content-inner').mCustomScrollbar({
						theme:"dark",
						mouseWheel:{ deltaFactor: 100 }
					});
				}
				// desktop
				if ( $('html').hasClass('medium-more') && $('#program-filter-popup').length ) {
					var $filterPopup = $('#program-filter-popup')
					$programBlock.find('.open-filter-mobile').remove();
					$filterPopup.find('.month').prependTo($programBlock);
					$filterPopup.find('.season').prependTo($programBlock);
					$filterPopup.find('.program-filter, .program-criterias').prependTo($programBlock);
					$filterPopup.find('.program-artistic-body').prependTo($programBlock);
					$filterPopup.find('.program-search-by-artist').prependTo($programBlock);
					$filterPopup.remove();
				}
			}
		}
		
		//
		// PROGRAM PAGE DAY COLUMN HEIGHT ON MOBILE
		//
		function dayColumnHeight() {
			if ( $('html').is('.medium-less') ) {
				$('.calendar-day-row').each(function () {
					var height = $(this).children('.calendar-events').height();
					$(this).children('.calendar-day-group').css('height', height);
				});
			}
			if ( $('html').is('.medium-more') ) {
				$('.calendar-day-row .calendar-day-group').css('height', '');
			}
		}
		
		//
		// PROGRAM TIME LEFT MARGIN CALC
		//
		$('.program-block .program-calendar .calendar-day-row .calendar-events .performance .state').each(function() {
			var width = $(this).outerWidth();
			var padding = parseInt($(this).parent().css('paddingLeft').slice(0, -2));
			$(this).next('.date').css('left', padding + width + 15);
		});
		
		//
		// CUSTOM SCROLLBAR
		//
		$('#header .block-snd-sidebar .content > *:not(.close), .popup-modal .popup-content-inner').mCustomScrollbar({
			theme:"dark",
			mouseWheel:{ deltaFactor: 100 }
		});
		
		//
		// IF NEWSLETTER PAGE MOVE NEWSLETTER FROM SIDEBAR TO PAGE CONTENT
		//
		if ( $('body').is('.page-node-38') ) {
			$('.region-content').prepend($('#block-simplenews-0').addClass('block-snd-sidebar-content'));
		}
		
		//
		// STOP SITE SCROLL ON POPUPS OR MODALS
		//
/*		console.log('bla');
		setTimeout(function() {
		$('#content #block-simplenews-0').addClass('block-snd-sidebar-content');
		},1000);*/
		$('.block-snd-sidebar:not(.block-snd-sidebar-content), .overlay-right').on(mousewheel+' touchmove', function(e) {
			e.preventDefault();
		});
		
		// popup stop scroll only on mobile
		function popupMousewheel() {
			if ( $('html').is('.small-less') ) {
				$('.popup-modal').on(mousewheel+' touchmove', function(e) {
					e.preventDefault();
				});
			}
			if ( $('html').is('.small-more') ) {
				$('.popup-modal').off(mousewheel+' touchmove');
			}
		}
		
		// menu stop scroll only on mobile
		function menuMousewheel() {
			if ( $('html').is('.large-less') ) {
				$menu.on(mousewheel+' touchmove', function(e) {
					e.preventDefault();
				});
				$menu.children('.content').mCustomScrollbar({
					theme:"dark",
					mouseWheel:{ deltaFactor: 100 }
				});
			}
			if ( $('html').is('.large-more') ) {
				$menu.off(mousewheel+' touchmove');
				$menu.children('.content').mCustomScrollbar('destroy');
			}
		}
		
		// Cookie bar 
		var cookieBar = $('.cookie-bar');
		
		// Vrati hodnotu pozadovanej cookie
		function getCookie (cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++)
			{
				var c = $.trim(ca[i]);
				if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			}
			return "";
		}
		
		// Nastavi cookie na pozadovanu hodnotu a vrati sameho seba
		function setCookie (cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";path=/;" + expires;
			
			return this;
		}
		
		if(getCookie("agreeWithCookie") != 1) {
			cookieBar.show();
			
			$('#agree-with-cookies').bind('click', function(){
				cookieBar.slideUp();
				setCookie("agreeWithCookie", 1, 30);
			});
		}
		
		// Rezervacie a vstupenky - nastavujem vysku podla najvyssieho .col
		equalheight = function(container){

			var currentTallest = 0,
				currentRowStart = 0,
				rowDivs = new Array(),
				$el,
				topPosition = 0;
			$(container).each(function() {

				$el = $(this);
				$($el).height('auto')
				topPostion = $el.position().top;

				if (currentRowStart != topPostion) {
					for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
						rowDivs[currentDiv].height(currentTallest);
					}
					rowDivs.length = 0; // empty the array
					currentRowStart = topPostion;
					currentTallest = $el.height();
					rowDivs.push($el);
				} else {
					rowDivs.push($el);
					currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
				}
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
			});
		}

		$(window).load(function() {
			equalheight('.col-page .col-4');
		});


		$(window).resize(function(){
			equalheight('.col-page .col-4');
		});

    });

	
})(jQuery, Drupal, this, this.document);

/*
template - Poziadavka na upravu templatu HTML

	@name				:String   - nazov poziadavku
	@description		:String   - presny popis co sa pozaduje - ako upravit template (HTML)
	@specialPageBool	:Boolean  - podmienka ze na ktorych strankach treba upravu vykonat, a
	                                k na vsetkych, tak saci zadat len true
	@testTemplateBool	:Boolean  - podmienka testujuca implementovanie poziadavky (ak vrati true, 
	                                poziadavka bola implementovana)
	@changeTemplateFn	:Function - funkcia ktora vykona (docasne) upravu, kym uprava HTML nie je 
	                                vykonana na serveri	
*/
function template( name, description, specialPageBool, testTemplateBool, changeTemplateFn ) {
	if (specialPageBool) {
		if (!testTemplateBool) {
			changeTemplateFn();
			console.warn("Template request '"+name+"' is still TODO.");
			if (description)
				console.log(description);
		}
		else {
			console.error("Template request '"+name+"' is already implemented.");
		}
	}
}

// Utility redirect supporting IE<9
function redirect (url) {
	var ua        = navigator.userAgent.toLowerCase(),
		isIE      = ua.indexOf('msie') !== -1,
		version   = parseInt(ua.substr(4, 2), 10);

	// Internet Explorer 8 and lower
	if (isIE && version < 9) {
		var link = document.createElement('a');
		link.href = url;
		document.body.appendChild(link);
		link.click();
	}

	// All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like Internet Explorer 8 & lower does)
	else {
		window.location.href = url;
	}
}

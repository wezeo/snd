/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

	var hoverArtistsTimeout, // holding timeout reference on hovering #search-artist input
		programPersons,
		programPersonsFiltered = [],
		$popupArtists,
		$numResults,
		popupArtistsVisible = false,
		perfHovered = null,
		sndUtils = window.sndUtils,
		loadingPersons = false,

		CRITERIUM_TID_CINOHRA = 'criterium-tid-70',
		CRITERIUM_TID_OPERA = 'criterium-tid-71',
		CRITERIUM_TID_BALET = 'criterium-tid-72',
		CRITERIUM_TID_INE = 'criterium-tid-73',

		preferences = [
			{name: 'opera', 	count: localStorage.tracking_opera, 	criteriumTid: CRITERIUM_TID_OPERA},
			{name: 'cinohra', 	count: localStorage.tracking_cinohra, 	criteriumTid: CRITERIUM_TID_CINOHRA},
			{name: 'balet', 	count: localStorage.tracking_balet, 	criteriumTid: CRITERIUM_TID_BALET},
			{name: 'ine', 		count: localStorage.tracking_ine, 		criteriumTid: CRITERIUM_TID_INE}
		].sort(function (prev, next) {
			if(prev.count > next.count)
				return -1;
			else if(prev.count < next.count)
				return 1;
			else
				return Math.random() - 0.5;
		});

	$(function() { // document ready
		var $performances = $('.performance');

		if(!$performances.length)
			return;

		// filter performances by checkboxes
		$('.program-artistic-body [type="checkbox"], .program-criterias [type="checkbox"]').on('change', function(e){
			var $target = $(e.target);
			var checked = $target.is(':checked');

            // console.log('$target.val()', $target.val(), checked);

			if($target.parent().is('h3')){
				// is parent checkbox
				$target.parents('.program-criterium').find('.program-criterium-option input').each(function (i, opt) {
                    var val = $(opt).prop( "checked", checked ).val();
                    // console.log('dingdong', val, $(opt));
                    if(checked)
                        removeHiddenBy(val);
                    else
                        addHiddenBy(val);
                });
			}else{
				// child checkbox or program-artisctic-body checkbox
                if(checked)
                    removeHiddenBy($target.val());
                else
                    addHiddenBy($target.val());
                if($target.parents('.program-criterium-options').length) // is child checkbox
                	setParentCheckboxState($target.parents('.program-criterium'));
			}

			processChanges();
		});

		// filter by artist
		$('#search-artist')
			.on('mouseenter', artistsMouseEnter)
			.on('mouseleave', artistsMouseLeave)
			.on('focus', loadPersons)
			.on('propertychange input', sndUtils.propChange(filterByArtist)) // because change method fires on blur
			.after('<div id="num-results"></div>');

		$numResults = $('#num-results').hide();

		// popup for hovered performance if search-artist filter is active
		$popupArtists = $('<div id="popup-artists"></div>').hide().appendTo('body');

		$performances
			.hover(perfMouseEnter, perfMouseLeave);
			// .on('mouseenter', perfMouseEnter)
			// .on('mouseleave', perfMouseLeave);

        processChanges(); // initial processing
    });

	// function DEFINITIONS ===============================================================================================
	function setParentCheckboxState(groupWrapElement) {
        var parentCheckboxWrap = groupWrapElement.find('h3'),
			childCheckboxes = groupWrapElement.find('.program-criterium-options input'),
			totalLength = childCheckboxes.length,
			checkedLength = childCheckboxes.filter(':checked').length;

        if(totalLength == checkedLength){ // all are checked
            parentCheckboxWrap.removeClass('mixed');
            parentCheckboxWrap.find('input').prop( "checked", true );
		}else if(!checkedLength){ // none are checked
            parentCheckboxWrap.removeClass('mixed');
            parentCheckboxWrap.find('input').prop( "checked", false );
		}else{ // some are checked
            parentCheckboxWrap.addClass('mixed');
		}
    }

	function addHiddenBy(val) {
        $('.' + val).addClass('hidden-by-' + val);
    }

    function removeHiddenBy(val) {
        $('.' + val).removeClass('hidden-by-' + val);
    }

	function processChanges(){
		$('.calendar-day-row').each(function(i, day){
			var $day = $(day),
				$performances = $day.find('.performance:visible'),
				perfLen = $performances.length,
				noOfDoubles = (perfLen % 3) ? 3 - perfLen % 3 : 0, // returns 0-2
				$dayGroup = $day.children().first(),
				rows = Math.ceil(perfLen/3);

			// skip no-event rows
			if($day.hasClass('no-events'))
				return;

			// Update row counts class
			if(!rows)
				rows = 1; // minimum is one

			$dayGroup.removeClass(function (index, cls) { // http://stackoverflow.com/a/5182103
				return (cls.match (/(^|\s)row-count-\S+/g) || []).join(' ');
			});

			$dayGroup.addClass('row-count-' + rows);

			// Solve no-match row
			if(!$performances.length){
				$day.addClass('no-match');
				if($day.find('.no-match').length)
					$day.find('.no-match').removeClass('hidden-by-filter');
				else
					$day.children('.calendar-events').append('<p class="no-match">' + Drupal.t('Performances at this day, does not match search criteria.') + '</p>');

				// stop additional processing
				return;
			}
			// Remove no-events if used before, bacause there are events to show
			else{
				$day.removeClass('no-match');
				$day.find('.no-match').addClass('hidden-by-filter');
			}

			// Remove previous state
			$performances.removeClass('single double triple');

			// Add new classes according variants in distribution
			if(perfLen == 1){
				$performances.first().addClass('triple');
			}else{
				if(noOfDoubles){
					$performances.addClass('single');

					solveDoubles($performances[0], $performances[1], preferences);

					if(noOfDoubles>1)
						solveDoubles($performances[perfLen-2], $performances[perfLen-1], preferences, getTitle($performances.filter('.double')));
				}else
					$performances.addClass('single');
			}
		});
	}

	function getTitle(perf) {
		return perf.find('.title').attr('title');
    }

	function solveDoubles(perf1, perf2, preferences, titleOfFirstDouble) {
		var perf1hasClass = false,
		    perf2hasClass = false;

		perf1 = $(perf1);
		perf2 = $(perf2);

		if(titleOfFirstDouble)
			// do not make double of same title twice in one day
			if(getTitle(perf1) == titleOfFirstDouble){
                addClassDouble(perf2);
                return false;
            }else if(getTitle(perf2) == titleOfFirstDouble){
                addClassDouble(perf1);
                return false;
            }

		$.each(preferences, function (i, preference) {
			perf1hasClass = perf1.hasClass(preference.criteriumTid);
			perf2hasClass = perf2.hasClass(preference.criteriumTid);

			if (perf1hasClass && !perf2hasClass){
				addClassDouble(perf1);
				return false;
			}else if(!perf1hasClass && perf2hasClass){
				addClassDouble(perf2);
				return false;
			}else if(perf1hasClass && perf2hasClass){
				if(Math.random()>0.5)
					addClassDouble(perf1);
				else
					addClassDouble(perf2);
				return false;
			}
			// else continue to next preference

		});
	}

	function addClassDouble(perf) {
		perf.removeClass('single').addClass('double');
	}

	function artistsMouseEnter() {
		hoverArtistsTimeout = setTimeout(loadPersons, 500);
	}

	function artistsMouseLeave() {
		clearTimeout(hoverArtistsTimeout);
	}

	function loadPersons() {

		if(loadingPersons || programPersons)
			return;

        loadingPersons = true;
        var partPath = window.location.pathname.match(/\/(\d{4})(-\d{4})?(\/\d{2})/);
        if(partPath)
            partPath = partPath[1] + partPath[3];
        else
            partPath = '';
        
        // console.log('partPath', partPath);

		$.ajax({
			type: "POST",
			dataType: 'json',
			contentType: "application/json; charset=utf-8",
			url: '/api/getProgramPersons/' + partPath,
			// data: '{"Id":"' + Id + '"}',
			cache: true, //It must "true" if you want to cache else "false"
			success: function (data) {
				// console.log('resData', data);
                programPersons = data;

                // process persons
                $.each(programPersons, function (perfId, perf) {
                	perf.matchedArtists = [];
                	perf.id = perfId;
                	$.each(perf, function (role, names) {
                		if(role == 'matchedArtists' || role == 'id')
                			return;

                		perf[role] = {
                			names: names,
							latinized: names.map(function (name) {
								return $.latinize(name);
                            })
                		};
                	});
                });

                loadingPersons = false;
			},
			error: function (xhr, textStatus, error) {
				console.error(error, textStatus);
                loadingPersons = false;
			}
		});
	}
	
	function filterByArtist(e) {
		var query,
			latinized,
			$performances = $('.performance');

		programPersonsFiltered.length = 0; // reset

		if(!e.target.value || !programPersons){
			$numResults.slideUp();
			$performances.removeClass('hidden-by-search-artist');
			$popupArtists.hide();
			processChanges();
			return;
		}

		latinized = $.latinize(e.target.value);
		query = new RegExp('\\b' + latinized, 'i');

		$performances.addClass('hidden-by-search-artist');
		$.each(programPersons, function (perfId, perf) {
			perf.matchedArtists.length = 0; // reset previous search
			$.each(perf, function (role, data) {
				if(role == 'matchedArtists' || role == 'id')
					return;

                $.each(data.latinized, function (i, nameLatinized) {
                    if(nameLatinized.match(query)){
                        perf.matchedArtists.push({name: data.names[i], role: role});

                        if(programPersonsFiltered.indexOf(perf) == -1){
                            programPersonsFiltered.push(perf);
                            $('#performance-' + perfId).removeClass('hidden-by-search-artist');
                        }
                    }
                });
			});
		});

		if(programPersonsFiltered.length)
			$numResults.slideDown().html(programPersonsFiltered.length);
		else
			$numResults.slideUp();

		if(perfHovered)
			$popupArtists
				.html(getArtistsList(perfHovered.matchedArtists));

		processChanges();
	}

	function getArtistsList(list) {
		var result = $('<ul></ul>');
		$.each(list, function (i, artist) {
			result.append('<li><span>' + artist.role + ': </span>' + artist.name + '</li>');
		});
		return result;
	}

	function getPerfByIdFromFiltered(id) {
		var result = false;

		id = id.replace('performance-', '');

		$.each(programPersonsFiltered, function (i, perf) {
			if(perf.id == id){
				result = perf;
				return false;
			}
		});
		return result;
	}

	function perfMouseEnter(e) {
		var id = e.delegateTarget && e.delegateTarget.id,
			perf;

		if(popupArtistsVisible || !id || !programPersonsFiltered.length)
			return;

		perf = getPerfByIdFromFiltered(id);

		if(!perf)
			return;

		$popupArtists
			.css({top: e.pageY + 10, left: e.pageX + 10})
			.html(getArtistsList(perf.matchedArtists))
			.show();

		perfHovered = perf;

		$('body')
			.on('mousemove', movePopupArtists);

		popupArtistsVisible = true;
	}

	function perfMouseLeave() {
		$popupArtists
			.hide();
		$('body')
			.off('mousemove', movePopupArtists);

		popupArtistsVisible = false;
	}

	function movePopupArtists(e) {
		$popupArtists
			.css({top: e.pageY + 10, left: e.pageX + 10});
	}
	
})(jQuery, Drupal, this, this.document);

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
    var $popupMatched,
        $noMatch,
        popupMatchedVisible,
        itemHovered,
        $numResults,
        sndUtils = window.sndUtils,

        /**
         * Keys are item.id and values are arrays of latinized strings
         * @var {StringCacheList} itemStringCache
         */
        itemsMatched = {},
        searchQuery;

    $(function() { // document ready
        var
            /**
             * Cached collection of all items
             * @var {jQuery-Collection} $items
             */
            $items,

            /**
             * Certain use case of script for tweaking some odds for each case
             * @var {String} useCase
             */
            useCase,

            /**
             * @typedef {Object} StringCache
             * @property {string} str - original string with diacritics
             * @property {string} lat - latinized string (without diacritics)
             *
             * @typedef {Object} StringCacheList
             * @property {[StringCache]|[string} item_id dynamic property name according to item id
             *
             *
             * Keys are item.id and values are arrays of objects with strings and latinized strings
             * @var {StringCacheList} itemStringCache
             */
            itemStringCache = {};

        $items = $('.contacts').find('.item');
        useCase = 'contacts';

        if(!$items.length){
            $items = $('.osobnosti').find('.item');
            useCase = 'celebrities';
        }

        if(!$items.length)
            return;

        if(useCase == 'contacts'){
            $('.region-content').prepend(
                '<form class="contact-search" onsubmit="return false">'
                    +'<label for="search">' + Drupal.t('Search') + '</label>'
                    +'<input type="text" placeholder="' + Drupal.t('Type here to search') + '" name="search" id="search">'
                    +'<input type="submit" value="' + Drupal.t('Search') + '">'
                    +'<div id="num-results"></div>'
                    +'<div id="no-matched-item">' + Drupal.t('No contact matches the search query!') + '</div>'
                +'</form>');
        }else if(useCase == 'celebrities'){
            $('.osobnosti-search-by-name').append(
                '<div id="num-results"></div>'
                +'<div id="no-matched-item">' + Drupal.t('No celebrity matches the search query!') + '</div>'
            );
            // $('#no-matched-item').css('visibility', 'hidden');
            // $('#num-results').css('visibility', 'hidden');
        }

        $noMatch = $('#no-matched-item');
        $numResults = $('#num-results');

        cacheLatinizedItems(itemStringCache, $items);

        // popup for hovered performance if search-artist filter is active
        $popupMatched = $('<div id="popup-matched" style="position: absolute; z-index: 10000; border: solid 1px white; background-color: black; padding:5px;"></div>')
            .hide().appendTo('body');

        $('#search, #search-osobnosti')
            .on('propertychange input', sndUtils.propChange(function (e) { // because change method fires on blur
                var query = e.target.value;
                itemsMatched = {};

                // console.log('dingdong', query);
                if (!query) {
					$numResults.removeClass('show');
                    $noMatch.removeClass('show');
                    $items.removeClass('hidden-by-search');
                    $popupMatched.hide();
                    return;
                }

                searchMatchesAndHideOthers(query, itemStringCache);

                if(itemHovered)
                    $popupMatched
                        .html(getMatchedList(itemHovered));

                if(!$items.filter(':visible').length){
                    $noMatch.addClass('show');
                    $numResults.removeClass('show');
                }else{
                    $noMatch.removeClass('show');
                    $numResults.addClass('show').html(Drupal.t('Number of search results: ') + '<strong>'+$items.filter(':visible').length+'</strong>');
                }
            }));

        $items.hover(
            itemMouseEnter,
            itemMouseLeave
        );

    });

    // function DEFINITIONS ===============================================================================================

    function cacheLatinizedItems(itemStringCache, $items) {
        var whiteSpacesOnly = /^\s+$/;

        $items.each(function (i, item) {
            var cachedStrings = itemStringCache[item.id] = [],
                itemTextNodes = getTextNodesIn(item);

            $.each(itemTextNodes, function (i, str) {
                str = str.nodeValue;
                if(str && !str.match(whiteSpacesOnly))
                    cachedStrings.push({str: str, lat: $.latinize(str).toLowerCase()});
            });
        });
        
        // console.log('itemStringCache: ', itemStringCache);
    }

    function searchMatchesAndHideOthers(query, stringCacheList) {
        query = $.latinize(query).toLowerCase();
        searchQuery = query;

        var queryPattern = new RegExp(query),
            matchesLength = 0;

        // console.log('queryPattern', queryPattern);
        $.each(stringCacheList, function (id, stringCache) {
            $.each(stringCache, function (i, strings) {
                if(strings.lat.match(queryPattern))
                    itemsMatched[id] ? itemsMatched[id].push(strings.str) : itemsMatched[id] = [strings.str];
            });
            if(!itemsMatched[id])
                $('#' + id).addClass('hidden-by-search');
            else{
                $('#' + id).removeClass('hidden-by-search');
                matchesLength++;
            }
        });
    }

    function getTextNodesIn(el) {
        return $(el).find(":not(iframe)").addBack().contents().filter(function() {
            return this.nodeType == 3;
        });
    }

    function itemMouseEnter(e) {
        var id = e.delegateTarget && e.delegateTarget.id,
            itemMatchedStrings;

        if(popupMatchedVisible || !id || !itemsMatched[id])
            return;

        itemMatchedStrings = itemsMatched[id];

        $popupMatched
            .css({top: e.pageY + 10, left: e.pageX + 10})
            .html(getMatchedList(itemMatchedStrings))
            .show();

        itemHovered = itemMatchedStrings;

        $('body')
            .on('mousemove', movePopupMatched);

        popupMatchedVisible = true;
    }

    function itemMouseLeave() {
        $popupMatched
            .hide();
        $('body')
            .off('mousemove', movePopupMatched);

        popupMatchedVisible = false;
    }

    function getMatchedList(list) {
        var result = $('<ul></ul>');
        $.each(list, function (i, matchedString) {
            var tmp = $.latinize(matchedString).toLowerCase(),
                position = tmp.indexOf(searchQuery),
                position2 = position + searchQuery.length;
            matchedString = [matchedString.slice(0, position), '<span class="hilighted" style="color: red !important">', matchedString.slice(position, position2), '</span>', matchedString.slice(position2)].join('');
            result.append('<li>' + matchedString + '</li>');
        });
        return result;
    }

    function movePopupMatched(e) {
        $popupMatched
            .css({top: e.pageY + 10, left: e.pageX + 10});
    }

})(jQuery, Drupal, this, this.document);
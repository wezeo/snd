/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

    if(!localStorage.tracking_opera)
        localStorage.tracking_opera = 0;
    if(!localStorage.tracking_balet)
        localStorage.tracking_balet = 0;
    if(!localStorage.tracking_cinohra)
        localStorage.tracking_cinohra = 0;

    localStorage.tracking_ine = 0;

    $(function() { // document ready
        var $menu = $('#block-snd-menu-custom-depth-main-menu-block .menu:first-child'),
            tracked = $menu.find('.active-trail[class*="tracking"]'),
            type;
        if(tracked.length){
            type = tracked.attr('class').match(/tracking-(\w+)/);
            localStorage['tracking_' + type[1]]++;
        }
        // console.log('localStorage', localStorage);
    });

    // function DEFINITIONS ===============================================================================================



})(jQuery, Drupal, this, this.document);
